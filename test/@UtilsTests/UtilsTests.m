classdef UtilsTests < matlab.unittest.TestCase

    methods (Test)
        function testElectrostatic2DGrad(testCase)

            fixedCharges = [1; 1];
            xQuery            = [1.5; 1.5];

            [val, actualGrad] = electrostaticPotential2D(xQuery, fixedCharges);

            [fdGrad, fdErr] = myGrad(...
                            @(x) electrostaticPotential2D(x, fixedCharges), ...  
                            xQuery, eps ^ (1/3)); 
            testCase.verifyEqual(actualGrad, fdGrad, ...
                                 'RelTol', max(1e2 * fdErr, 1e-4));

        end % testElectrostatic2DGrad

        function testElectrostatic2DHess(testCase)
            
            fixedCharges = [1; 1];
            xQuery            = [1.5; 1.3];

            [~, ~, actualHess] = electrostaticPotential2D(xQuery, fixedCharges);

            [fdHess, fdErr] = myHess(...
                            @(x) electrostaticPotential2D(x, fixedCharges), ...
                            xQuery, eps ^ (1/3));

            testCase.verifyEqual(actualHess, fdHess, ...
                                 'RelTol', max(1e2 * fdErr, 1e-4));


        end % testElectrostatic2DHess
        
        function testElectrostaticGenDGrad(testCase)

            fixedCharges = [1; 1; 1];
            xQuery            = [1.5; 1.3; 1.2];

            [val, actualGrad] = electrostaticPotentialGenD(xQuery, fixedCharges);

            [fdGrad, fdErr] = myGrad(...
                            @(x) electrostaticPotentialGenD(x, fixedCharges), ...
                            xQuery, eps ^ (1/3));

            testCase.verifyEqual(actualGrad, fdGrad, ...
                                 'RelTol', max(1e2 * fdErr, 1e-4));

        end % testElectrostaticGenDGrad

        function testElectrostaticGenDHess(testCase)
            
            fixedCharges = [1; 1; 1];
            xQuery            = [1.5; 1.3; 1.2];

            [~, ~, actualHess] = electrostaticPotentialGenD(xQuery, fixedCharges);

            [fdHess, fdErr] = myHess(...
                            @(x) electrostaticPotentialGenD(x, fixedCharges), ...
                            xQuery, eps ^ (1/3));

            testCase.verifyEqual(actualHess, fdHess, ...
                                 'RelTol', max(1e2 * fdErr, 1e-4));


        end % testElectrostaticGenDHess

        function testElectrostaticGenDWeightedGrad(testCase)

            fixedCharges = [1 -1; 1 -1; 1 -1];
            xQuery       = [1.5; 1.3; 1.2];
            W{1}         = diag(rand(3, 1));
            W{2}         = diag(rand(3, 1));
            [val, actualGrad] = electrostaticPotentialGenDWeighted(...
                                    xQuery, W, fixedCharges, 3);

            [fdGrad] = complex_step_grad(...
                        @(x) electrostaticPotentialGenDWeighted(...
                                x, W, fixedCharges, 3), ...
                        xQuery, 1e-100);

            testCase.verifyEqual(actualGrad, fdGrad, ...
                                 'RelTol', 1e-6);

        end % testElectrostaticGenDGrad

        function testElectrostaticGenDWeightedHess(testCase)
            
            fixedCharges = [1 -1; 1 -1; 1 -1];
            xQuery       = [1.5; 1.3; 1.2];
            W{1}         = diag(rand(3, 1));
            W{2}         = diag(rand(3, 1));

            [~, ~, actualHess] = electrostaticPotentialGenDWeighted(...
                                    xQuery, W, fixedCharges, 3);
            fdHess = myHess_complex_step(...
                        @(x) electrostaticPotentialGenDWeighted(...
                                x, W, fixedCharges, 3), ...
                        xQuery, eps ^ (1/3));
            
            testCase.verifyEqual(actualHess, fdHess, ...
                                 'RelTol', 1e-6);


        end % testElectrostaticGenDHess


        function testFDGrad(testCase)

            myFun = @(x) sin(x(1)) * cos(x(2));

            xQuery = [1; 1];
            expectedGrad = [cos(xQuery(1)) * cos(xQuery(2)); ...
                            -sin(xQuery(1)) * sin(xQuery(2))];
            [actualGrad, fdErr] = myGrad(myFun, xQuery, eps ^ (1/3));

            testCase.verifyEqual(actualGrad, expectedGrad, ...
                                 'RelTol', max(1e2 * fdErr, 1e-4));

        end

        function testFDHess(testCase)

            myFun = @(x) sin(x(1)) * cos(x(2));

            xQuery = [1; 1];
            expectedHess = [-sin(xQuery(1)) * cos(xQuery(2)), -cos(xQuery(1)) * sin(xQuery(2)); ...
                            -cos(xQuery(1)) * sin(xQuery(1)), -sin(xQuery(1)) * cos(xQuery(2))];
            [actualHess, fdErr] = myHess(myFun, xQuery, eps ^ (1/3));

            testCase.verifyEqual(actualHess, expectedHess, ...
                                 'RelTol', max(1e2 * fdErr, 1e-4));

        end % testFDHess

        function testUpdateMeanAndCov(testCase)

            dim = 100;
            szX = 500;
            szY = 900;

            X = randn(dim, szX);
            Y = randn(dim, szY);
            Z = [X, Y];

            meanX = mean(X, 2);
            covX  = cov(X', 1);

            [updatedMean, updatedCov] = updateMeanAndCov(Y, szX, meanX, covX);

            actualMean = mean(Z, 2);
            actualCov  = cov(Z', 1);

            updatedCombined = [updatedMean, updatedCov];
            actualCombined = [actualMean, actualCov];

            testCase.verifyEqual(updatedCombined, actualCombined, ...
                                 'RelTol', 1e-10);
        end % testUpdateMeanAndCov

        function testNormalizeLogWeights(testCase)

            w = rand(5, 1);
            logw = log(w);
            expected = normalizeLogWeights(logw);
            actual = w ./ sum(w);

            testCase.verifyEqual(expected, actual, 'RelTol', 1e-10);
        end % testNormalizeLogWeights

        function testMyMean(testCase)

            dim = randi(10, 1);

            x = randn(dim, 1000);

            testCase.verifyEqual(mymean(x), mean(x, 2), 'RelTol', 1e-10);
        end % testMyMean

        function testMyMeanWithWt(testCase)

            x = rand(2, 5);
            w = [0.1, 0.2, 0.3, 0.2, 0.2];

            avg = zeros(2, 1);

            for i = 1 : numel(w)
                avg = avg + w(i) * x(:, i);
            end

            testCase.verifyEqual(avg, mymean(x, w), 'RelTol', 1e-10);

        end % testMyMeanWithWt

        function testMyCov(testCase)

            dim = randi(10, 1);

            x = randn(dim, 1000);

            testCase.verifyEqual(mycov(x), cov(x', 1), 'RelTol', 1e-10);

        end % testMyCov

        function testMyCovWithWt(testCase)
            
            x = rand(1, 5);
            w = [0.1, 0.2, 0.3, 0.2, 0.2];


            testCase.verifyEqual(mycov(x, w), var(x, w), 'RelTol', 1e-10);
        end % testMyMeanWithWt

        function testAdaptiveKLDist(testCase)

            
            dim = randi(10, 1);

            mean1 = rand(dim, 1);
            mean2 = rand(dim, 1);

            l1 = tril(rand(dim));
            l2 = tril(rand(dim));

            cov1 = l1 * l1';
            cov2 = l2 * l2';

            mix1 = Gaussian(mean1, cov1);
            mix2 = Gaussian(mean2, cov2);

            tol = 0.001;
            actualDKL = adaptiveKLDist(mix1, mix2, tol);
            expectedDKL = 0.5 * (trace(mix2.precisionMat * mix1.covMat) ...
                                 + (mix2.meanVec - mix1.meanVec)' ...
                                    * mix2.precisionMat ...
                                    * (mix2.meanVec - mix1.meanVec) ...
                                    - mix1.dim ...
                                    + mix2.logDetCovMat - mix1.logDetCovMat);
            
            testCase.verifyEqual(actualDKL, expectedDKL, 'RelTol', 0.01);

        end

        function testTruncMean(testCase)
            %Mean of truncated normal must remain same is truncation is
            %symmetric

            trunc_limits = sort([rand(), 5 * rand()]);

            avg      = mean(trunc_limits);
            variance = 3;
            
            dMin = trunc_limits(1);
            dMax = trunc_limits(2);


            [~, actualAvg] = getTruncStats(avg, variance, dMin, dMax);

            expectedAvg = avg;

            testCase.verifyEqual(actualAvg, expectedAvg, 'RelTol', 1e-10);
            
        end % function definition

        function testInfLimits(testCase)
            %Test moments of truncated normal if truncation limits are [-Inf,
            %Inf]. With these limits, there is no truncation and moments should
            %be the same as that of the original normal.


            avg = 2;
            variance = 4;
            dMin = -1e12;
            dMax = 1e12;

            [truncMu, truncAvg, truncVar] = getTruncStats(avg, variance, dMin, dMax);

            actualOutput = [truncMu; truncAvg; truncVar];
            expectedOutput = [1; avg; variance;];

            testCase.verifyEqual(actualOutput, expectedOutput, 'RelTol', 1e-10);


        end % function definition

        function testTruncStatsValues(testCase)

            % Reference:
            % https://github.com/cossio/TruncatedNormal.jl/blob/master/test/moments.jl

            avg      = -2;
            variance = 9;
            dMin     = 50;
            dMax     = 70;

            [~, actualAvg, actualVar] = getTruncStats(avg, variance, dMin, dMax);

            expectedAvg = 50.1719434998988;
            expectedVar = 0.0293734381071683;

            act = [actualAvg, actualVar];
            expected = [expectedAvg, expectedVar];

            testCase.verifyEqual(act, expected, 'RelTol', 1e-10);


        end % testTruncStatsValues

        function testTruncHelperFunction1(testCase)

            actual = [trunc_helper1(1, 1 + 1e-8), ...
                      trunc_helper1(0.5, 0.5), ...
                      trunc_helper1(-2, 1), ...
                      trunc_helper1(-2, -1), ...
                      trunc_helper1(1, 2)];
            
            expected = [1.7724538597677852522848499, ...
                        0.5 * sqrt(pi), ...
                        -0.190184666491092019086029, ...
                        -2.290397265491751547564988, ...
                        2.29039726549175154756498826];
            
            testCase.verifyEqual(actual, expected, 'RelTol', 1e-10);

        end % testTruncHelperFunction1


        function testTruncHelperFunction2(testCase)

            actual = [trunc_helper2(1, 1 + 1e-8), ...
                      trunc_helper2(0.5, 0.5), ...
                      trunc_helper2(-1, 1), ...
                      trunc_helper2(-2, -1), ...
                      trunc_helper2(1, 2)];
            
            expected = [0.886226943177296522704243, ...
                        0.5 * 0.5 * sqrt(pi) - sqrt(pi) / 2, ...
                        -0.436548113220292413, ...
                        2.17039030552464315391802, ...
                        2.170390305524643153918];

            testCase.verifyEqual(actual, expected, 'RelTol', 1e-10);

        end % testTruncHelperFunction1

        function testGradNegLLHMixtureWeightsMLE(testCase)

            mean1 = [1; 1];
            mean2 = [-3; -5];
            means = [mean1, mean2];
            
            cov1 = diag([4, 1]);
            cov2 = eye(2);
            
            covs          = zeros(2, 2, 2);
            covs(:, :, 1) = cov1;
            covs(:, :, 2) = cov2;
            
            wts = [0.75, 0.25];
            
            actual_model = GaussianMixture(means, covs, wts);
            %actual_model = GaussianMixture(mean1, cov1, [1]);
            
            data = actual_model.rnd(10000);
            
            init_wts = [0.5, 0.5];
            
            init_model = GaussianMixture(means, covs, init_wts);
            
            [~, actualGrad] = negLLHMixtureWeightsMLE(init_wts', init_model, data)
            [fdGrad, fdErr] = myGrad(@(w) negLLHMixtureWeightsMLE(...
                                      w, init_model, data), init_wts', eps ^ (1/3));
            
            testCase.verifyEqual(actualGrad, fdGrad, ...
                                 'RelTol', max(1e2 * fdErr, 1e-4));
            
        end % testGradNegLLHMixtureWeightsMLE

        function testLogPDFUtils(testCase)

            dim = ceil(10 * rand());

            meanVec1 = rand(dim, 1);
            meanVec2 = rand(dim, 1);

            cholFactor1 = tril(rand(dim, dim));
            covMat1     = cholFactor1 * cholFactor1';
            cholFactor2 = tril(rand(dim, dim));
            covMat2     = cholFactor2 * cholFactor2';

            weights = [rand(), rand()];
            weights = weights / sum(weights);

            means = [meanVec1, meanVec2];

            covariances = zeros(dim, dim, 2);

            covariances(:, :, 1) = covMat1;
            covariances(:, :, 2) = covMat2;

            gmm = GaussianMixture(means, covariances, weights);

            nSamples = 10;


            matlab_gmm = gmdistribution(means', covariances, weights);
            
            testSamples = random(matlab_gmm, nSamples);
            testSamples = testSamples';

            actualPDF   = exp(logpdf_utils(gmm, testSamples));
            expectedPDF = matlab_gmm.pdf(testSamples');

            testCase.verifyEqual(actualPDF, expectedPDF, 'RelTol', 1e-6);


        end % testLogPDFUtils

        function testLogPDFUtilsAppend(testCase)

            dim = ceil(10 * rand());

            meanVec1 = rand(dim, 1);
            meanVec2 = rand(dim, 1);

            cholFactor1 = tril(rand(dim, dim));
            covMat1     = cholFactor1 * cholFactor1';
            cholFactor2 = tril(rand(dim, dim));
            covMat2     = cholFactor2 * cholFactor2';

            weights = [rand(), rand()];
            weights = weights / sum(weights);

            means = [meanVec1, meanVec2];

            covariances = zeros(dim, dim, 2);

            covariances(:, :, 1) = covMat1;
            covariances(:, :, 2) = covMat2;

            gmm_old = GaussianMixture([meanVec1], [covMat1], [1]);
            gmm_new = GaussianMixture(means, covariances, weights);

            nSamples = 10;


            matlab_gmm = gmdistribution(means', covariances, weights);

            testSamples = random(matlab_gmm, nSamples);
            testSamples = testSamples';

            [~, lp_i_old] = logpdf_utils(gmm_old, testSamples);

            actualPDF   = exp(logpdf_utils_append(gmm_new.weightArr, ...
                                                  gmm_new.components(2), ...
                                                  testSamples, lp_i_old));
            expectedPDF = matlab_gmm.pdf(testSamples');

            testCase.verifyEqual(actualPDF, expectedPDF, 'RelTol', 1e-6);


        end % testLogPDFUtilsAppend

        function testContinuationAdaptiveKLDist(testCase)

            dim = ceil(10 * rand());

            meanVec1 = rand(dim, 1);
            meanVec2 = rand(dim, 1);
            meanVec3 = rand(dim, 1);

            cholFactor1 = tril(rand(dim, dim));
            covMat1     = cholFactor1 * cholFactor1';
            cholFactor2 = tril(rand(dim, dim));
            covMat2     = cholFactor2 * cholFactor2';
            cholFactor3 = tril(rand(dim, dim));
            covMat3     = cholFactor3 * cholFactor3';

            weights = rand(1, 3);;

            means_old = [meanVec1, meanVec2];
            means_new = [meanVec1, meanVec2, meanVec3];

            covariances_old = zeros(dim, dim, 2);

            covariances_old(:, :, 1) = covMat1;
            covariances_old(:, :, 2) = covMat2;

            covariances_new = zeros(dim, dim, 3);
            covariances_new(:, :, 1) = covMat1;
            covariances_new(:, :, 2) = covMat2;
            covariances_new(:, :, 3) = covMat3;

            gmm_old = GaussianMixture(means_old, covariances_old, weights(1:2) / sum(weights(1:2)));
            gmm_new = GaussianMixture(means_new, covariances_new, weights / sum(weights));

            tol = 1e-2;

            [actualKLDist, actual_err] = continuationAdaptiveKLDist(gmm_old, ...
                                                      weights / sum(weights), ...
                                                      Gaussian(meanVec3, covMat3), ...
                                                      tol);

            [expectedKLDist, expected_err] = adaptiveKLDist(gmm_old, gmm_new, tol);


            testCase.verifyEqual(actualKLDist, expectedKLDist, 'RelTol', 4 * tol);


        end % testLogPDFUtilsAppend



    end % methods
end % classdef 
