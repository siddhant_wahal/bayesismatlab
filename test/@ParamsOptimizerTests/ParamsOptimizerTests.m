classdef ParamsOptimizerTests < matlab.mock.TestCase

    methods (Test)

        function testKLDiv(testCase)
            %KLDiv must be 0 when the ideal and the target IS distributions are
            %identical
            
            linearEstimator = LinearizedEstimator;
            prior = Gaussian;
            interval = [-1e16, 1e16];

            paramsOptimizer = ParamsOptimizer(linearEstimator, interval, prior);

            paramsOptimizer.pushFwdMean = 2 * rand();
            paramsOptimizer.pushFwdVar = 4 * rand();

            actualKLDiv = paramsOptimizer.getKLDist([1e16; rand()]);
            expectedKLDiv = 0;

            testCase.verifyEqual(actualKLDiv, expectedKLDiv, 'AbsTol', 1e-10);

        end % function definition

        function testKLDivGrad(testCase)

            linearEstimator = LinearizedEstimator;
            prior = Gaussian;
            interval = sort([rand(), 6 * rand()]);

            paramsOptimizer = ParamsOptimizer(linearEstimator, interval, prior);

            paramsOptimizer.pushFwdMean = 2 * rand();
            paramsOptimizer.pushFwdVar = 4 * rand();

            paramVec = [9; 2];


            [dKL, actualGrad] = paramsOptimizer.getKLDist(paramVec);
            [fdGrad, fdErr] = myGrad(@(x) paramsOptimizer.getKLDist(x), ...
                                    paramVec, eps ^ (1 / 3));
            
            testCase.verifyEqual(actualGrad, fdGrad, 'RelTol', ...
                                 max(1e2 * fdErr, 1e-4));

        end % function definition


        function testKLDivVsVar(testCase)


            dim = 2;
            meanVec = ones(dim, 1);
            v = [cos(pi/3); sin(pi/3)];
            w = [-sin(pi/3); cos(pi/3)];
            d = [0.1; 1.0];
            U = [v, w];
            
            covMat = U * diag(d) * U';
            
            %instantiate objects
            prior = Gaussian(meanVec, covMat);
            linProb = LinearMatProblem(dim);
            optProb = LinearMatOptProblem(linProb, prior);

            grad = linProb.B';
            pushFwdVar = grad' * prior.covMat * grad;
            pushFwdMean = grad' * prior.meanVec;
            targetMax = pushFwdMean + 4 * sqrt(pushFwdVar);
            targetMin = pushFwdMean + 3 * sqrt(pushFwdVar);

            initNoiseVar = 0.01 * (targetMax - targetMin)^2;

            data = targetMin + rand() * (targetMax - targetMin);

            map = prior.meanVec ...
                  + (data - pushFwdMean) ...
                  / (initNoiseVar + pushFwdVar) * prior.covMat * grad;
            
            linearEstimator = LinearizedEstimator(map, linProb);
            linearEstimator.build();
            interval = [targetMin, targetMax];

            paramsOptimizer = ParamsOptimizer(linearEstimator, interval, prior);

            paramVec = [initNoiseVar; data];

            expectedDKL = paramsOptimizer.getKLDist(paramVec);
            actualDKL = paramsOptimizer.getKLDistVsVar(paramVec(1));

            testCase.verifyEqual(actualDKL, expectedDKL, 'RelTol', 1e-10);
        end % function definition
        
        function testKLDivVsVarGrad(testCase)


            dim = 2;
            meanVec = ones(dim, 1);
            v = [cos(pi/3); sin(pi/3)];
            w = [-sin(pi/3); cos(pi/3)];
            d = [0.1; 1.0];
            U = [v, w];
            
            covMat = U * diag(d) * U';
            
            %instantiate objects
            prior = Gaussian(meanVec, covMat);
            linProb = LinearMatProblem(dim);
            optProb = LinearMatOptProblem(linProb, prior);

            grad = linProb.B';
            pushFwdVar = grad' * prior.covMat * grad;
            pushFwdMean = grad' * prior.meanVec;
            targetMax = pushFwdMean + 4 * sqrt(pushFwdVar);
            targetMin = pushFwdMean + 3 * sqrt(pushFwdVar);

            initNoiseVar = 0.01 * (targetMax - targetMin)^2;

            data = targetMin + rand() * (targetMax - targetMin);

            map = prior.meanVec ...
                  + (data - pushFwdMean) ...
                  / (initNoiseVar + pushFwdVar) * prior.covMat * grad;
            
            linearEstimator = LinearizedEstimator(map, linProb);
            linearEstimator.build();
            interval = [targetMin, targetMax];

            paramsOptimizer = ParamsOptimizer(linearEstimator, interval, prior);

            [dKL, actualGrad] = paramsOptimizer.getKLDistVsVar(initNoiseVar)
            [fdGrad, fdErr] = myGrad(@(x) paramsOptimizer.getKLDistVsVar(x), ...
                                    initNoiseVar, eps ^ (1/3));

            testCase.verifyEqual(actualGrad, fdGrad, 'RelTol', ...
                                 max(1e2 * fdErr, 1e-4));
        end % function definition


    end % methods 

end % class
