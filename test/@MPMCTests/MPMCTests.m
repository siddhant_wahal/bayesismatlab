classdef MPMCTests < matlab.unittest.TestCase

    methods (Test)
        
        function testRegression(testCase)

            mean1 = [1; 1];
            mean2 = [-3; -5];
            means = [mean1, mean2];
            
            cov1 = diag([4, 1]);
            cov2 = eye(2);
            
            covs          = zeros(2, 2, 2);
            covs(:, :, 1) = cov1;
            covs(:, :, 2) = cov2;
            
            wts = [0.75, 0.25];
            
            actual_model = GaussianMixture(means, covs, wts);
            %actual_model = GaussianMixture(mean1, cov1, [1]);
            targetPDF = @(x) actual_model.pdf(x);
            
            rng(1);
            
            init_means = [1.03, 1.11; 0.92, 0.98];
            init_covs = zeros(2, 2, 2);
            init_covs(:, :, 1) = diag([0.9, 0.95]);
            init_covs(:, :, 2) = diag([1.1, 1.2]);
            init_wts = [0.49, 0.51];


            init_model = GaussianMixture(init_means, init_covs, init_wts);
            mpmc = MPMC(targetPDF, init_model);
            mpmc.n_samples_per_iter = 100000;
            mpmc.tol = 1e-4;
            mpmc.max_iter = 10;
            mpmc.run();

            actual_val = [mpmc.mixture.components(1).meanVec, ...
                          mpmc.mixture.components(2).meanVec, ...
                          mpmc.mixture.components(1).covMat, ...
                          mpmc.mixture.components(2).covMat, ...
                          mpmc.mixture.weightArr'];

            expected_val = [1.00176945007902, -2.99730223969768, ...
                            4.01627138506082, -0.00925942051285752, ...
                            1.00827261819199,  0.00724729056610684, ...
                            0.749937027344249;
                            0.997540209669499, -5.00247374041839, ...
                            -0.00925942051285752, 1.00426557346978, ...
                            0.00724729056610684, 0.989328589235528, ...
                            0.250062972655751];

            testCase.verifyEqual(expected_val, actual_val, 'RelTol', 1e-8);

    end

    end % methods

end % classdef 
