classdef CubicProblemTests < matlab.unittest.TestCase

    properties
        dim;
        problem;
        opt_problem;
        prior;
        target;

    end % properties

    methods (TestMethodSetup)

        function setupCubicProblem(testCase)

            testCase.dim = 1 + randi(4, 1);

            id_like = zeros(testCase.dim, testCase.dim, testCase.dim);
            
            for i = 1 : testCase.dim
                id_like(:, :, i) = eye(testCase.dim);
            end
            
            id_like_tensor = tensor(id_like);

            sym_rand_tensor = symtensor(@randn, 3, testCase.dim);

            S = 10. * id_like_tensor + full(sym_rand_tensor);
            
            H = wishrnd(2 * eye(testCase.dim), testCase.dim + 1);
            
            b = rand(testCase.dim, 1);

            testCase.problem = CubicProblem(testCase.dim, S, H, b);

            testCase.prior = Gaussian(-2 * ones(testCase.dim, 1), ...
                                      eye(testCase.dim));

            testCase.opt_problem = CubicOptProblem(testCase.problem, ...
                                                   testCase.prior)

            testCase.target = 5.;
        end % setupCubicProblem

    end % TestMethodSetup methods

    methods (TestMethodTeardown)

        function teardownCubicProblem(testCase)

            testCase.problem = [];
            testCase.opt_problem = [];
            testCase.prior = [];
            testCase.target = [];

        end

    end % TestMethodTearDown methods

    methods (Test)

        function testGradQOI(testCase)

            n_samples = 10;

            samples = testCase.prior.rnd(n_samples);

            grad    = zeros(testCase.dim, n_samples);
            fd_grad = zeros(testCase.dim, n_samples);
            fd_err  = zeros(testCase.dim, n_samples);

            for i = 1 : n_samples

                [~, jac] = testCase.problem.getQOI(samples(:, i));

                grad(:, i) = jac';

                fd_grad(:, i) = myGrad(@(x)testCase.problem.getQOI(x), ...
                                                  samples(:, i), ...
                                                  eps ^(1/3));

            end

            testCase.verifyEqual(grad, fd_grad, 'RelTol', 1e-6)
        end % testGradQOI

        function testGradObjFun(testCase)

            n_samples = 10;

            samples = testCase.prior.rnd(n_samples);

            grad    = zeros(testCase.dim, n_samples);
            fd_grad = zeros(testCase.dim, n_samples);
            fd_err  = zeros(testCase.dim, n_samples);

            for i = 1 : n_samples

                [~, grad(:, i)] = testCase.opt_problem.objFunRegularized(samples(:, i), testCase.target, 1.);

                fd_grad(:, i) = ...
                                myGrad(...
                                        @(x)testCase.opt_problem.objFunRegularized(x, testCase.target, 1.), ...
                                        samples(:, i), ...
                                        eps ^ (1/3));

            end

            testCase.verifyEqual(grad, fd_grad, 'RelTol', 1e-6)
        end % testGradObjFun


        function testHessQOI(testCase)

            sample = testCase.prior.rnd(1);

            hess = testCase.problem.getHess(sample);

            fd_hess = myHess(@(x) testCase.problem.getQOI(x), ...
                                         sample, eps ^ (1/3));

            testCase.verifyEqual(hess, fd_hess, ...
                                 'RelTol', 1e-6);


        end % testHessQOI

        function testHessObjFun(testCase)

            sample = testCase.prior.rnd(1);

            hess = testCase.opt_problem.getHess(sample, testCase.target, 1.);

            fd_hess = myHess(@(x)testCase.opt_problem.objFunRegularized(x, testCase.target, 1.), ...
                                          sample, eps ^ (1/3));

            testCase.verifyEqual(hess, fd_hess, ...
                                 'RelTol', 1e-4, ...
                                 'AbsTol', 1e-4);

        end % testHessObjFun
    end % Test methods
    

end % classdef
