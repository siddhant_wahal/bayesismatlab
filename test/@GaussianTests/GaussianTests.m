classdef GaussianTests < matlab.unittest.TestCase

    methods (Test)

    
        function testMean(testCase)
            %Check if samples are being generated correctly by evaluating a test
            %integral : \int 1^T x p(x) dx. Should be equal to sum(meanVec).

            dim = ceil(10 * rand());
            meanVec = rand(dim, 1);
            cholFactor = tril(rand(dim, dim));
            covMat = cholFactor * cholFactor';

            g = Gaussian(meanVec, covMat);

            nSamples = 1e6;
            samples = g.rnd(nSamples);
            actualIntegral = sum(mean(samples, 2));

            testCase.verifyEqual(actualIntegral, sum(meanVec), 'RelTol', 1e-2);
        end
        
        
        function testPDF(testCase)
            %Check how implementaion of pdf(x) compares with internal matlab
            %function mvnpdf

            dim = ceil(10 * rand());
            meanVec = rand(dim, 1);
            cholFactor = tril(rand(dim, dim));
            covMat = cholFactor * cholFactor';

            g = Gaussian(meanVec, covMat);

            nSamples = 10;
            testSamples = 2 * rand(dim, nSamples);

            actualPDF = g.pdf(testSamples);
            expectedPDF = mvnpdf(testSamples', meanVec', covMat);

            testCase.verifyEqual(actualPDF, expectedPDF, 'RelTol', 1e-10);

        end

        function testLogPDF(testCase)
            %Check if exp(logpdf(x)) == pdf(x)
        
            dim = 1 + ceil(10 * rand());
            meanVec = rand(dim, 1);
            cholFactor = tril(rand(dim, dim));
            covMat = cholFactor * cholFactor';

            g = Gaussian(meanVec, covMat);

            nSamples = 10;
            testSamples = 2 * rand(dim, nSamples);

            actualLogPDF = g.logpdf(testSamples);
            actualPDF = g.pdf(testSamples);

            testCase.verifyEqual(exp(actualLogPDF), actualPDF, 'RelTol', 1e-10);
        end

        function testPrecisionMatLeft(testCase)
            %Check if covMat * precisionMat = I

            dim = 1 + ceil(10 * rand());
            meanVec = rand(dim, 1);
            cholFactor = tril(rand(dim, dim));
            covMat = cholFactor * cholFactor';

            g = Gaussian(meanVec, covMat);

            testCase.verifyEqual(g.precisionMat * g.covMat, eye(dim), ...
                                 'AbsTol', 1e-8);
        end

        function testPrecisionMatRight(testCase)
            %Check if precisionMat * covMat = I
        
            dim = 1 + ceil(10 * rand());
            meanVec = rand(dim, 1);
            cholFactor = tril(rand(dim, dim));
            covMat = cholFactor * cholFactor';

            g = Gaussian(meanVec, covMat);

            testCase.verifyEqual(g.covMat * g.precisionMat, eye(dim), ...
                                 'AbsTol', 1e-10);
        end


        function testPDFGradFD(testCase)
            %Compare gradients with finite difference

            dim = 1 + ceil(10 * rand());
            meanVec = rand(dim, 1);
            cholFactor = tril(rand(dim, dim));
            covMat = cholFactor * cholFactor';

            g = Gaussian(meanVec, covMat);

            nSamples = 10;

            samples = g.rnd(nSamples);

            actualGradMat = zeros(dim, nSamples);
            fdGradMat = zeros(dim, nSamples);
            fdErrMat = zeros(dim, nSamples);

            for i = 1 : nSamples

                sample = samples(:, i);
                [~, actualGradMat(:, i)] = g.pdf(sample);

                [fdGradMat(:, i), fdErrMat(:, i)] = ...
                            myGrad(@(x) g.pdf(x), sample, eps ^ (1 / 3));

            end

            testCase.verifyEqual(actualGradMat, fdGradMat, ...
                                 'RelTol', max(1e2 * fdErrMat, 1e-6));

        end


        function testPDFHessFD(testCase)
            %Compare gradients with finite difference

            dim = 1 + ceil(10 * rand());
            meanVec = rand(dim, 1);
            cholFactor = tril(rand(dim, dim));
            covMat = cholFactor * cholFactor';

            g = Gaussian(meanVec, covMat);

            nSamples = 1;

            sample = g.rnd(nSamples);

            [~, ~, actualHess] = g.pdf(sample);

            [fdHess, fdErr] = ...
                         myHess(@(x) g.pdf(x), sample, eps ^ (1 / 3));


            testCase.verifyEqual(actualHess, fdHess, ...
                                 'RelTol', max(1e2 * fdErr, 1e-6));

        end


        function testGradLogPDFAtMean(testCase)
            %Ensure gradient of logpdf at mean is always 0
 
            dim = 1 + ceil(10 * rand());
            meanVec = rand(dim, 1);
            cholFactor = tril(rand(dim, dim));
            covMat = cholFactor * cholFactor';

            g = Gaussian(meanVec, covMat);
            [~, grad] = g.logpdf(meanVec);

            testCase.verifyEqual(grad, zeros(dim, 1), 'AbsTol', 1e-10);

        end

        function testLogPDFHessian(testCase)
            %Ensure hessian of logpdf is always negative precision mat
            
            dim = 1 + ceil(10 * rand());
            meanVec = rand(dim, 1);
            cholFactor = tril(rand(dim, dim));
            covMat = cholFactor * cholFactor';

            g = Gaussian(meanVec, covMat);

            [~, ~, hess] = g.logpdf(rand(dim, 1));

            testCase.verifyEqual(hess, -g.precisionMat, 'RelTol', 1e-10);

        end


        function testLogPDFGradFD(testCase)
            %Compare gradients with finite difference

            dim = 1 + ceil(10 * rand());
            meanVec = rand(dim, 1);
            cholFactor = tril(rand(dim, dim));
            covMat = cholFactor * cholFactor';

            g = Gaussian(meanVec, covMat);

            nSamples = 10;

            samples = -3 * rand(dim, nSamples);

            actualGradMat = zeros(dim, nSamples);
            fdGradMat = zeros(dim, nSamples);
            fdErrMat = zeros(dim, nSamples);

            for i = 1 : nSamples

                sample = samples(:, i);
                [~, actualGradMat(:, i)] = g.logpdf(sample);

                [fdGradMat(:, i), fdErrMat(:, i)] = ...
                            myGrad(@(x) g.logpdf(x), sample, eps ^ (1 / 3));

            end

            testCase.verifyEqual(actualGradMat, fdGradMat, ...
                                 'RelTol', max(1e2 * fdErrMat, 1e-4));

        end
    end
end
