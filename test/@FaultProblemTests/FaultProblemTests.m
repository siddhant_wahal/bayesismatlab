classdef FaultProblemTests < matlab.unittest.TestCase
    
    properties

        num_faults;
        fault_problem;
        opt_problem;
        prior;
        target;

    end

    methods (TestMethodSetup)

        function setupFaultProblem(testCase)

            testCase.num_faults = 3;
            testCase.fault_problem = FaultProblem('3_faults.mat', ...
                                                   testCase.num_faults);

            testCase.prior = Gaussian(-2 * ones(testCase.num_faults, 1), ...
                                      eye(testCase.num_faults));

            testCase.opt_problem = FaultOptProblem(testCase.fault_problem, ...
                                                   testCase.prior);
            
            testCase.target = testCase.fault_problem.getQOI(testCase.prior.meanVec);
        end % setupFaultProblem

    end % TestMethodSetup methods

    methods (TestMethodTeardown)

        function teardownFaultProblem(testCase)

            testCase.num_faults    = [];
            testCase.fault_problem = [];
            testCase.opt_problem    = [];

        end 

    end % TestMethodTeardown methods

    methods (Test)

        function testGradQOI(testCase)
            % Test if gradient of the QOI is consistent with finite differences

            n_samples = 10;
            
            samples = testCase.prior.rnd(n_samples);

            grad    = zeros(testCase.num_faults, n_samples);
            fd_grad = zeros(testCase.num_faults, n_samples);
            fd_err  = zeros(testCase.num_faults, n_samples);


            for i = 1 : n_samples

                [~, jac] = testCase.fault_problem.getQOI(samples(:, i));
                grad(:, i) = jac';

                [fd_grad(:, i), fd_err(:, i)] = ...
                                    myGrad( @(m)testCase.fault_problem.getQOI(m), ...
                                           samples(:, i), ...
                                           eps ^ (1/3));

            end

            testCase.verifyEqual(grad, fd_grad, ...
                                 'RelTol', max(100 * fd_err ./ fd_grad, 1e-4));
            

        end % testGradQOI

        function testHessQOI(testCase)

            
            sample  = testCase.prior.rnd(1);

            hess = testCase.fault_problem.getHess(sample);


            [fd_hess, fd_err] = myHess_complex_step(@(m) testCase.fault_problem.getQOI(m), ...
                                       sample, eps ^ (1/3));


            testCase.verifyEqual(hess, fd_hess, ...
                                 'RelTol', max(100 * fd_err ./ fd_hess, 1e-4));
            

        end % testHessQOI

        function testGradObjFun(testCase)
            % Test if gradient of the QOI is consistent with finite differences

            n_samples = 10;
            
            samples = testCase.prior.rnd(n_samples);

            grad    = zeros(testCase.num_faults, n_samples);
            fd_grad = zeros(testCase.num_faults, n_samples);
            fd_err  = zeros(testCase.num_faults, n_samples);


            for i = 1 : n_samples


                [~, grad(:, i)] = testCase.opt_problem.objFunRegularized(samples(:, i), testCase.target, 1.0);

                [fd_grad(:, i), fd_err(:, i)] = ...
                                    myGrad(@(m)testCase.opt_problem.objFunRegularized(m, testCase.target, 1.0), ...
                                           samples(:, i), ...
                                           eps ^ (1/3));


            end

            testCase.verifyEqual(grad, fd_grad, ...
                                 'RelTol', max(100 * fd_err ./ fd_grad, 1e-4));
            

        end % testGradObjFun

        function testHessObjFun(testCase)

            
            sample  = testCase.prior.rnd(1);

            hess = testCase.opt_problem.getHess(...
                                sample, ...
                                testCase.fault_problem.getQOI(...
                                                testCase.prior.meanVec), ...
                                1.0);


            [fd_hess, fd_err] = ...
                    myHess_complex_step(...
                                @(m) testCase.opt_problem.objFunRegularized(...
                                            m, testCase.target, ...
                                            1.0), ...
                                        sample, eps ^ (1/3));

            testCase.verifyEqual(hess, fd_hess, ...
                                 'RelTol', max(100 * fd_hess ./ fd_hess, 1e-4));
            

        end % testHessObjFun


    end % Test methods

end % classdef
