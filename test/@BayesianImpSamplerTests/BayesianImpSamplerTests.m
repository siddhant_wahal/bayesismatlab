classdef BayesianImpSamplerTests < matlab.unittest.TestCase

    methods (Test)

        function testRun(testCase)
            %IS weights should be 1 if the prior and IS distributions are the
            %same

            dim     = 1 + randi(10, 1);
            problem = LinearMatProblem(dim);

            meanVec = rand(dim, 1);
            L       = tril(rand(dim, dim));
            covMat  = L * L';
            prior   = Gaussian(meanVec, covMat);
            optProb = LinearMatOptProblem(problem, prior);

            targetMin = -Inf;
            targetMax = Inf;

            impSampler = BayesianImpSampler([targetMin, targetMax], ...
                                            optProb, ...
                                            0.01);
            
            impSampler.normals      = GaussianMixture([meanVec], ...
                                                 [covMat], 1.0);
            impSampler.normalsExist = true;
            impSampler.nSamples     = 100;
            impSampler.dim          = dim;

            [avg, rmse, ess, dkl, frac] = impSampler.run();

            actualVals   = [avg, rmse, ess, dkl, frac];
            expectedVals = [1, 0, 1, 0, 1];

            testCase.verifyEqual(actualVals, expectedVals, 'AbsTol', 1e-10);

        end % testRun

    end % methods
end % classdef 
