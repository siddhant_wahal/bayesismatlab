classdef EMGMMTests < matlab.unittest.TestCase

    methods (Test)
        
        function testRegression(testCase)

            mean1 = [1; 1];
            mean2 = [-3; -5];
            means = [mean1, mean2];
            
            cov1 = diag([4, 1]);
            cov2 = eye(2);
            
            covs          = zeros(2, 2, 2);
            covs(:, :, 1) = cov1;
            covs(:, :, 2) = cov2;
            
            wts = [0.75, 0.25];
            
            actual_model = GaussianMixture(means, covs, wts);
            %actual_model = GaussianMixture(mean1, cov1, [1]);
            
            rng(1);
            data = actual_model.rnd(10000);
            
            init_means = [1.03, 1.11; 0.92, 0.98];
            init_covs = zeros(2, 2, 2);
            init_covs(:, :, 1) = diag([0.9, 0.95]);
            init_covs(:, :, 2) = diag([1.1, 1.2]);
            init_wts = [0.49, 0.51];


            init_model = GaussianMixture(init_means, init_covs, init_wts);
            em = EMGMM(data, init_model);
            em.tol = 1e-6;
            em.max_iter = 500;
            em.run();

            actual_val = [em.mixture.components(1).meanVec, ...
                          em.mixture.components(2).meanVec, ...
                          em.mixture.components(1).covMat, ...
                          em.mixture.components(2).covMat, ...
                          em.mixture.weightArr'];
            expected_val = [1.03412608204, -3.02471335615498, ...
                            4.01694675403057, -0.00845283652002879, ...
                            0.98848990732569, 0.0109750901964064, ...
                            0.754044417607985; ...
                            1.01391909372221, -4.9874937427613, ...
                            -0.00845283652002881, 0.99772836700975, ...
                            0.0109750901964064, 1.0426646533138, ...
                            0.245955582392014];

            testCase.verifyEqual(expected_val, actual_val, 'RelTol', 1e-8);

    end

    end % methods

end % classdef 
