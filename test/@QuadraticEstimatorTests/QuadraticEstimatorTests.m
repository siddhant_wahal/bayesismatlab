classdef QuadraticEstimatorTests < matlab.unittest.TestCase

    methods (Test)

        function testValues(testCase)

            %Values from QuadraticEstimator should be equal to an actual
            %quadratic function.

            %Actual quadratic problem
            dim = 1 + randi(10, 1);
            B = ones(1, 0);
            coeffSq = ones(dim, 1);
            quadraticProb = LowDimEmbeddingProblem(dim, dim, B, coeffSq);

            %QuadraticEstimator of problem
            g0 = rand(dim, 1);
            q = QuadraticEstimator(g0, quadraticProb)

            g = rand(dim, 100);

            %Values from QuadraticEstimator
            actual_val = q.evaluate(g);
            %Values from function
            expected_val = quadraticProb.getQOI(g);

            testCase.verifyEqual(actual_val, expected_val, 'RelTol', 1e-10);
        end % function
        
    end % methods
end % classdef 
