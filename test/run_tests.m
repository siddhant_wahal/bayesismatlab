addpath ../src/
addpath ../src/utils/
addpath ../src/examples/linearMat/
addpath ../src/examples/lowDimEmbedding/

try
    suite = matlab.unittest.TestSuite.fromFolder(pwd);
    results = run(suite);
    display(results);
catch e
    disp(getReport(e, 'extended'));
    exit(1)
end
exit(any([results.Failed]));

