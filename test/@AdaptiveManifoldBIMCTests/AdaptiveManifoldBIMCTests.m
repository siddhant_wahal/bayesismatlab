classdef AdaptiveManifoldBIMCTests < matlab.unittest.TestCase

    methods (Test)

        function testCostVal(testCase)

            %Tests that the cost function is behaving as expected.

            %Pick random dim between 1 and 11
            dim     = 1 + randi(10, 1);
            %Set up quadratic problem
            B       = ones(1, 0);
            coeffSq = ones(dim, 1);
            prob    = LowDimEmbeddingProblem(dim, dim, B, coeffSq);
            prior   = Gaussian(ones(dim, 1), eye(dim));
            optProb = LowDimEmbeddingOptProblem(prob, prior);
            
            %Set up importance sampler
            targetMin  = 11;
            targetMax  = 25;
            impSampler = AdaptiveManifoldBIMC([targetMin, targetMax], ...
                                              optProb, ...
                                              0.01 * (targetMax - targetMin)^2);
            

            
            x    = prior.rnd(1);
            data = targetMin + (targetMax - targetMin) * rand();

            actualLambdaVal = impSampler.costOneByOne(x, 0, 0, 1, data);
            actualBetaVal   = impSampler.costOneByOne(x, 1, 0, 0, data);
            actualMuVal     = impSampler.costOneByOne(x, 0, 2, 0, data); 
            actualVal       = [actualLambdaVal, actualBetaVal, actualMuVal];

            expectedLambdaVal = - (data - prob.getQOI(x));
            expectedBetaVal   = - prior.logpdf(x);
            expectedMuVal     = (data - prob.getQOI(x))^2;
            expectedVal       = [expectedLambdaVal, ...
                                 expectedBetaVal, ...
                                 expectedMuVal];

            testCase.verifyEqual(actualVal, expectedVal, 'RelTol', 1e-10);

        end % function testCostVal

        function testCostGrad(testCase)

            %Tests that the cost function gradient is correct by comparing
            %against finite differences

            %Pick random dim between 1 and 11
            dim = 1 + randi(10, 1);
            %Set up quadratic problem
            B       = ones(1, 0);
            coeffSq = ones(dim, 1);
            prob    = LowDimEmbeddingProblem(dim, dim, B, coeffSq);
            prior   = Gaussian(ones(dim, 1), eye(dim));
            optProb = LowDimEmbeddingOptProblem(prob, prior);
            
            %Set up importance sampler
            targetMin  = 11;
            targetMax  = 25;
            impSampler = AdaptiveManifoldBIMC([targetMin, targetMax], ...
                                              optProb, ...
                                              0.01 * (targetMax - targetMin)^2);
            

            
            x      = prior.rnd(1);
            data   = targetMin + (targetMax - targetMin) * rand();
            beta   = 1; 
            mu     = 1; 
            lambda = 1;

            [~, actualGrad] = impSampler.costOneByOne(x, beta, mu, ...
                                                      lambda, data);
            [fdGrad, fdErr] = myGrad(@(x) impSampler.costOneByOne(...
                                            x, beta, mu, lambda, data), ...
                                     x, eps ^ (1/3));

            testCase.verifyEqual(actualGrad, fdGrad, ...
                                 'RelTol', max(1e2 * fdErr, 1e-4));

        end % function testCostGrad

        function testCostHess_mv(testCase)

            %Tests that the cost function hessian is correct by comparing
            %against finite differences

            %Pick random dim between 1 and 11
            dim = 1 + randi(10, 1);
            %Set up quadratic problem
            B       = ones(1, 0);
            coeffSq = ones(dim, 1);
            prob    = LowDimEmbeddingProblem(dim, dim, B, coeffSq);
            prior   = Gaussian(ones(dim, 1), eye(dim));
            optProb = LowDimEmbeddingOptProblem(prob, prior);
            
            %Set up importance sampler
            targetMin  = 11;
            targetMax  = 25;
            impSampler = AdaptiveManifoldBIMC([targetMin, targetMax], ...
                                              optProb, ...
                                              0.01 * (targetMax - targetMin)^2);
            
            impSampler.useGNHess = false;
            
            x      = prior.rnd(1);
            data   = targetMin + (targetMax - targetMin) * rand();
            beta   = 1; 
            mu     = 1; 
            lambda = 1;

            [~, ~, Hinfo]   = impSampler.costOneByOne(x, beta, mu, ...
                                                         lambda, data);
            Y = rand(dim, 5);
            
            actualHY        = impSampler.costHess_mv(Hinfo, Y, x, ...
                                                     beta, mu, lambda, ...
                                                     data, prob.getQOI(x));

            [fdHess, fdErr] = myHess(@(x) impSampler.costOneByOne(...
                                            x, beta, mu, lambda, data), ...
                                     x, eps ^ (1/3));

            fdHY      = fdHess * Y;
            testCase.verifyEqual(actualHY, fdHY, ...
                                 'RelTol', 1e-2);

        end % function testCostHess_mv
 
        function testCostHess_mv_linear(testCase)

            %Tests that the cost function hessian is correct by comparing
            %directly against expected value

            %Pick random dim between 1 and 11
            dim = 1 + randi(10, 1);
            %Set up linear problem
            prob    = LinearMatProblem(dim);
            prior   = Gaussian(ones(dim, 1), eye(dim));
            optProb = LinearMatOptProblem(prob, prior);
            
            %Set up importance sampler
            targetMin  = 11;
            targetMax  = 25;
            impSampler = AdaptiveManifoldBIMC([targetMin, targetMax], ...
                                              optProb, ...
                                              0.01 * (targetMax - targetMin)^2);
            
            impSampler.useGNHess = false;
            
            x    = prior.rnd(1);
            data = targetMin + (targetMax - targetMin) * rand();
            Y    = rand(dim, 5);

            [~, ~, betaHinfo] = impSampler.costOneByOne(x, 1, 0, 0, data);
            actualBetaHY      = impSampler.costHess_mv(betaHinfo, Y, x, ...
                                                       1, 0, 0, data, ...
                                                       prob.getQOI(x));
            
            [~, ~, muHinfo] = impSampler.costOneByOne(x, 0, 1, 0, data);
            actualMuHY      = impSampler.costHess_mv(muHinfo, Y, x, ...
                                                         0, 1, 0, data, ...
                                                         prob.getQOI(x));
            
            [~, ~, lambdaHinfo] = impSampler.costOneByOne(x, 0, 0, 1, data);
            actualLambdaHY      = impSampler.costHess_mv(lambdaHinfo, Y, ...
                                                            x, 0, 0, 1, ...
                                                            data, prob.getQOI(x));

            [~, ~, temp]     = prior.logpdf(x);
            expectedBetaHY   = - temp * Y;
            jac              = prob.getJacobian(x);
            expectedMuHY     = jac' * jac * Y;
            expectedLambdaHY = zeros(dim) * Y;

            actualHY   = [actualBetaHY, actualMuHY, actualLambdaHY];
            expectedHY = [expectedBetaHY, expectedMuHY, expectedLambdaHY];

            testCase.verifyEqual(actualHY, expectedHY, ...
                                 'RelTol', 1e-10);

        end % function testCostHess_mv

        function testElectrostaticCost2D(testCase)

            dim = 2;
            %Set up linear problem
            prob    = LinearMatProblem(dim);
            prior   = Gaussian(ones(dim, 1), eye(dim));
            optProb = LinearMatOptProblem(prob, prior);
            
            %Set up importance sampler
            targetMin  = 11;
            targetMax  = 25;
            impSampler = AdaptiveManifoldBIMC([targetMin, targetMax], ...
                                              optProb, ...
                                              0.01 * (targetMax - targetMin)^2);

            q1 = rand(2, 1); q2 = rand(2, 1);
            qGuess = 0.25 * q1 + 0.75 * q2;
            impSampler.fixedCharges = [q1, q2];

            opts = optimoptions(@fminunc);
            opts.Algorithm = 'trust-region';
            opts.SpecifyObjectiveGradient = true;
            opts.HessianFcn = 'objective';
            opts.StepTolerance = 1e-6;
            qMin = fminunc(@(x) impSampler.costOneByOne(x, 0, 0, 0, 1), ...
                           qGuess, opts);

            qExpected = 0.5 * (q1 + q2);

            testCase.verifyEqual(qExpected, qMin, 'RelTol', opts.StepTolerance);
            
        end % testElectrostaticCost2D
 
         function testRegularizedLeastSquares(testCase)


            dim = 1 + randi(10, 1);
            prob    = LinearMatProblem(dim);
            prior   = Gaussian(ones(dim, 1), eye(dim));
            optProb = LinearMatOptProblem(prob, prior);
 
            %Set up importance sampler
            targetMin  = 11;
            targetMax  = 25;
            impSampler = AdaptiveManifoldBIMC([targetMin, targetMax], ...
                                              optProb, ...
                                              0.01 * (targetMax - targetMin)^2);

           
            impSampler.data = prob.getQOI(prior.rnd(1));
            impSampler.noiseVar = 0.01 * prob.B * prior.covMat * prob.B';

            opts = optimoptions(@fminunc);
            opts.Algorithm = 'trust-region';
            opts.SpecifyObjectiveGradient = true;
            opts.HessianFcn = 'objective';
            opts.StepTolerance = 1e-10;
            opts.OptimalityTolerance = 1e-10;
            opts.FunctionTolerance = 1e-10;
            xMin = fminunc(@(x) impSampler.costOneByOne(x, 1, ...
                                        1/impSampler.noiseVar , 0, ...
                                        impSampler.data), ...
                           prior.rnd(1), opts);


            xExpected = prior.meanVec + ...
                          (impSampler.data - impSampler.getQOI(prior.meanVec)) ...
                          / (impSampler.noiseVar + ...
                                prob.B * prior.covMat * prob.B') ...
                          * prior.covMat * prob.B';



            testCase.verifyEqual(xExpected, xMin, 'RelTol', 1e-4);

        end % testRegularizedLeastSquares
              
    end % methods
end % classdef 
