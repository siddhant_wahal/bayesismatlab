classdef GaussianMixtureTests < matlab.unittest.TestCase

    methods (Test)

        function testMean(testCase)
            %Check if samples are being generated correctly by evaluating sample
            %mean
            dim = ceil(10 * rand());
            meanVec1 = rand(dim, 1);
            meanVec2 = rand(dim, 1);
            cholFactor1 = tril(rand(dim, dim));
            covMat1 = cholFactor1 * cholFactor1';

            cholFactor2 = tril(rand(dim, dim));
            covMat2 = cholFactor2 * cholFactor2';

            weights = [rand(), rand()];
            weights = weights / sum(weights);

            means = [meanVec1, meanVec2];
            covariances = zeros(dim, dim, 2);
            covariances(:, :, 1) = covMat1;
            covariances(:, :, 2) = covMat2;

            gmm = GaussianMixture(means, covariances, weights);
            eqGauss = gmm.getEquivalentGaussian();

            nSamples = 1e6;

            samples = gmm.rnd(nSamples);
            actualIntegral = sum(mean(samples, 2));

            expectedIntegral = sum(eqGauss.meanVec);
            testCase.verifyEqual(actualIntegral, expectedIntegral, 'RelTol', 1e-2);

        end

        function testPDF(testCase)
            %Check how implementaion of pdf(x) compares with internal matlab
            %function mvnpdf


            dim = ceil(10 * rand());
            meanVec1 = rand(dim, 1);
            meanVec2 = rand(dim, 1);
            cholFactor1 = tril(rand(dim, dim));
            covMat1 = cholFactor1 * cholFactor1';

            cholFactor2 = tril(rand(dim, dim));
            covMat2 = cholFactor2 * cholFactor2';

            weights = [rand(), rand()];
            weights = weights / sum(weights);

            means = [meanVec1, meanVec2];
            covariances = zeros(dim, dim, 2);
            covariances(:, :, 1) = covMat1;
            covariances(:, :, 2) = covMat2;

            gmm = GaussianMixture(means, covariances, weights);
            nSamples = 10;
            testSamples = mvnrnd(meanVec1', covMat1, nSamples);
            testSamples = testSamples';

            matlab_gmm = gmdistribution(means', covariances, weights);

            actualPDF = gmm.pdf(testSamples);
            expectedPDF = matlab_gmm.pdf(testSamples');

            testCase.verifyEqual(actualPDF, expectedPDF, 'RelTol', 1e-6);

        end

        function testLogPDF(testCase)
            %Check if exp(logpdf(x)) == pdf(x)
        
            
            dim = ceil(10 * rand());
            %dim = 3
            meanVec1 = rand(dim, 1);
            meanVec2 = rand(dim, 1);
            cholFactor1 = tril(rand(dim, dim));
            covMat1 = cholFactor1 * cholFactor1';

            cholFactor2 = tril(rand(dim, dim));
            covMat2 = cholFactor2 * cholFactor2';

            weights = [rand(), rand()];
            weights = weights / sum(weights);

            means = [meanVec1, meanVec2];
            covariances = zeros(dim, dim, 2);
            covariances(:, :, 1) = covMat1;
            covariances(:, :, 2) = covMat2;

            gmm = GaussianMixture(means, covariances, weights);
            
            nSamples = 50;
            testSamples = 2 * rand(dim, nSamples);
            
            actualLogPDF = gmm.logpdf(testSamples);
            actualPDF = gmm.pdf(testSamples);

            testCase.verifyEqual(exp(actualLogPDF), actualPDF, 'RelTol', 1e-10);
        end

        function testGradLogPDFAtMeanOneComponent(testCase)
            %Ensure gradient of logpdf at mean is always 0
 
            dim = ceil(10 * rand());
            meanVec = rand(dim, 1);
            cholFactor = tril(rand(dim, dim));
            covMat = cholFactor * cholFactor';

            gmm = GaussianMixture([meanVec], [covMat], [1]);
            [~, grad] = gmm.logpdf(meanVec);

            testCase.verifyEqual(grad, zeros(dim, 1), 'AbsTol', 1e-10);

        end

        function testLogPDFHessianOneComponent(testCase)
            %Ensure hessian of logpdf is always negative precision mat
            
            dim = ceil(10 * rand());
            meanVec = rand(dim, 1);
            cholFactor = tril(rand(dim, dim));
            covMat = cholFactor * cholFactor';

            covariances = zeros(dim, dim, 1);
            covariances(:, :, 1) = covMat;
            gmm = GaussianMixture([meanVec], covariances, [1])

            [~, grad, hess] = gmm.logpdf(gmm.components(1).rnd(1));

            testCase.verifyEqual(hess, -gmm.components(1).precisionMat, ...
                                 'RelTol', 1e-10);

        end


        function testLogPDFGradFD(testCase)
            %Compare gradients with finite difference


            dim = ceil(10 * rand());
            meanVec1 = rand(dim, 1);
            meanVec2 = rand(dim, 1);
            cholFactor1 = tril(rand(dim, dim));
            covMat1 = cholFactor1 * cholFactor1';

            cholFactor2 = tril(rand(dim, dim));
            covMat2 = cholFactor2 * cholFactor2';

            weights = [rand(), rand()];
            weights = weights / sum(weights);

            means = [meanVec1, meanVec2];
            covariances = zeros(dim, dim, 2);
            covariances(:, :, 1) = covMat1;
            covariances(:, :, 2) = covMat2;

            gmm = GaussianMixture(means, covariances, weights);
            nSamples = 10;

            samples = gmm.rnd(nSamples);

            actualGradMat = zeros(dim, nSamples);
            fdGradMat = zeros(dim, nSamples);
            fdErrMat = zeros(dim, nSamples);

            i = 1;
            while i <= nSamples

                sample = samples(:, i);
                [~, temp1] = gmm.logpdf(sample);

                actualGradMat(:, i) = temp1;

                [temp2, temp3] = ...
                            myGrad(@(x) gmm.logpdf(x), sample, eps ^ (1 / 3));
                fdGradMat(:, i) = temp2;
                fdErrMat(:, i) = abs(temp3 ./ temp2);
                i = i + 1;
            end

            testCase.verifyEqual(actualGradMat, fdGradMat, ...
                                 'RelTol', ...
                                 max(1e1 * fdErrMat, ...
                                     1e-4 * ones(size(fdErrMat))));

       end

        function testHessSymmetry(testCase)

            dim = ceil(10 * rand());
            meanVec1 = rand(dim, 1);
            meanVec2 = rand(dim, 1);
            cholFactor1 = tril(rand(dim, dim));
            covMat1 = cholFactor1 * cholFactor1';

            cholFactor2 = tril(rand(dim, dim));
            covMat2 = cholFactor2 * cholFactor2';

            weights = [rand(), rand()];
            weights = weights / sum(weights);

            means = [meanVec1, meanVec2];
            covariances = zeros(dim, dim, 2);
            covariances(:, :, 1) = covMat1;
            covariances(:, :, 2) = covMat2;

            gmm = GaussianMixture(means, covariances, weights);
            [~, grad, hess] = gmm.logpdf(rand(dim, 1));

            testCase.verifyEqual(hess, hess', ...
                                 'RelTol', 1e-10);

        end


        function testHessFD(testCase)

            dim = ceil(10 * rand());
            meanVec1 = rand(dim, 1);
            meanVec2 = rand(dim, 1);
            cholFactor1 = tril(rand(dim, dim));
            covMat1 = cholFactor1 * cholFactor1';

            cholFactor2 = tril(rand(dim, dim));
            covMat2 = cholFactor2 * cholFactor2';

            weights = [rand(), rand()];
            weights = weights / sum(weights);

            means = [meanVec1, meanVec2];
            covariances = zeros(dim, dim, 2);
            covariances(:, :, 1) = covMat1;
            covariances(:, :, 2) = covMat2;

            gmm = GaussianMixture(means, covariances, weights);

            sample = gmm.rnd(1);

            [~, grad, actualHess] = gmm.logpdf(sample);

            [fdHess, fdErr] = ...
                            myHess(@(x) gmm.logpdf(x), sample, eps ^ (1 / 3));

            fdErr = abs(fdErr ./ fdHess);
            testCase.verifyEqual(actualHess, fdHess, ...
                                 'RelTol', ...
                                 max(1e1 * fdErr, ...
                                     1e-4 * ones(size(fdErr))));

        end
    end
end
