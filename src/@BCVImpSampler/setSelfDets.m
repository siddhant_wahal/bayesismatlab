function setSelfDets(obj)

obj.selfDets = zeros(obj.nOpt);

    for i = 1 : obj.nOpt
        for j = 1 : obj.nOpt
            obj.selfDets(i, j) = sum(log(eig(obj.normals.components(i).covMat * obj.normals.components(j).precisionMat)));
        end
    end

    obj.selfDetsExist = true;
end
