function weightArr = getCVWeight(obj, k, sampleArr)

    n = size(sampleArr, 2);
    weightArr = zeros(n, 1);
    

    for i = 1 : n
        sample = sampleArr(:, i);
        sumTerm = 0.0;
        for j = 1 : obj.nOpt
            sumTerm = sumTerm + exp(0.5 * obj.normals.components(k).sqMahal(sample) ... 
                                    - 0.5 * obj.normals.components(j).sqMahal(sample) ...
                                    + log(obj.normals.weightArr(j)) + 0.5 * obj.selfDets(k, j));
        end

        weightArr(i) = 1.0 / sumTerm;

    end



end
