function [avg, rmse, no_cv_avg, no_cv_rmse, ess] = run(obj, linearApprox)

    fprintf('Generating %d samples ...\n', obj.nSamples)
    if obj.normalsExist == false
        obj.createNormals();
    end

    if obj.detsExist == false
        obj.setDets();
    end

    if obj.selfDetsExist == false
        obj.setSelfDets();
    end

    %initialize empty (zero) arrays
    dim = length(obj.optProblem.prior.meanVec);
    nonLinFunTimesWt = zeros(obj.nSamples, 1);
    linFunTimesWt = zeros(obj.nSamples, 1);
    weightArr = zeros(obj.nSamples, 1);
    nonLinObsArr = zeros(obj.nSamples, 1);
    linObsArr = zeros(obj.nSamples, 1);
    samplesArr = zeros(dim, obj.nSamples);

    if obj.loadSamplesFromFile
        temp = dlmread(obj.samplesFileName);
        samplesArr = temp(:, 1 : dim)';
        size(samplesArr)
        weightArr = temp(:, dim + 1);
        nonLinObsArr = temp(:, end - 1);
        linObsArr = temp(:, end);
        clear temp;
    else

        samplesArr = obj.normals.rnd(obj.nSamples);
        weightArr = obj.getISWeight(samplesArr);
        nonLinObsArr = obj.getQOIFromSamples(samplesArr);
        linObsArr = linearApprox.evaluate(samplesArr);
        if obj.saveSamples
            dlmwrite(obj.samplesFileName, [samplesArr', weightArr, nonLinObsArr, linObsArr], '-append', 'delimiter', ' ', 'precision', '%1.14f');
        end
    end
        
        insideIdxNonLin = nonLinObsArr < obj.targetObj.limits(2) & nonLinObsArr > obj.targetObj.limits(1);
        insideIdxLin = linObsArr < obj.targetObj.limits(2) & linObsArr > obj.targetObj.limits(1);
        nonLinFunTimesWt(insideIdxNonLin) = weightArr(insideIdxNonLin);
        linFunTimesWt(insideIdxLin) = weightArr(insideIdxLin);



    linProb = linearApprox.getLinearizedProb(obj.optProblem.prior, ...
                                          obj.targetObj.limits(1), ...
                                          obj.targetObj.limits(2));

    y = nonLinFunTimesWt;
    X = zeros(obj.nSamples, obj.nOpt - 1);
    
    for i = 1 : obj.nOpt - 1
        X(:, i) = obj.getCVWeight(i, samplesArr);
    end
    
    no_cv_avg = mean(nonLinFunTimesWt);
    no_cv_rmse = sqrt(var(nonLinFunTimesWt) / obj.nSamples);

    weightNorm = nonLinFunTimesWt / sum(nonLinFunTimesWt);
    ess = 1 / (obj.nSamples * sum(weightNorm .^ 2))
    %add linearized probability estimate as another control variate
    X = [X - 1.0, linFunTimesWt - linProb, ones(obj.nSamples, 1)];
    b = mldivide(X, y);

    beta = b(1 : end - 1);
    avg = b(end);
    variance  = 1.0 / (obj.nSamples - obj.nOpt) * (y - X * b)' * (y - X * b);
    rmse = sqrt(variance /(obj.nSamples));

end
