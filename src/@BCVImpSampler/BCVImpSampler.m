classdef BCVImpSampler < BayesianImpSampler
    properties
        selfDets = []
        selfDetsExist = false
    end

    methods
        function obj = BCVImpSampler(targetObj, optProblem, noiseVar)
            obj@BayesianImpSampler(targetObj, optProblem, noiseVar);
        end

        [weightArr] = getCVWeight(obj, k, sampleArr);
        setSelfDets(obj);
        [avg, rmse, no_cv_avg, no_cv_rmse, ess] = run(obj, linearApprox);
    end
end
