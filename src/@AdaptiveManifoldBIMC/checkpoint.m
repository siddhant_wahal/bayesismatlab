function checkpoint(obj, filename)

    mixture    = obj.normals;
    surrogates = obj.surrogates;
    data       = obj.data;
    noiseVar   = obj.noiseVar;
    save(filename, 'mixture', 'surrogates', 'data', 'noiseVar');

end
