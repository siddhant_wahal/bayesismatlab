function [varargout] = costOneByOne(obj, x, beta, mu, lambda, data)
% Returns the value, gradient and hessian  of the cost function as requested.
%
% Inputs:
%   x      : Parameter at which cost function is to be evaluated
%   beta   : Coefficient that controls tradeoff between repulsive potential and the
%            attractive (prior) potential.
%   mu     : Coefficient of the penalty term
%   lambda : Augmented Lagrangian variable
%   data   : Data in the penalty (misfit) term  
% 
% Output:
%  [cost, cost_grad, cost_hess]  : Value, gradient and hessian of the cost
%                                  function as requested 

    if nargout == 1
        qoi = obj.optProblem.problem.getQOI(x);
    elseif nargout > 1
        [qoi, jac] = obj.optProblem.problem.getQOI(x);
    end

    %compute contribution to cost from the repulsive potential
    if obj.use_weighted_norm

        [repulsion_cost, repulsion_grad, repulsion_hess] ...
                        = electrostaticPotentialGenDWeighted(...
                                x, ...
                                obj.fixedChargesMetrics, ...
                                obj.fixedCharges, ...
                                obj.exponent);
    
    else
        [repulsion_cost, repulsion_grad, repulsion_hess] ...
                        = electrostaticPotentialGenD(x, obj.fixedCharges, obj.exponent);
    
    end
    %compute contribution to cost from attractive potential
    attraction_cost = - beta * obj.optProblem.prior.logpdf(x);

    %compute penalty and lagrange multiplier term
    penalty = 0.5 * mu * (data - qoi) * (data - qoi);
    lagrange_mult_term = - lambda * (data - qoi);
    
    %compute total cost
    cost = repulsion_cost + attraction_cost + penalty + lagrange_mult_term;

    %increment constraint evaluations
    obj.funcEvals = obj.funcEvals + 1;

    varargout(1) = {cost};

    if nargout > 1
        
        grad = zeros(obj.dim, 1);

        %Gradient of attractive potential
        [~, prior_grad] = obj.optProblem.prior.logpdf(x);
        attr_grad = - beta * prior_grad;

        %Gradient of penalty and lagrange multiplier
        penalty_grad = - mu * (data - qoi) * jac';
        lagrange_mult_grad = lambda * jac';

        cost_grad = repulsion_grad ...
                    + attr_grad ...
                    + penalty_grad ...
                    + lagrange_mult_grad;
        
        varargout(2) = {cost_grad};

        if nargout > 2
            %Hessian calculations
            hess = zeros(obj.dim);

            [~, ~, prior_hess] = obj.optProblem.prior.logpdf(x);
            attr_hess = -beta * prior_hess;

            penalty_hess_GN = mu * jac' * jac;

            hess_GN = repulsion_hess ...
                        + attr_hess ...
                        + penalty_hess_GN;
            
            if ~obj.useGNHess

                %If full hessian requested, compute the remaining hessian using
                %matrix vector products.

                obj.options.HessianMultiplyFcn = @(Hinfo, Y) ...
                            obj.costHess_mv(Hinfo, Y, x, ...
                                            beta, mu, lambda, ...
                                            data, qoi);

            end
            %Otherwise, use this as approximation as the hessian
            varargout(3) = {hess_GN};
        end 
end
