function tunedGaussian = getTunedGaussian(obj, newPt, varargin)
    
    if nargin > 2
        noiseVar = cell2mat(varargin(1));
    else
        noiseVar = obj.getOptVar(newPt);
    end

    [newGNHess, invNewGNHess] = obj.optProblem.getGNHess(newPt, noiseVar);

    tunedGaussian = Gaussian(newPt, invNewGNHess);

end
