classdef AdaptiveManifoldBIMC < BayesianImpSampler
    
    properties

        plot_codes = struct('initialGuess',      0, ...
                            'fixedCharges',      1, ...
                            'componentCenters',  2, ...
                            'optimizerState',    3, ...
                            'idealDistribution', 4,  ...
                            'selfKLDiv',         5);
        
        options = optimoptions(@fminunc);
        
        %state of the algorithm
        animate_mode           = false;
        viz_mode               = false;
        initialized            = false;
        loadFirstComponent     = false;
        saveFirstComponent     = false;
        loadFinalMixture       = false;
        saveFinalMixture       = false;
        useTrueMapForCovTuning = false;
        useGNHess              = true;


        %properties
        likelihood;
        maxFailureCount;
        repulsion;
        metric_rank;

        use_low_rank_metric = false;
        use_weighted_norm   = false;

        exponent               = 3;
        use_quad_surrogate     = true;
        absPerplexityTol       = 0.99;
        relPerplexityTol       = 1e-2;
        maxPts                 = 2e2;
        fixedCharges           = [];
        fixedChargesMetrics    = {};
        centers                = [];
        logMixtureWts          = [];
        surrogates             = [];
        firstComponentFilename = [];
        finalMixtureFilename   = [];
        animateFilename        = [];
        selfPerplexityArr      = [];
        mpmcPerplexityArr      = [];
        targetKLDivArr         = [];
        visitedGaussians       = {};
        visitedGaussiansLogWts = {};
        nBeta                  = 5;
        %betaArr                = [1e-3, 1e-2, 1e-1, 1];
        betaArr                = [];
 
        %diagnostics
        funcEvals;
        fminuncEvals;
        
    end

    methods
        function obj = AdaptiveManifoldBIMC(limits, optProblem, initNoiseVar)

            obj@BayesianImpSampler(limits, optProblem, initNoiseVar);

            obj.fixedCharges = [];
            obj.funcEvals    = 0;
            obj.fminuncEvals = 0;


            if isa(obj.optProblem.prior, 'Gaussian')
                obj.maxFailureCount = 5;
            elseif isa(obj.optProblem.prior, 'GaussianMixture')
                obj.maxFailureCount = 5 * obj.optProblem.prior.nComp;
            end


            obj.options.Algorithm                = 'trust-region';
            obj.options.SpecifyObjectiveGradient = true;
            obj.options.HessianFcn               = 'objective';
            obj.options.DerivativeCheck          = 'off';
            obj.options.FinDiffType              = 'central';
            obj.options.Display                  = 'off';
            obj.options.OptimalityTolerance      = 1e-2;
            obj.options.FunctionTolerance        = 1e-2;
            obj.options.StepTolerance            = 1e-2;
            obj.options.MaxFunctionEvaluations   = 5;
            obj.options.OutputFcn                = @(x, optimValues, state) ...
                                                     plot_function(x, ...
                                                                   optimValues, ...
                                                                   state, ...
                                                                   obj);
        end

        viz(obj, plot_codes, varargin);
        createNormals(obj);
        initialize(obj);
        adaptOneByOne(obj, nCharges);
        checkpoint(obj, filename);
        tuneEM(obj, data_size, max_iter, wts_only);
        tuneMPMC(obj, n_samples_per_iter, max_iter, wts_only);
        animate(obj, filename);
        continuation(obj, newPt, data);
        [betaMin, betaMax] = getRegularizationParam(obj);
        
        [varargout]     = costOneByOne(obj, x, beta, mu, lambda, data);
        [HY]            = costHess_mv(obj, Hinfo, Y, x, beta, mu, lambda, data, qoi);
        [var]           = getOptVar(obj, x);
        [tunedGaussian] = getTunedGaussian(obj, newPt, varargin);
        [val]           = targetPDF(obj, x);
        [dkl]           = klDistToTarget(obj, n);
        [weight]        = getMixtureWeights(obj, gauss1);
        [val]           = getAvgTargetPDF(obj, gaussian, nSamples);
        [log_post]      = getLogPosterior(obj, x);
        [val]           = targetPDFQuad(obj, x);
        [data]          = generateEMData_master(obj, data_size);
        [d]             = getQOI_NNQuad(obj, x);

        reducedNormals = getReducedNormals(obj, psi_r, full_normals);
        [val] = targetPDFQuadReduced(obj, x_r, phi_r, reduced_prior)
        tuned_normals = tuneMPMCReduced(obj, ...
                                        n_samples_per_iter, ...
                                        max_iter, ...
                                        wts_only, ...
                                        psi_r, ...
                                        reduced_normals);
        full_normals = getFullNormals(obj, psi_r, psi_perp, reduced_normals);
    end
end
