function [HY] = costHess_mv(obj, Hinfo, Y, x, beta, mu, lambda, data, qoi);

            %hess_GN = Hinfo(1 : obj.dim,  1 : obj.dim);
            %
            %x = Hinfo(obj.dim + 1, :);
            %lambda = Hinfo(obj.dim + 2, 1);
            %mu = Hinfo(obj.dim + 3, 1);
            %data = Hinfo(obj.dim + 4, 1);
            %qoi = Hinfo(obj.dim + 5, 1);

            hess_GN = Hinfo;
            forward_map_hess_mv = obj.optProblem.problem.getHess_mv(x, Y);
            

            HY = hess_GN * Y ...
                 + (lambda - mu * (data - qoi)) * forward_map_hess_mv;            

end
