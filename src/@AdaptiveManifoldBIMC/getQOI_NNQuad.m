function [d] = getQOI_NNQuad(obj, x)

    n = size(x, 2);

    n_surrogates = numel(obj.surrogates);

    dist = zeros(n, n_surrogates);
    d    = zeros(n, 1);

    for i = 1 : n_surrogates
        diff = x - obj.surrogates(i).g0;
        dist(:, i) = (sum(diff .* diff))' ;
    end

    [~, min_comp] = min(dist, [], 2); %2: find min of each row


    for i = 1 : n_surrogates

        d(min_comp == i) = obj.surrogates(i).evaluate(x(:, min_comp == i));

    end

end
