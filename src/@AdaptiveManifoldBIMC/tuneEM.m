function tuneEM(obj, data_size, max_iter, wts_only)
% Tune mixture parameters using the EM algorithm

    %generate data for tuning
    data = obj.generateEMData_master(data_size);

    em          = EMGMM(data, obj.normals);
    em.max_iter = max_iter;
    em.display  = true;
    em.run(wts_only);

    obj.normals = em.mixture.deepCopy();

end
