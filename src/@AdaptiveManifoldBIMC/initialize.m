function initialize(obj)
    
    fprintf('Initializing ... ')


    if obj.loadFirstComponent
        %If first component already constructed, load it from file
        load(obj.firstComponentFilename, 'data', 'noiseVar', 'firstComponent', 'q');
    else
        
        dim = obj.dim;

        %Find the optimal parameters as in BIMC v1.0
        mid                 = mean(obj.limits);
        [midMAP, funcEvals] = obj.optProblem.getMAP(mid, ...
                                       obj.optProblem.prior.rnd(1), ...
                                       obj.initNoiseVar);
        
        d = obj.getQOI(midMAP);

        assert(d < obj.limits(2) && d > obj.limits(1), ...
               'Midpoint MAP not inside pre-image');

        %Strategy for funcEvals:
        % - when working with negative log of the posterior, e.g., to compute
        %   the psudo-MAP point, increment funcEvals
        %   based on number of function evaluations required to minimize the
        %   negative log posterior
        % - when working with the repulsive cost function, e.g., for
        %   continuation, increment obj.funcEvals inside costOneByOne
        obj.funcEvals    = obj.funcEvals + funcEvals;
        obj.fminuncEvals = obj.fminuncEvals + funcEvals;
        
        optParamVec = obj.getOptParams(midMAP);
        noiseVar    = optParamVec(1);
        data        = optParamVec(2);
        
        assert(data < obj.limits(2) && data > obj.limits(1), ...
               'Optimal data not inside target interval');
               

        %Then compute the MAP using these optimal parameters
        [map, funcEvals] = obj.optProblem.getMAP(data, ...
                                    midMAP, ...
                                    noiseVar);
        [d, jac]         = obj.optProblem.problem.getQOI(map);
        assert(d < obj.limits(2) && d > obj.limits(1), ...
               'MAP not inside pre-image');
        
        %Increment funcEvals counter
        
        obj.funcEvals    = obj.funcEvals + funcEvals;
        obj.fminuncEvals = obj.fminuncEvals + funcEvals;

        %Tune covariance
        firstComponent = obj.getTunedGaussian(map, noiseVar);
        
        if obj.use_quad_surrogate
            s = QuadraticEstimator(firstComponent.meanVec, obj.optProblem.problem);
        else
            s = LinearizedEstimator(firstComponent.meanVec, ...
                                    obj.optProblem.problem);
        end

        if obj.saveFirstComponent
            save(obj.firstComponentFilename, 'data', 'noiseVar', ...
                'firstComponent', 's')
        end
    end

    obj.noiseVar       = noiseVar;
    obj.data           = data;
    obj.likelihood     = Gaussian(data, noiseVar);
    obj.normals        = GaussianMixture(firstComponent.meanVec, ...
                                         full(firstComponent.covMat), 1);
    if obj.animate_mode
        obj.animate(sprintf('./raw_data/normals_%d', i + 1))
    end
    obj.surrogates     = [s];
    obj.logMixtureWts  = [obj.getMixtureWeights(obj.normals.components(1))];
    obj.fixedCharges   = [obj.fixedCharges, firstComponent.meanVec];

    if obj.use_low_rank_metric

        [U, ~, ~] = svd(full(firstComponent.precisionMat));

        Ur = U(:, 1 : obj.metric_rank);

        obj.fixedChargesMetrics{1} = Ur * Ur';

    else

        obj.fixedChargesMetrics{1} ...
                       = firstComponent.precisionMat;

    end

    obj.centers        = [obj.centers, firstComponent.meanVec];
    obj.initialized    = true;

    if ~obj.useGNHess
        obj.options.HessianMultiplyFcn = @(Hinfo, Y) ...
                            obj.costHess_mv(Hinfo, Y, zeros(obj.dim, 1), ...
                                            0, 0, 0, 0, 0);
    end

    [betaMin, betaMax] = obj.getRegularizationParam()

    %betaMin = abs(obj.repulsion(obj.optProblem.prior.meanVec, map) ...
    %          / (obj.optProblem.prior.logpdf(obj.optProblem.prior.meanVec)));

    %x_near = map;
    %search_dir = jac' / myVecNorm(jac');

    %[~, prior_grad] = obj.optProblem.prior.pdf(x_near);

    %postMarginalVar = search_dir' * obj.normals.components(1).covMat * search_dir;
    %step_size = sqrt(postMarginalVar);


    %%if search_dir' * prior_grad > 0
    %%        seach_dir = -search_dir;
    %%end
    %

    %keyboard
    %if search_dir' * prior_grad < 0
    %        search_dir = -search_dir;
    %end


    %x_near = x_near  + step_size * search_dir;
    %

    %[~, attraction_grad] = obj.optProblem.prior.logpdf(x_near);
    %[~, repulsion_grad]  = obj.repulsion(x_near, map);

   
    %%betaMax = abs(obj.repulsion(x_near, map) ...
    %%          / (-obj.optProblem.prior.logpdf(x_near)));

    %betaMax = myVecNorm(repulsion_grad) / myVecNorm(attraction_grad);
    %betaMin = 1 / betaMax;

    if betaMin >= betaMax
        obj.betaArr = logspace(-3, 1, obj.nBeta);
    else
        obj.betaArr = logspace(log10(betaMin), log10(betaMax), obj.nBeta)
    end
    fprintf('Done.\n')
end
