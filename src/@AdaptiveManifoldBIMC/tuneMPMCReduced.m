function tuned_normals = tuneMPMCReduced(obj, ...
                                         n_samples_per_iter, ...
                                         max_iter, wts_only, ...
                                         psi_r, ...
                                         reduced_normals)
% Tune mixture parameters using the M-PMC algorithm in the reduced space.
% Inputs:
% -- n_samples_per_iter : Number of samples used to compute the integral per
%                         iteration
% -- max_iter           : Maximum number of iterations
% -- wts_only           : Tune the weights only?
% -- psi_r              : Leading eigenvectors of the expected prior
%                         preconditioned Gauss-Newton Hessian
% -- reduced_normals    : GaussianMixture instance in the reduced space that
%                         needs to be tuned.
% Outputs:
% -- tuned_normals : GaussianMixture that results from tuning using MPMC

    phi_r = obj.optProblem.prior.cholFactor * psi_r;
    xi_r  = obj.optProblem.prior.cholFactor' \ psi_r;

    r = size(phi_r, 2);

    reduced_prior_mean = xi_r' * obj.optProblem.prior.meanVec;
    reduced_prior_cov  = speye(r);

    reduced_prior = Gaussian(reduced_prior_mean, reduced_prior_cov);

    targetPDF = @(x_r) obj.targetPDFQuadReduced(x_r, phi_r, reduced_prior);

    mpmc                    = MPMC(targetPDF, reduced_normals);
    mpmc.n_samples_per_iter = n_samples_per_iter;
    mpmc.max_iter           = max_iter;
    mpmc.display            = true;

    mpmc.run(wts_only);
    tuned_normals = mpmc.mixture.deepCopy();

end
