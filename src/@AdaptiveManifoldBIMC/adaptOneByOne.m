function adaptOneByOne(obj)
%Adds Gaussian objects to the IS mixture one by one using an adaptive discovery
%procedure.
    
    fprintf('Adaptively sampling on manifold ... ');
    
    if obj.initialized == false
        obj.initialize();
    end

    %Diagnostic plotting
    if obj.viz_mode
        obj.viz([obj.plot_codes.idealDistribution, ...
                 obj.plot_codes.componentCenters, ...
                 obj.plot_codes.fixedCharges]);

        %obj.viz([obj.plot_codes.componentCenters, ...
        %         obj.plot_codes.fixedCharges]);
    end
    
    
    while obj.normals.nComp < obj.maxPts

        if length(obj.selfPerplexityArr) > 2 ...

            relChange = abs(obj.selfPerplexityArr(end) ...
                            -  obj.selfPerplexityArr(end -1)) ...
                        / obj.selfPerplexityArr(end - 1);

            if relChange < obj.relPerplexityTol ...
                && ...
                obj.selfPerplexityArr(end) > obj.absPerplexityTol
                break;
            end
        end
        
        initGuess = obj.optProblem.prior.rnd(1);
        
        if obj.viz_mode
            obj.viz([obj.plot_codes.initialGuess], initGuess);
        end

        
        %Sample a data point from the target interval
        data = obj.limits(1) + (obj.limits(2) - obj.limits(1)) * rand();
       
        %Perform continuation
        obj.continuation(initGuess, data);
        
        %Loop over collection of Gaussians visited and check if some Gaussian can be
        %added to the IS mixture
        %while size(obj.visitedGaussians, 1) > 0
        %    proposedPerplexityArr = [];
        %    for j = 1 : size(obj.visitedGaussians, 1)
        %        proposedMixture = obj.normals.deepCopy();
        %        proposedLogWts  = [obj.logMixtureWts, obj.visitedGaussiansLogWts{j}];
        %        proposedMixture.append(obj.visitedGaussians{j}, ...
        %                               normalizeLogWeights(proposedLogWts));
        %        
        %        proposedPerplexityArr = [proposedPerplexityArr, ...
        %                                 exp(-adaptiveKLDist(obj.normals, ...
        %                                               proposedMixture, ...
        %                                               0.05))];
        %                            %myKLDist(obj.normals, proposedMixture, 1e6)];
        %        
        %    end

        %    [minPerplexity, minIdx] = min(proposedPerplexityArr);

        %    %Add if perplexity is smaller than tolerance
        %    if minPerplexity < obj.absPerplexityTol
        %        newLogWts = [obj.logMixtureWts, obj.visitedGaussiansLogWts{minIdx}];
        %        obj.normals.append(obj.visitedGaussians{minIdx}, ...
        %                           normalizeLogWeights(newLogWts));
        %        %disp('appended\n')
        %        if obj.animate_mode
        %            obj.animate(sprintf('./raw_data/normals_%d', i + 1))
        %        end

        %        obj.logMixtureWts     = [obj.logMixtureWts, ...
        %                                 obj.visitedGaussiansLogWts{minIdx}];
        %        obj.fixedCharges      = [obj.fixedCharges, ...
        %                                 obj.visitedGaussians{minIdx}.meanVec];
        %        obj.centers           = [obj.centers, obj.visitedGaussians{minIdx}.meanVec];
        %        obj.selfPerplexityArr = [obj.selfPerplexityArr, minPerplexity];

        %        %fprintf('%d, %1.4e\n', obj.normals.nComp, minPerplexity);
        %        if obj.use_quad_surrogate
        %            s = QuadraticEstimator(obj.visitedGaussians{minIdx}.meanVec, ...
        %                                   obj.optProblem.problem);
        %        else
        %            s = LinearizedEstimator(obj.visitedGaussians{minIdx}.meanVec, ...
        %                                    obj.optProblem.problem);
        %        end


        %        obj.surrogates = [obj.surrogates, s];

        %        %Remove the added Gaussian from the list
        %        obj.visitedGaussians{minIdx}    = {};
        %        obj.visitedGaussiansLogWts{minIdx} = {};
        %        
        %        %Reset success counter

        %        if obj.viz_mode
        %            obj.viz([obj.plot_codes.fixedCharges, ...
        %                     obj.plot_codes.componentCenters, ...
        %                     obj.plot_codes.selfKLDiv]);
        %            %obj.viz([obj.plot_codes.selfKLDiv]);
        %        end
        %    else
        %        break;
        %    end

        %end
    end
    obj.normalsExist = true;
    obj.nOpt         = obj.normals.nComp;
    fprintf('Done.\n')

end
