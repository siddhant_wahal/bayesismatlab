function stop = plot_function(x, optimValues, state, obj)
    stop = false;
    
    switch state
    case 'iter'
        if obj.viz_mode
            obj.viz([obj.plot_codes.optimizerState], x);
        end
    end
end
