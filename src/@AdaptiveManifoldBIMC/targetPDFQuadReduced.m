function [val] = targetPDFQuadReduced(obj, x_r, phi_r, reduced_prior)
% Returns the reduced target PDF, where the forward model has been approximated
% by a surrogate.
% Inputs:
% -- x_r           : Query locations in the reduced space
% -- phi_r         : Matrix that projects the reduced parameter back to the 
%                    full parameter 
% -- reduced_prior : The marginalization of the prior distribution in the
%                    reduced space
% Returns: 
% -- val : Value of the target PDF

    d = obj.getQOI_NNQuad(phi_r * x_r);

    inside = d < obj.limits(2) & d > obj.limits(1);
    
    val = zeros(size(x_r, 2), 1);
    
    val(inside) = reduced_prior.pdf(x_r(:, inside));

end

