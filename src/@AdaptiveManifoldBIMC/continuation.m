function continuation(obj, initGuess, data)
%Function for performing continuation with respect to the regularization
%parameter that controls the strength of the prior potential.
%
%Inputs:
%- newCharge - Starting value for charge location obtained by minimizing the
%              potential energy using beta 
%- data:     - Data value for the misfit term in the cost function
%
%Outputs:
%
%- successful - Boolean that represents whether continuation was successful or
%               or not. If true, a new component was appended to the IS
%               distribution. If false, no change was made.


    %Array to store KL divergence between current IS mixture and the proposed IS
    %mixture at every beta
    proposedPerplexityArr = zeros(obj.nBeta, 1);
    %Array to store the weight of tuned gaussian proposed for each beta
    proposedMixtureLogWts = zeros(obj.nBeta, 1);
    %Array to store the tuned gaussian proposed for each beta
    proposedTunedGaussians = [];


    %Obtain an initial guess for lambda, the lagrange multiplier
    betaInit = 10^( 0.5 * (log10(obj.betaArr(2)) + log10(obj.betaArr(3))));

    if obj.use_weighted_norm
        [~, repulsion_grad] = electrostaticPotentialGenDWeighted(...
                                    initGuess, obj.fixedChargesMetrics, ...
                                    obj.fixedCharges, obj.exponent);
    else
        [~, repulsion_grad] = electrostaticPotentialGenD(initGuess, obj.fixedCharges, obj.exponent);
    end

    [~, prior_grad] = obj.optProblem.prior.logpdf(initGuess);
    jac = obj.optProblem.problem.getJacobian(initGuess);
    
    lambdaInitGuess = myVecNorm(repulsion_grad - betaInit * prior_grad) ...
                      / (myVecNorm(jac'));

    oldCharge         = initGuess;
    toAddFixedCharges = [];
    toAddMetrics      = {};

    early_termination = false;

    %Loop over beta values
    for i = 1 : obj.nBeta
        
        beta = obj.betaArr(i);

        %Penalty and augmented lagrangian coefficients
        mu     = 1.0 /(obj.noiseVar);
        lambda = lambdaInitGuess;

        %Get new charge location at current beta
        [newCharge, ~, ~, output] = fminunc(@(x) obj.costOneByOne(...
                                                x, beta, mu, lambda, data), ...
                                    oldCharge, obj.options);
        

        
        %Increment fminunc evaluations
        d = obj.getQOI(newCharge);
        obj.fminuncEvals = obj.fminuncEvals + output.funcCount;
        %no need to increment fminuncEvals for QOI evaluatioin above because the
        %optimizer can always be hacked to return it. There's nothing new
        %happening here
        
        
        %If newCharge is outside the pre-image, increase mu and modify
        %lambda till it is inside
        while d < obj.limits(1) || d > obj.limits(2)

            lambda = lambda - mu  * (data - d);
            mu     = 2.0 * mu;
            
            [newCharge, ~, ~, output] = fminunc(@(x) obj.costOneByOne(...
                                                    x, beta, mu, lambda, data), ...
                                                newCharge, obj.options);
            d                         = obj.getQOI(newCharge);
            obj.fminuncEvals          = obj.fminuncEvals + output.funcCount;
        end


        oldCharge = newCharge;
        
        %Tune the covariance around it 
        newGaussian            = obj.getTunedGaussian(newCharge);
        proposedTunedGaussians = [proposedTunedGaussians, ...
                                  newGaussian];
        

        toAddFixedCharges = [toAddFixedCharges, ...
                             proposedTunedGaussians(i).meanVec];
        
        if obj.use_low_rank_metric

            [U, ~, ~] = svd(full(proposedTunedGaussians(i).precisionMat));

            Ur = U(:, 1 : obj.metric_rank);

            toAddMetrics{end + 1} = Ur * Ur';

        else

            toAddMetrics{end + 1} = ...
                            proposedTunedGaussians(i).precisionMat;

        end

        %Compute what the IS mixture would look like if this tuned gaussian
        %is appended
        proposedMixtureLogWts(i) = ...
                obj.getMixtureWeights(proposedTunedGaussians(i));
        
        proposedMixture = obj.normals.deepCopy();
        
        tempLogWts = [obj.logMixtureWts, proposedMixtureLogWts(i)];

        proposedMixture.append(proposedTunedGaussians(i), ...
                               normalizeLogWeights(tempLogWts));

        %Compute how different the proposed mixture is from current one,
        %measured in terms of the KL divergence

        if i == 1

            [dkl, ~, allSamples, allLogPDF_i, allLogPDF] = ...
                continuationAdaptiveKLDist(obj.normals, ...
                                           normalizeLogWeights(tempLogWts), ...
                                           newGaussian, ...
                                           0.05);
        else
            dkl = continuationAdaptiveKLDist(obj.normals, ...
                                           normalizeLogWeights(tempLogWts), ...
                                           newGaussian, ...
                                           0.05, ...
                                           allSamples, ...
                                           allLogPDF_i, ...
                                           allLogPDF);
        end

        proposedPerplexityArr(i) = exp(-dkl);


        if i > 1

            if proposedPerplexityArr(i) <= proposedPerplexityArr(i - 1)
                continue;
            else
                minIdx = i - 1;
                obj.normals.append(proposedTunedGaussians(minIdx), ...
                       normalizeLogWeights([obj.logMixtureWts, ...
                            proposedMixtureLogWts(minIdx)]));
   
                obj.logMixtureWts     = [obj.logMixtureWts, proposedMixtureLogWts(minIdx)];
                obj.centers           = [obj.centers, proposedTunedGaussians(minIdx).meanVec];
                obj.selfPerplexityArr = [obj.selfPerplexityArr, proposedPerplexityArr(minIdx)];

                %fprintf('%d, %1.4e\n', obj.normals.nComp, proposedPerplexityArr(i));
                if obj.use_quad_surrogate
                    s = QuadraticEstimator(proposedTunedGaussians(minIdx).meanVec, ...
                                           obj.optProblem.problem);
                else
                    s = LinearizedEstimator(proposedTunedGaussians(minIdx).meanVec, ...
                                            obj.optProblem.problem);
                end

                obj.surrogates = [obj.surrogates, s];
                
                early_termination = true;
                break;
            end
        end

    end

    %If perplexity monotonically decreased for all beta, append the last one

    if i == obj.nBeta && ~early_termination

        minIdx = obj.nBeta;

        obj.normals.append(proposedTunedGaussians(minIdx), ...
                       normalizeLogWeights([obj.logMixtureWts, ...
                            proposedMixtureLogWts(minIdx)]));
    
        obj.logMixtureWts     = [obj.logMixtureWts, proposedMixtureLogWts(minIdx)];
        obj.centers           = [obj.centers, proposedTunedGaussians(minIdx).meanVec];
        obj.selfPerplexityArr = [obj.selfPerplexityArr, proposedPerplexityArr(minIdx)];

        %fprintf('%d, %1.4e\n', obj.normals.nComp, proposedPerplexityArr(i));
        if obj.use_quad_surrogate
            s = QuadraticEstimator(proposedTunedGaussians(minIdx).meanVec, ...
                                   obj.optProblem.problem);
        else
            s = LinearizedEstimator(proposedTunedGaussians(minIdx).meanVec, ...
                                    obj.optProblem.problem);
        end
       
        obj.surrogates = [obj.surrogates, s];
                



    end

    %[~, minIdx] = min(proposedPerplexityArr);

    %obj.normals.append(proposedTunedGaussians(minIdx), ...
    %                   normalizeLogWeights([obj.logMixtureWts, ...
    %                        proposedMixtureLogWts(minIdx)]));
    %
    %obj.logMixtureWts     = [obj.logMixtureWts, proposedMixtureLogWts(minIdx)];
    %obj.centers           = [obj.centers, proposedTunedGaussians(minIdx).meanVec];
    %obj.selfPerplexityArr = [obj.selfPerplexityArr, proposedPerplexityArr(minIdx)];

    %%fprintf('%d, %1.4e\n', obj.normals.nComp, proposedPerplexityArr(i));
    %if obj.use_quad_surrogate
    %    s = QuadraticEstimator(proposedTunedGaussians(minIdx).meanVec, ...
    %                           obj.optProblem.problem);
    %else
    %    s = LinearizedEstimator(proposedTunedGaussians(minIdx).meanVec, ...
    %                            obj.optProblem.problem);
    %end

    %obj.surrogates = [obj.surrogates, s];
    %
    %if obj.viz_mode
    %    obj.viz([obj.plot_codes.componentCenters, ...
    %             obj.plot_codes.selfKLDiv]);
    %end
    %         
 
    if obj.viz_mode
        obj.viz([obj.plot_codes.componentCenters, ...
                 obj.plot_codes.selfKLDiv]);
    end


    obj.fixedCharges = [obj.fixedCharges, toAddFixedCharges];

    for i = 1 : size(toAddMetrics, 2)

        obj.fixedChargesMetrics{end + 1} = toAddMetrics{i};

    end
    
    if obj.viz_mode
        obj.viz([obj.plot_codes.fixedCharges]);
    end
    
end
