function animate(obj, filename)

    %linear
    x_lim = [1, 3];
    y_lim = [0, 3];
    h = figure;

    samples = obj.normals.rnd(1e4);

    x = linspace(x_lim(1), x_lim(2), 200);
    y = linspace(y_lim(1), y_lim(2), 200);

    [X, Y] = meshgrid(x, y);

    obj.optProblem.prior.plot_ellipse(0.9, [0, 0, 1, 0.7]);
    idealPDF = obj.targetPDF([X(:), Y(:)]');

    contour(X, Y, reshape(idealPDF, numel(x), numel(y)), 5, ...
            'LineWidth', 2);
    obj.normals.plot_ellipses(0.9, [1, 0, 0]);

    y1 = y_lim(1);
    y2 = y_lim(2);

    B = obj.optProblem.problem.B;

    x2_max = (obj.limits(2) - y2 * B(2)) / B(1);
    x1_max = (obj.limits(2) - y1 * B(2)) / B(1);

    x2_min = (obj.limits(1) - y2 * B(2)) / B(1);
    x1_min = (obj.limits(1) - y1 * B(2)) / B(1);

    plot([x1_min, x2_min], [y1, y2], '--k', 'LineWidth', 3);
    plot([x1_max, x2_max], [y1, y2], '--k', 'LineWidth', 3);

    xlim(x_lim);
    ylim(y_lim);

    xlabel('$m_1$');
    ylabel('$m_2$');

    xticks([0, 1, 2, 3]);
    yticks([0, 1, 2, 3]);


    print(filename, '-dpng', '-r600');
    close(h)

    %x_lim = [-1.5, 1.5];
    %y_lim = [-16, -13];
    %h = figure;

    %samples = obj.normals.rnd(1e4);

    %obj.optProblem.prior.plot_ellipse(0.9, [0, 0, 1, 0.7]);
    %x = linspace(x_lim(1), x_lim(2), 200);
    %y = linspace(y_lim(1), y_lim(2), 200);

    %[X, Y] = meshgrid(x, y);

    %idealPDF = obj.targetPDF([X(:), Y(:)]');

    %contour(X, Y, reshape(idealPDF, numel(x), numel(y)), 10, ...
    %        'LineWidth', 2);
    %obj.normals.plot_ellipses(0.9, [1, 0, 0]);

    %x_arr = linspace(x_lim(1), x_lim(2), 100);

    %y1 = x_arr .^2 - 0.1 * sqrt(obj.limits(1) - (1 - x_arr) .^2);
    %y2 = x_arr .^2 - 0.1 * sqrt(obj.limits(2) - (1 - x_arr) .^2);
    %%B = obj.optProblem.problem.B;

    %%x2_max = (obj.limits(2) - y2 * B(2)) / B(1);
    %%x1_max = (obj.limits(2) - y1 * B(2)) / B(1);

    %%x2_min = (obj.limits(1) - y2 * B(2)) / B(1);
    %%x1_min = (obj.limits(1) - y1 * B(2)) / B(1);

    %plot(x_arr, y1, '--k', 'LineWidth', 3);
    %plot(x_arr, y2, '--k', 'LineWidth', 3);
    %%plot([x1_min, x2_min], [y1, y2], '--k');
    %%plot([x1_max, x2_max], [y1, y2], '--k');

    %xlim(x_lim);
    %ylim(y_lim);

    %xlabel('$m_1$');
    %ylabel('$m_2$');

    %xticks([-1.5, 0, 1.5])
    %yticks([y_lim(1) : y_lim(2)])


    %print(filename, '-dpng', '-r600');
    %close(h)


end
