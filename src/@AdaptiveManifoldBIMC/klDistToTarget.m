function [dkl] = klDistToTarget(obj, n)

    samples = obj.normals.rnd(n);
    obsArr = obj.getQOI(samples);

    inside = obsArr < obj.limits(2) & obsArr > obj.limits(1);

    weights = obj.getISWeight(samples);
    weights = inside .* weights;
    
    normalizedWeights = weights / sum(weights);

    dkl = log(n) + normalizedWeights(inside)' * log(normalizedWeights(inside));

end
