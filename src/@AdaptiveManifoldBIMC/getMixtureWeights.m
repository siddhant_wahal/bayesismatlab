function weight = getMixtureWeights(obj, gauss1)
% Returns the mixture weight of a component in the IS mixture.
% Input:
% - gauss1 : Component whose weight is to be evaluated
% Output:
% - weight : log of the weight in the mixture.

    if isa(obj.optProblem.prior, 'Gaussian')
        weight = logIntProductGaussians(gauss1, obj.optProblem.prior);
    elseif isa(obj.optProblem.prior, 'GaussianMixture')
        weight = logIntProductGMM(gauss1, obj.optProblem.prior);
    end

end
