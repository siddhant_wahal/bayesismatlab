function [val] = targetPDFQuad(obj, x)

    d = obj.getQOI_NNQuad(x);

    inside = d < obj.limits(2) & d > obj.limits(1);
    
    val = zeros(size(x, 2), 1);
    
    val(inside) = obj.optProblem.prior.pdf(x(:, inside));

end

