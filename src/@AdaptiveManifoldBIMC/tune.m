function tunedGaussian = tune(obj, inputGaussian, quadEstimate)
%Tunes the mean and covariance based on a quadratic estimate starting from a
%specified Gaussian distribution. Adaptively increases the number of samples
%from the input Gaussian distribution till the sample mean and covariance aren't
%changing significantly.
%
%Inputs:
%   inputGaussian: An instance of the Gaussian class that represents the
%   initial gaussian PDF
%   quadEstimate: An instance of the QuadraticEstimator class that represents
%   the quadratic approximation of the forward map.
%
%Output:
%   tunedGaussian: A Gaussian object that represents the tuned Gaussian PDF

    
    %Number of samples to start with. Also, the increment in number of samples.
    dim = inputGaussian.dim;
    nSamples = 2 * dim;

    inSamples = [];

    while size(inSamples, 2) < dim
        samples = inputGaussian.rnd(nSamples);
    
        if obj.useTrueMapForCovTuning
            obs = quadEstimate.evaluateTrueMap(samples);
            obj.funcEvals = obj.funcEvals + size(samples, 2);
        else
            obs = quadEstimate.evaluate(samples);
        end
        
        inside = obs < obj.limits(2) & obs > obj.limits(1);
        inSamples = samples(:, inside);
    end

    newMean = mean(inSamples, 2);
    newCov  = cov(inSamples', 1);
    newSize = size(inSamples, 2);
    
    count = 0;

    while true

        if any(any(isnan(newCov))) || any(isnan(newMean))
            keyboard
        end
        
        oldMean = newMean;
        oldCov  = newCov;
        oldSize = newSize;

        samples = inputGaussian.rnd(nSamples);
        
        if obj.useTrueMapForCovTuning
            obs = quadEstimate.evaluateTrueMap(samples);
            obj.funcEvals = obj.funcEvals + size(samples, 2);
        else
            obs = quadEstimate.evaluate(samples);
        end
        
        inside = obs < obj.limits(2) & obs > obj.limits(1);
        %Size of all accepted samples used so far
        newSize = size(samples(:, inside), 2) + oldSize;
        
        [newMean, newCov] = updateMeanAndCov(samples(:, inside), oldSize, ...
                                             oldMean, oldCov);

        relChangeMean = myVecNorm(newMean - oldMean) ...
                        / myVecNorm(oldMean);
        relChangeCov = norm(newCov - oldCov, 'fro') / norm(oldCov, 'fro');

        if relChangeMean < 0.05 ...
           && relChangeCov < 0.05 ...
           && newSize > 2 * dim

            count = count + 1;

            %Break if relative changes are below tolerances 5 consecutive times
            if count >= 5
                break;
            end
        else
            %reset counter
            count = 0;
        end

    end

    %if size(inSamples, 2) < 2 * obj.dim
    %    tunedGaussian = inputGaussian;
    %else
    %    tunedGaussian = Gaussian(newMean, newCov);
    %end

    tunedGaussian = Gaussian(newMean, newCov);
end
