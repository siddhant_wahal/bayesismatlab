function log_post = getLogPosterior(obj, x)

    d        = obj.getQOI(x);
    log_post = obj.likelihood.logpdf(d) + obj.optProblem.prior.logpdf(x);

end
