function full_normals = get_full_normals(obj, psi_r, psi_perp, reduced_normals)
% Projects the GaussianMixture in the reduced parameter space back up to the
% full parameter space
% Inputs:
% -- psi_r           : Leading eigenvectors of the expected prior preconditioned
%                      Gauss-Newton Hessian
% -- psi_perp        : Matrix so that [psi_r, psi_perp] forms a complete normal
%                      basis for the full parameter space
% -- reduced_normals : GaussianMixture in the reduced parameter space
% Outputs:
% -- full_normals : Projection of reduced_normals


    r = size(psi_r, 2);
    

    phi_r = obj.optProblem.prior.cholFactor * psi_r;
    xi_r  = obj.optProblem.prior.cholFactor' \ psi_r;

    phi_perp = obj.optProblem.prior.cholFactor * psi_perp;
    xi_perp  = obj.optProblem.prior.cholFactor' \ psi_perp;

    full_means = zeros(obj.dim, reduced_normals.nComp);
    full_covs  = zeros(obj.dim, obj.dim, reduced_normals.nComp);

    for i = 1 : reduced_normals.nComp

        full_means(:, i)   = phi_r * reduced_normals.components(i).meanVec ...
                             + phi_perp * xi_perp' * obj.optProblem.prior.meanVec;
        full_covs(:, :, i) = sparse(phi_r ...
                                    * reduced_normals.components(i).covMat ...
                                    * phi_r' ...
                             + phi_perp * phi_perp');
    end

    full_normals = GaussianMixture(full_means, full_covs, reduced_normals.weightArr);


end
