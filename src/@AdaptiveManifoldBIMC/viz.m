function viz(obj, plot_codes, varargin)

for i = 1 : length(plot_codes)

    code = plot_codes(i);

    switch code
    case obj.plot_codes.initialGuess


        initGuess = cell2mat(varargin(1));
        if obj.dim == 2
            
            figure(1)
            h1 = plot(initGuess(1), initGuess(2), '.r', 'markersize', 20);
            hold on
            axis equal
            pause(0.01)
            delete(h1)
        
        elseif obj.dim >= 3
            figure(1)
            h1 = plot(initGuess(1), initGuess(2), '.r', 'markersize', 20);
            hold on
            axis equal
            
            figure(2)
            h2 = plot(initGuess(2), initGuess(3), '.r', 'markersize', 20);
            hold on
            axis equal

            figure(3)
            h3 = plot(initGuess(3), initGuess(1), '.r', 'markersize', 20);
            hold on
            axis equal

            pause(0.01)
            delete(h1)
            delete(h2)
            delete(h3)

            %pause(0.01)
            %h1 = plot3(initGuess(1), initGuess(2), initGuess(3), '.r', 'markersize', 20);
            %hold on
            %axis equal
            %pause(0.01)
            %delete(h1)


        end


    case obj.plot_codes.fixedCharges


        for i = 1 : size(obj.fixedCharges, 2)
            if obj.dim == 2
                figure(1)
                %plot(obj.fixedCharges(1, i), obj.fixedCharges(2, i), ...
                %    'ok', 'markersize', 10);
                
                hold on
                axis equal

            elseif obj.dim >= 3
                figure(1)
                plot(obj.fixedCharges(1, i), ...
                     obj.fixedCharges(2, i), ...
                     'ok', 'markersize', 10);

                hold on
                axis equal

                figure(2)
                plot(obj.fixedCharges(2, i), ...
                     obj.fixedCharges(3, i), ...
                     'ok', 'markersize', 10);

                hold on
                axis equal


                figure(3)
                plot(obj.fixedCharges(3, i), ...
                     obj.fixedCharges(1, i), ...
                     'ok', 'markersize', 10);

                hold on
                axis equal

                %figure(1)
                %plot3(obj.fixedCharges(1, i), ...
                %      obj.fixedCharges(2, i), ...
                %      obj.fixedCharges(3, i), ...
                %     'ok', 'markersize', 10);

                %hold on
                %axis equal



            end
        
        end
        pause(0.01)

    case obj.plot_codes.componentCenters


        if obj.dim == 2
            figure(1)
            plot(obj.centers(1, end), obj.centers(2, end), ...
                '.b', 'markersize', 20);
            hold on
            axis equal 
            pause(0.01)
        elseif obj.dim >= 3
            
            %figure(1)
            %plot3(obj.centers(1, end), obj.centers(2, end), obj.centers(3, end), ...
            %     '.b', 'markersize', 20);
            %
            %hold on 
            %axis equal
            
            figure(1)
            plot(obj.centers(1, end), obj.centers(2, end), ...
                 '.b', 'markersize', 20);
            hold on
            axis equal 
            pause(0.01)


            figure(2)
            plot(obj.centers(2, end), obj.centers(3, end), ...
                 '.b', 'markersize', 20);
            hold on
            axis equal 
            pause(0.01)

            figure(3)
            plot(obj.centers(3, end), obj.centers(1, end), ...
                 '.b', 'markersize', 20);
            hold on
            axis equal 
            pause(0.01)
        end


    case obj.plot_codes.idealDistribution


        samples = obj.optProblem.prior.rnd(1e5);
        obs = obj.getQOI(samples);
        inside = obs < obj.limits(2) & obs > obj.limits(1);
        inSamples = samples(:, inside);

        if obj.dim == 2
            figure(1)
            scatter(inSamples(1, :), inSamples(2, :), ...
                    'o', ...
                    'MarkerFaceAlpha', 0.3, ...
                    'MarkerFaceColor', [0.7, 0.7, 0.7], ...
                    'MarkerEdgeColor', 'none');
            hold on 
            axis equal

        elseif obj.dim >= 3
            figure(1)
            scatter(inSamples(1, :), inSamples(2, :), ...
                    'o', ...
                    'MarkerFaceAlpha', 0.3, ...
                    'MarkerFaceColor', [0.7, 0.7, 0.7], ...
                    'MarkerEdgeColor', 'none');
            xlabel('$m_1$')
            ylabel('$m_2$')
            hold on
            axis equal

            figure(2)
            scatter(inSamples(2, :), inSamples(3, :), ...
                    'o', ...
                    'MarkerFaceAlpha', 0.3, ...
                    'MarkerFaceColor', [0.7, 0.7, 0.7], ...
                    'MarkerEdgeColor', 'none');
            xlabel('$m_2$')
            ylabel('$m_3$')

            hold on
            axis equal

            figure(3)
            scatter(inSamples(3, :), inSamples(1, :), ...
                    'o', ...
                    'MarkerFaceAlpha', 0.3, ...
                    'MarkerFaceColor', [0.7, 0.7, 0.7], ...
                    'MarkerEdgeColor', 'none');

            xlabel('$m_3$')
            ylabel('$m_1$')
            hold on 
            axis equal


            %hold on
            %axis equal
            %scatter3(inSamples(1, :), inSamples(2, :), inSamples(3, :), ...
            %        'o', ...
            %        'MarkerFaceAlpha', 0.1, ...
            %        'MarkerFaceColor', [0.7, 0.7, 0.7], ...
            %        'MarkerEdgeColor', 'none');
            %hold on
            %axis equal


        end


    case obj.plot_codes.selfKLDiv

        figure(4)
        semilogy(obj.normals.nComp, obj.selfPerplexityArr(end), '.b', 'MarkerSize', 20);
        xlabel('$n$')
        xlabel('$D_{\mathrm{KL}}$')
        hold on
        pause(0.01)

    case obj.plot_codes.optimizerState

        
        state = cell2mat(varargin(1));

        if obj.dim == 2
            figure(1)
            h1 = plot(state(1), state(2), '+r', 'MarkerSize', 10);

            hold on;
            axis equal;
            pause(0.01);
            delete(h1);
        else
            figure(1)
            h1 = plot(state(1), state(2), '+r', 'MarkerSize', 10);
            hold on
            axis equal
            
            figure(2)
            h2 = plot(state(2), state(3), '+r', 'MarkerSize', 10);
            hold on
            axis equal

            figure(3)
            h3 = plot(state(3), state(1), '+r', 'MarkerSize', 10);
            hold on
            axis equal
            
            pause(0.05)
            delete(h1)
            pause(0.05)
            delete(h2)
            pause(0.05)
            delete(h3)

            %figure(1)
            %h1 = plot3(state(1), state(2), state(3), '+r', 'MarkerSize', 10);
            %hold on
            %axis equal

            %pause(0.01)
            %delete(h1)

           %delete(h3)

        end

        
    end
end

end
