function createNormals(obj)
    %Constructs the IS mixture distribution. Depending on flags, this might just
    %involve loading from file, or constructing them from scratch.

    if obj.normalsExist == false
        %Load from file
        if obj.loadFinalMixture 
            load(obj.finalMixtureFilename, ...
                 'mixture', 'surrogates', 'data', 'noiseVar');
            obj.data       = data;
            obj.noiseVar   = noiseVar;
            obj.normals    = mixture;
            obj.surrogates = surrogates;
            
            obj.normalsExist = true;
        else
            %Initialize
            if obj.initialized == false
                obj.initialize();
            end

            %obj.adaptEP(nCharges);
            %Adaptively add components
            obj.adaptOneByOne();

            %Save mixture to file
            if obj.saveFinalMixture
                obj.checkpoint(obj.finalMixtureFilename);
            end
        end
    end
end
