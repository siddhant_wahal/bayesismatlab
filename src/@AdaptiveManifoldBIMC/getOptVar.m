function [var] = getOptVar(obj, x)

    linApprox = LinearizedEstimator(x, obj.optProblem.problem);
    
    if isa(obj.optProblem.prior, 'Gaussian')
        paramOpt = ParamsOptimizer(linApprox, obj.limits, obj.optProblem.prior);
    else
        paramOpt = ParamsOptimizer(linApprox, obj.limits, ...
                                   obj.optProblem.prior.equivalentGaussian);
    end

    var = paramOpt.getOptVar(obj.noiseVar, [], 1e-6, 1e-6);

end
