function tuneMPMC(obj, n_samples_per_iter, max_iter, wts_only)
% Tune mixture parameters using the M-PMC algorithm

    if obj.useTrueMapForCovTuning
        targetPDF = @(x) obj.targetPDF(x);
    else
        targetPDF = @(x) obj.targetPDFQuad(x);
    end

    mpmc                    = MPMC(targetPDF, obj.normals);
    mpmc.n_samples_per_iter = n_samples_per_iter;
    mpmc.max_iter           = max_iter;
    mpmc.display            = true;

    mpmc.run(wts_only);
    obj.normals = mpmc.mixture.deepCopy();
    obj.mpmcPerplexityArr = mpmc.norm_perp;
end
