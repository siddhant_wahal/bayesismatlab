function data = generateEMData_master(obj, data_size)

    nSamples = 20 * obj.dim;

    inSamples = [];
    
    sq_mahal_dist = zeros(nSamples, obj.normals.nComp);
    obsArr        = zeros(nSamples, 1);

    while size(inSamples, 2) < data_size


        samples = obj.optProblem.prior.rnd(nSamples);

        obsArr = obj.getQOI_NNQuad(samples);

        inside = obsArr < obj.limits(2) & obsArr > obj.limits(1);
        
        inSamples = [inSamples, samples(:, inside)];

    end

    data = inSamples;

end
