function reduced_normals = getReducedNormals(obj, psi_r, full_normals)
% Projects the GaussianMixture in the full parameter space down to the LIS base.
% Inputs:
% -- psi_r        : Leading eigenvectors of the expected prior preconditioned
%                   Gauss-Newton Hessian
% -- full_normals : GaussianMixture in the full parameter space
% Outputs:
% -- reduced_normals : LIS projection of full_normals

    r = size(psi_r, 2);

    phi_r = obj.optProblem.prior.cholFactor * psi_r;
    xi_r  = obj.optProblem.prior.cholFactor' \ psi_r;

    reduced_means = zeros(r, full_normals.nComp);
    reduced_covs  = zeros(r, r, full_normals.nComp);

    for i = 1 : full_normals.nComp

        reduced_means(:, i)   = xi_r' * full_normals.components(i).meanVec;
        reduced_covs(:, :, i) = sparse(xi_r' ...
                                * full_normals.components(i).covMat ...
                                * xi_r);
    end

    reduced_normals = GaussianMixture(reduced_means, ...
                                      reduced_covs, ...
                                      full_normals.weightArr)
end
