function [beta_min, beta_max] = getRegularizationParam(obj)
    
    map = obj.normals.components(1).meanVec;

    [~, jac] = obj.optProblem.problem.getQOI(map);

    grad_dir = jac' / myVecNorm(jac');

    null_grad = null(full(jac));

    rand_dir = null_grad * rand(size(null_grad, 2), 1);

    rand_dir = rand_dir / myVecNorm(rand_dir);

    [~, prior_grad_map] = obj.optProblem.prior.pdf(map);

    if rand_dir' * prior_grad_map > 0
        rand_dir = - rand_dir;
    end

    post_marginal_var = grad_dir' * obj.normals.components(1).covMat * grad_dir;
    sigma = sqrt(post_marginal_var);

    prior_marginal_var = rand_dir' * obj.optProblem.prior.covMat * rand_dir;
    gamma = sqrt(prior_marginal_var);
    
    %set near length scale based on posterior variance
    x_near = map + 1 * sigma * rand_dir;
    %set far length scale based on prior variance
    %but assumes that prior is isotropic
    %x_far  = map + 3 * gamma * rand_dir
    %so go back to using post variance
    x_far = map + 6 * sigma * rand_dir;

    [~, attr_grad_near] = obj.optProblem.prior.logpdf(x_near);
    attr_grad_near = - attr_grad_near;

    if obj.use_weighted_norm
        [~, repulsion_grad_near] = electrostaticPotentialGenDWeighted(...
                                        x_near, ...
                                        obj.fixedChargesMetrics, ...
                                        obj.fixedCharges, ...
                                        obj.exponent);
    else 
        [~, repulsion_grad_near] = electrostaticPotentialGenD(x_near, obj.fixedCharges, obj.exponent);
    end

    coeff_mat_near = [2 * attr_grad_near' * attr_grad_near, 2 * attr_grad_near' * jac'; 
                      2 * attr_grad_near' * jac', 2 * jac * jac'];
    rhs_near = [- 2 * repulsion_grad_near' * attr_grad_near; 
                - 2 * repulsion_grad_near' * jac'];

    sol_near = coeff_mat_near \ rhs_near;
    
    beta_max = sol_near(1);

    [~, attr_grad_far] = obj.optProblem.prior.logpdf(x_far);
    attr_grad_far = - attr_grad_far;

    if obj.use_weighted_norm
        [~, repulsion_grad_far] = electrostaticPotentialGenDWeighted(...
                                        x_far, ...
                                        obj.fixedChargesMetrics, ...
                                        obj.fixedCharges, ...
                                        obj.exponent);
    else 
        [~, repulsion_grad_far] = electrostaticPotentialGenD(x_far, obj.fixedCharges, obj.exponent);
    end


    coeff_mat_far = [2 * attr_grad_far' * attr_grad_far, 2 * attr_grad_far' * jac'; 
                     2 * attr_grad_far' * jac', 2 * jac * jac'];

    rhs_far = [- 2 * repulsion_grad_far' * attr_grad_far; 
               - 2 * repulsion_grad_far' * jac'];

    sol_far = coeff_mat_far \ rhs_far;
    
    beta_min = sol_far(1);


end
