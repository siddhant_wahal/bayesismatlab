function [varargout] = getQOI(obj, g)

    d = zeros(size(g, 2), 1);

    for i = 1 : size(g, 2)
        gg = g(:, i);
        [u_meas, u_scat, ~, invF] = obj.forward_problem(gg);
        %u_meas -> ntarget x ndir
        u_meas = u_meas(:);

        d(i) = u_meas' * u_meas;
    end

    varargout(1) = {d};

    if nargout > 1
        grad = real(obj.JTransposed(g, u_scat, invF, u_meas));
        varargout(2) = {2 * grad'};
    end



end % function
