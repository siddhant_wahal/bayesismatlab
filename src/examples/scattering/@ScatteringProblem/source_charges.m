function q = source_charges(flag,x,y)
% This function generates charges on the grid provided
% 
% OPTION:
% q = source_charges(flag,x,y);
%
% INPUT:
% flag -> domain type:
%         0 : point charge euqal to 1 at point x(5,5) and y(5,5).
% x -> NXN matrix with the value of the coordinate x of the point
% y -> NXN matrix with the value of the coordinate y of the point
%
% OUTPUT:
% q -> NXN matrix with the charge values at the domain
%
switch flag
    case 0
        
        q  = zeros(size(x));        
        nx = floor(size(x,1)/4);
        q(nx,nx)     = .2;
        q(3*nx,nx)   = .4;
        q(nx,3*nx)   = 0.1;
        q(3*nx,3*nx) =.3;
    case 1
        
        q = 0.15*exp(-(x.*x+y.*y)/1e-2);                
        
    case 2
        
        q = 100*(sin(2*pi*x*5).^2+sin(2*pi*y*5).^2>0.5);

    case 3
        
        q = 100*(sin(2*pi*x*3).^2+sin(2*pi*y*3).^2>0.5);

    case 4
        
        q = 100*(sin(2*pi*x*7).^2+sin(2*pi*y*7).^2>0.5);
        
    otherwise
        
        fprintf('Charge not defined!\n')
        
end
