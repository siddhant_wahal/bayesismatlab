function J = formJacobian(obj, g)

    [~, u_scat, ~, invF] = obj.forward_problem(g);
    u_tot = obj.u_inc + transpose(u_scat);
    J = obj.derivative_LS_matrix_fmm(g, u_tot, invF, eye(numel(g)));

end
