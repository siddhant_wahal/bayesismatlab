function w = JTransposed(obj, g, u_scat, invF, x)
% This function calculates the matrix of the derivative of the 
% forward operator times another matrix Y. It uses the FMM.
%
% OPTIONS :
%   J=derivative_LS_matrix_matvec(Jinfo,Y,flag,kh,nsource,source,target)
%   Provides the derivative matrix J*Y or J'Y or J'*J*Y where J is the
%   derivative
%
% INPUT :
%   kh        -> wavenumber
%   x_domain  -> 1XN vector with sources x-coordinates
%   y_domain  -> 1XN vector with sources y-coordinates
%   x_meas    -> 1XNt vector with detectors x-coordinates
%   y_meas    -> 1XNt vector with detectors y-coordinates
%   q_domain  -> 1XN vector with charge value at each source point
%   u_inc     -> NdXN vector with incident waves
%   u_scat    -> NxNd vector with scattered waves
%   x         -> 1xNt vector x to apply J^T 
%
% OUTPUT :
%   w       -> J^T x
%   

    nsource  = size(obj.x_domain,2);
    ntarget  = size(obj.x_meas,2);
    source   = [obj.x_domain;obj.y_domain];
    target   = [obj.x_meas;obj.y_meas];
    u_total  = obj.u_inc+transpose(u_scat);
    q_domain = g';
    q        = q_domain;
    ndir     = size(obj.u_inc,1);
    x = reshape(x, ntarget, ndir);
    
    w = zeros(nsource,1);

    LS_op = invF';
    for idir = 1:ndir

        %a1 = -obj.kh^2*u_total(idir,:)'.*conj(fmm_helmholtz_2d(obj.kh,ntarget,target,conj(x(:,idir)),nsource,source));        

        temp = obj.G_d' * x(:, idir);
        a1 = -obj.kh^2*u_total(idir,:)'.* temp;

        %setting mat-vec
        %LS_op = @(var)(obj.kh*obj.kh*q'.*conj(fmm_helmholtz_2d(obj.kh,nsource,source,var'))+var);

        %setting rhs to find a2
        %rhsa2 = q'.*conj(fmm_helmholtz_2d(obj.kh,ntarget,target,conj(x(:,idir)),nsource,source));        
        rhsa2 = q'.* temp;

        %solving the problem using
        %tol = 1e-10;  maxit = min(500, numel(g));; %restart_coef = 20;
        %[a2aux,~,~,iter] = gmres(LS_op,rhsa2,[],tol,maxit);                

        a2aux = LS_op * rhsa2;


        %finding a2
        a2 = obj.kh^4*u_total(idir,:)'.* conj(obj.G) * a2aux;
        %conj(fmm_helmholtz_2d(obj.kh,nsource,source,conj(a2aux)));        

        %solution
        w = w + a1 + a2; 

    end
    
return
