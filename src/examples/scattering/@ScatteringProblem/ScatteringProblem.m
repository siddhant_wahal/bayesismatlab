classdef ScatteringProblem < handle

    properties

        Ns
        x_domain;
        y_domain;
        kh;
        u_inc;
        x_meas;
        y_meas;
        xArr;
        yArr;
        G;
        G_d;
    
    end % properties
methods
        
        function obj = ScatteringProblem(kh, Ns)

            obj.Ns = Ns; % number of sources in each direction(used to set the 
                     % regular grid in the domain
            
            %regular discretization of the domain
            % the domain is [-1/2,1/2]^2;
            nsource = obj.Ns * obj.Ns;
            %Parameter dim  = obj.Ns * obj.Ns x 1

            src_t = -1/2:1/(obj.Ns-1):1/2; % Set number of points in each dimension in the domain
            [x,y] = meshgrid(src_t); % Setting points in the domain
            obj.xArr = x;
            obj.yArr = y;
            source = [x(:)';y(:)'];
            obj.x_domain = source(1, :);
            obj.y_domain = source(2, :);
                     
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % incoming plane wave parameters
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %obj.kh = 5*2*pi; % wavenumber
            obj.kh = kh;
            
            Nd = 4; % number of incoming directions 
            theta = 0:2*pi/Nd:2*pi-2*pi/Nd; % setting vector with directions  
            
            % generating incident wave
            obj.u_inc = exp(1i*obj.kh*(bsxfun(@times,cos(theta'),x(:)') + ...
                bsxfun(@times,sin(theta'),y(:)')));
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Receptors positions
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            R = 3; % radius of circle with measurements
            Nt = 100; % number of targets
            
            %setting target points
            ntarget = Nt;
            trg_t = 0:2*pi/Nt:2*pi-2*pi/Nt;
            target = R*[cos(trg_t);sin(trg_t)];
            obj.x_meas = target(1, :);
            obj.y_meas = target(2, :);

            obj.G       = 1i/4*greengardrokhlinhank106(obj.kh*sqrt(bsxfun(@minus,obj.x_domain',obj.x_domain).^2+bsxfun(@minus,obj.y_domain',obj.y_domain).^2));        
            obj.G(1:nsource+1:nsource*nsource) = 0;    

            obj.G_d = 1i/4*greengardrokhlinhank106(obj.kh*sqrt(bsxfun(@minus,obj.x_meas',obj.x_domain).^2+bsxfun(@minus,obj.y_meas',obj.y_domain).^2));                        
        end


        [varargout] = getQOI(obj, g);
        [jac] = getJacobian(obj, g);
        J = formJacobian(obj, g);
        [u_meas, u_scat, F, invF] = forward_problem(obj, g);
        [u_meas, u_scat] = forward_problem_fmm(obj, g);
        [jHx] = JTransposed(obj, g, u_scat, invF, x);
        [Jx] = derivative_LS_matrix_fmm(obj, q,u_total,invF, Y)
        [Hx] = getHess_mv(obj, g, x);
        H = getHess(obj, g);
    end % methods

end % classdef
