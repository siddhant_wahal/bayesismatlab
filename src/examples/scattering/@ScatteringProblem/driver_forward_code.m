clear all
% setting parameters

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Setting the domain
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Ns = 32; % number of sources in each direction(used to set the 
         % regular grid in the domain

%regular discretization of the domain
% the domain is [-1/2,1/2]^2;
nsource = Ns*Ns;
src_t = -1/2:1/(Ns-1):1/2; % Set number of points in each dimension in the domain
[x,y] = meshgrid(src_t); % Setting points in the domain
source = [x(:)';y(:)'];
         
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% incoming plane wave parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
kh = 5*2*pi; % wavenumber

Nd = 4; % number of incoming directions 
theta = 0:2*pi/Nd:2*pi-2*pi/Nd; % setting vector with directions  

% generating incident wave
u_inc = exp(1i*kh*(bsxfun(@times,cos(theta'),x(:)') + ...
    bsxfun(@times,sin(theta'),y(:)')));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Receptors positions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
R = 3; % radius of circle with measurements

Nt = 100; % number of targets

%setting target points
ntarget = Nt;
trg_t = 0:2*pi/Nt:2*pi-2*pi/Nt;
target = R*[cos(trg_t);sin(trg_t)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sound profile
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This is the domain that we have the measurements
type_domain = 1;
% flag for the function source_charges
                 % 0 - > charge 1 at (x(5,5),y(5,5))
                 % 1 -> gaussian function 
                 % 2 -> photonic crystal
                 % 3 -> photonic crystal


% setting charge values - > q values at the points
q = source_charges(type_domain,x,y);
charge = transpose(q(:));

%checking the code
fprintf('Test forward operator!\n')
fprintf('Calculating using matrix...\n')
tic
[u_meas,u_scat] = forwad_problem(kh,source(1,:),source(2,:),charge,u_inc,target(1,:),target(2,:));
toc

fprintf('Calculating using mat-vec...\n')
tic
[u_meas_mv,u_scat_mv] = forwad_problem_fmm(kh,source(1,:),source(2,:),charge,u_inc,target(1,:),target(2,:));
toc

for idir = 1 : Nd
    
    fprintf('\nFor direction %d\n',idir)
    fprintf('Error_scat is %d\n',norm(u_meas_mv(:,idir)-u_meas(:,idir))/norm(u_meas_mv(:,idir)))

end

fprintf('Test DF/Dq^Td!\n')
d = u_meas;
Jtx_mv = JTransposed(kh,source(1,:),source(2,:),target(1,:),target(2,:),charge,u_inc,u_scat,d);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%this minimal code caculates w= J^Td
G_d = 1i/4*greengardrokhlinhank106(kh*sqrt(bsxfun(@minus,target(1,:)',source(1,:)).^2 + ...
    bsxfun(@minus,target(2,:)',source(2,:)).^2));
G   = 1i/4*greengardrokhlinhank106(kh*sqrt(bsxfun(@minus,source(1,:)',source(1,:)).^2 + ...
    bsxfun(@minus,source(2,:)',source(2,:)).^2));
G(1:Ns*Ns+1:end) = 0;
F   = eye(Ns*Ns)+kh^2*G*diag(charge);
u_total = u_inc + transpose(u_scat);
J   = zeros(Nt,Ns*Ns);
Jtx   = zeros(Ns*Ns,1);

for ii = 1 : Nd
    J   = -kh^2*bsxfun(@times,G_d,u_total(ii,:)) + ...
    -kh^2*bsxfun(@times,G_d,transpose(q(:)))*(F\(-kh^2*bsxfun(@times,G,u_total(ii,:))));
    Jtx = Jtx + J'*d(:,ii);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Error Jtx is %d\n',norm(Jtx_mv-Jtx)/norm(Jtx))

x = randn(Ns*Ns,2);
Jx_mv = derivative_LS_matrix_fmm(kh,source(1,:),source(2,:),target(1,:),target(2,:),charge,u_inc + transpose(u_scat),x);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%this minimal code caculates w= Jx
% note that it reuses previously created matrices
Jx = zeros(Nt*Nd,size(x,2));
for ii = 1 : Nd
    J   = -kh^2*bsxfun(@times,G_d,u_total(ii,:)) + ...
    -kh^2*bsxfun(@times,G_d,transpose(q(:)))*(F\(-kh^2*bsxfun(@times,G,u_total(ii,:))));
    Jx((ii-1)*Nt+1:ii*Nt,:) = J*x;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Error Jx is %d\n',norm(Jx_mv-Jx)/norm(Jx))
