function [H] = getHess_mv(obj, g, x)
    J = obj.formJacobian(g);
    H = 2 * real(J' * J );
end
