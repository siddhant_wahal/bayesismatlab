function [Hx] = getHess_mv(obj, g, x)
    %Return H_GN * x, where H_GN is the Gauss Newton Hessian of the
    %qoi-to-parameter map. 
    Hx = zeros(numel(g), size(x, 2));
    J = obj.formJacobian(g);

    Hx = 2 * real(J' * (J * x));
    %[~, u_scat,~, invF] = obj.forward_problem(g);
    %u_tot = obj.u_inc + transpose(u_scat);
    %Jx = obj.derivative_LS_matrix_fmm(g, u_tot, invF, x);

    %
    %parfor i = 1 : size(Jx, 2)
    %    JtJxx       = obj.JTransposed(g, u_scat, invF, Jx(:, i));
    %    Hxx         = 2 * real(JtJxx);
    %    Hx(:, i)    = Hxx;
    %end

end
