function [u_meas,u_scat] = forward_problem_fmm(obj, g)
% This function calculates the field at the detectors and at the domain
% solving directly the system for the Lippmann-Schwinger equation.
%
% OPTIONS :
%   [u_meas,u_scat] = forwad_problem_fmm(kh,x_domain,y_domain,q_domain,u_inc,x_meas,y_meas)
%   Provides the scattered field u_scat on the source points and the scattered
%   field u_meas on the detector points
%
% INPUT :
%   kh      -> wavenumber
%   x_domain-> 1XN vector with x-coordinates of the domain points
%   y_domain-> 1XN vector with y-coordinates of the domain points
%   q_domain-> 1XN vector with charge value at each source point
%   u_inc   -> NdXN vector with incident waves
%   x_meas  -> 1XNd vector with x-coordinates of detectors
%   y_meas  -> 1XNd vector with y-coordinates of detectors
% OUTPUT :
%   u_scat  -> scattered field on the points of the domain
%   u_meas  -> measured field on detecor points
%    

    %setting matrices and solving the problem
    nsource = size(obj.x_domain,2);
    ntarget = size(obj.x_meas,2);
    ndir    = size(obj.u_inc,1);
    u_scat  = zeros(nsource,ndir);
    u_meas  = zeros(ntarget,ndir);
    q_domain = g';
    source  = [obj.x_domain;obj.y_domain];
    target  = [obj.x_meas;obj.y_meas];
        
    %setting mat-vec
    LS_op = @(var)(obj.kh*obj.kh*fmm_helmholtz_2d(obj.kh,nsource,source,q_domain.*transpose(var))+var);

    %solving the problem using 
    tol = 1e-10;  maxit = min(500, numel(g));
    
    for idir = 1:ndir
        rhs    = -obj.kh^2*fmm_helmholtz_2d(obj.kh,nsource,source,q_domain.*obj.u_inc(idir,:));        
        [u_scat(:,idir),~,~,iter] = gmres(LS_op,rhs,[],tol,maxit);    
        %fprintf('Number of iterations for direction %d is %d ...\n',idir,iter(2))
    end
    
    %calculating the value of the domain at the target                    
    for idir = 1 : ndir
        
        aux = q_domain.*obj.u_inc(idir,:)+q_domain.*transpose(u_scat(:,idir));
        u_meas(:,idir) = -obj.kh^2*fmm_helmholtz_2d(obj.kh,nsource,source,aux,ntarget,target);
        
    end
return
