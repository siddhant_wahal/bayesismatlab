function w = derivative_LS_matrix_fmm(obj, g,u_total, invF,Y)
% This function calculates the matrix of the derivative of the 
% forward operator times another matrix Y. It uses the FMM.
%
% OPTIONS :
%   J=derivative_LS_matrix_fmm(kh,x_source,y_source,x_target,y_target,q_domain,u_total)
%   Provides the derivative matrix J*Y where J is the derivative
%
% INPUT :
%   Y       -> matrix to be multiplied
%   kh      -> wavenumber
%   x_source  -> 1XN vector with sources x-coordinates
%   y_source  -> 1XN vector with sources y-coordinates
%   x-target  -> 1XNd vector with detectors x-coordinates
%   y-target  -> 1XNd vector with detectors y-coordinates
%   u_total   -> NdXN vector with total field, this should be
%                u_total = u_inc + tranpose(u_scat);
%   q         -> value of the sound profile in the domain
%
% OUTPUT :
%   w       -> matrix-matrix product JY
%
    
    q = g';
    source  = [obj.x_domain;obj.y_domain];
    nsource = size(source,2);
    
    target  = [obj.x_meas;obj.y_meas];
    ntarget = size(target,2);
       
    ndir = size(u_total,1);
    ncol = size(Y,2);
        
    w = zeros(ndir*ntarget,ncol);                
    temp = zeros(ntarget, ndir, ncol);


    LS_op = invF;

    for icol = 1 : ncol

        xaux = Y(:,icol);        

        for idir = 1:ndir

            a1 = -obj.kh^2*fmm_helmholtz_2d(obj.kh,nsource,source,transpose(u_total(idir,:)).*xaux,ntarget,target);        

            %setting mat-vec
            %LS_op = @(var)(obj.kh*obj.kh*fmm_helmholtz_2d(obj.kh,nsource,source,q.*transpose(var))+var);

            %setting rhs to find a2 
            rhsa2 = -obj.kh^2*fmm_helmholtz_2d(obj.kh,nsource,source,transpose(u_total(idir,:)).*xaux);        

            %solving the problem using 
            %tol = 1e-10;  maxit = min(500, numel(g));        
            %[a2aux,~,~,iter] = gmres(LS_op,rhsa2,[],tol,maxit);                        
%                 fprintf('For direction %d nr iter = %d\n', idir, iter);

            a2aux = LS_op * rhsa2;

            %finding a2
            a2 = -obj.kh^2*fmm_helmholtz_2d(obj.kh,nsource,source,q.*transpose(a2aux),ntarget,target);        

            %solution
            %w((idir-1)*ntarget+1:idir*ntarget,icol) = a1 + a2; 

            temp(:, idir, icol) = a1 + a2;
        end
    end
    
    for icol = 1 : ncol
        for idir = 1 : ndir
            w((idir-1)*ntarget+1:idir*ntarget,icol) = temp(:, idir, icol);
        end
    end
return
