function [u_meas,u_scat, F, invF] = forward_problem(obj, g)
% This function calculates the field at the detectors and at the domain
% solving directly the system for the Lippmann-Schwinger equation.
%
% OPTIONS :
%   [u_s,flag,relres,iter]=LS_solution_direct(kh,source,charge,u_inc)
%   Provides the scattered field u_s on the source points
%
%   [u_s,flag,relres,iter,u_d]=LS_solution_direct(kh,source, ...
%   ...,charge,u_inc,target)
%   Provides the scattered field u_s on the source points and the scattered
%   field u_d on the detector points
%
% INPUT :
%   kh      -> wavenumber
%   x_domain-> 1XN vector with x-coordinates of the domain points
%   y_domain-> 1XN vector with y-coordinates of the domain points
%   q_domain-> 1XN vector with charge value at each source point
%   u_inc   -> NdXN vector with incident waves
%   x_meas  -> 1XNd vector with x-coordinates of detectors
%   y_meas  -> 1XNd vector with y-coordinates of detectors
% OUTPUT :
%   u_scat  -> scattered field on the points of the domain
%   u_meas  -> measured field on detecor points
%    

    %setting matrices and solving the problem
    nsource = size(obj.x_domain,2);
    ntarget = size(obj.x_meas,2);
    ndir    = size(obj.u_inc,1);
    u_scat  = zeros(nsource,ndir);
    u_meas  = zeros(ntarget,ndir);
    q_domain = g';
    
    F      = (speye(nsource))+obj.kh^2*bsxfun(@times,obj.G,q_domain);%forward operator                

    invF = F \ speye(nsource);
    
    for idir = 1:ndir
        rhs    = -obj.kh^2*obj.G*transpose(obj.u_inc(idir,:).*q_domain);
        u_scat(:,idir) = invF * rhs;   
    end
    
    %calculating the value of the domain at the target                
    for idir = 1 : ndir
        u_meas(:,idir) = -obj.kh^2*obj.G_d*transpose((obj.u_inc(idir,:)+transpose(u_scat(:,idir))).*q_domain);
    end
return
