function [jac] = getJacobian(obj, g)

    [u_meas, u_scat, F, invF] = obj.forward_problem(g);

    grad = real(obj.JTransposed(g, u_scat, invF, u_meas));

    jac = 2 * grad';

end
