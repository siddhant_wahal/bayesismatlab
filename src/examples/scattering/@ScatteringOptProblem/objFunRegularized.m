function [varargout] = objFunRegularized(obj, g, target, noiseVar)

    if nargout == 1
        d        = obj.problem.getQOI(g);
    elseif nargout > 1
        [d, jac] = obj.problem.getQOI(g);
    end
    
    misfit       = (target - d) * (target - d) / (2 * noiseVar);
    reg          = - obj.prior.logpdf(g);
    val          = misfit + reg;
    varargout(1) = {val};
    
        if nargout > 1
    
            misfit_grad   = - 1.0 / noiseVar * (target - d) * jac';
            [~, reg_grad] = obj.prior.logpdf(g);
            reg_grad      = - reg_grad;
            grad          = misfit_grad + reg_grad;

            varargout(2)  = {grad};
    
            if nargout > 2
    
                misfit_hess_GN   = 1.0 / noiseVar * jac' * jac;
                [~, ~, reg_hess] = obj.prior.logpdf(g);
                reg_hess         = - reg_hess;
                hess_GN          = misfit_hess_GN + reg_hess;
    
                if ~obj.useGNHess
                    obj.options.HessianMultiplyFcn = @(Hinfo, Y) obj.hess_mv(...
                                                        Hinfo, Y, g, ...
                                                        target, d, noiseVar);
                end
                varargout(3) = {hess_GN};
            end
        end
    
end
