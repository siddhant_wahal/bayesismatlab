classdef ScatteringOptProblem < handle
    properties
        problem;
        prior;
        useGNHess;
        test = [];
        options = optimoptions(@fminunc);
    end
    methods
        function obj = ScatteringOptProblem(problem, prior)
            
            obj.problem   = problem;
            obj.prior     = prior;
            obj.useGNHess = true;

            %fminunc options
            obj.options.Algorithm                = 'trust-region';
            obj.options.Display                  = 'iter';
            obj.options.SpecifyObjectiveGradient = true;
            obj.options.HessianFcn               = 'objective';
            obj.options.OptimalityTolerance      = 1e-5;
            obj.options.StepTolerance            = 1e-5;
            obj.options.FunctionTolerance        = 1e-5;
            obj.options.DerivativeCheck          = 'off';
            obj.options.FinDiffType              = 'central'
            obj.options.MaxFunEvals              = 500;
            obj.options.TypicalX                 = prior.meanVec;
        end

        [varargout]       = objFunRegularized(obj, g, target, noiseVar);
        [d]               = getQOI(obj, g);
        [map, varargout]  = getMAP(obj, target, g0, noiseVar);
        [HY]              = hess_mv(obj, Hinfo, Y, g, target, d, noiseVar)
        [hess, varargout] = getGNHess(obj, g, noiseVar);
    end




end
