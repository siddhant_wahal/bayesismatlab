function [hess, varargout] = getGNHess(obj, g, noiseVar)
    
    jac         = obj.problem.getJacobian(g);
    misfit_hess = 1.0 / noiseVar * jac' * jac;
    
    [~, ~, reg_hess] = obj.prior.logpdf(g);
    reg_hess         = - reg_hess;
    
    hess = misfit_hess + reg_hess;

    if nargout > 1
        if isa(obj.prior, 'Gaussian')
            inv_reg_hess = obj.prior.covMat;
        else
            inv_reg_hess = inv(reg_hess);
        end
        
        invHess = inv_reg_hess - 1 / (noiseVar + jac * inv_reg_hess * jac') ...
                                     * (inv_reg_hess * jac') ...
                                     * (inv_reg_hess * jac')';
        varargout(1) = {invHess};
    end

end
