function [map, varargout] = getMAP(obj, target, g0, noiseVar)
    
    if ~obj.useGNHess
        obj.options.HessianMultiplyFcn = @(Hinfo, Y) ...
                            obj.hess_mv(Hinfo, Y, g0, ...
                                        0, 0, 1);
    end

    [map, ~, ~, output] = fminunc(@(g)obj.objFunRegularized(g, target, noiseVar), ...
                                  g0, obj.options);

    if nargout > 1
        varargout(1) = {output.funcCount};
    end

end
