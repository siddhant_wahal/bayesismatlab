function HY = hess_mv(obj, Hinfo, Y, g, target, d, noiseVar)

    HY = Hinfo * Y ...
         -(1.0 / noiseVar) * (target - d) * obj.problem.getHess_mv(g, Y);

end
