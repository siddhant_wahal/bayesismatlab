function out=fmm_helmholtz_2d(kh,nsource,source,charge,ntarget,target)
% nsource  -> number N of sources
% source   -> matrix 2XN of source coordinates
% charge   -> vector 1XN with charge values
% ntarget  -> number M of targets
% target   -> matrix 2XM with target coordinates

%setting other flags
ifcharge=1;
ifdipole=0;
dipstr = zeros(1,nsource)+1i*zeros(1,nsource);
dipvec = zeros(2,nsource);
iprec=4;

%calling the FMM
switch nargin
    case 4
        %setting flags
        ifpot = 1;
        ifgrad = 0;
        ifhess = 0;

        U=hfmm2dpart(iprec,kh,nsource,source,ifcharge,charge,ifdipole,dipstr,dipvec,ifpot,ifgrad,ifhess);        
        
        out=transpose(U.pot);

    case 6
        %setting flags
        ifpot = 0;
        ifgrad = 0;
        ifhess = 0;
        ifpottarg = 1;
        ifgradtarg = 0;
        ifhesstarg = 0;
        
        U=hfmm2dpart(iprec,kh,nsource,source,ifcharge,charge,ifdipole,dipstr,dipvec,ifpot,ifgrad,ifhess,ntarget,target,ifpottarg,ifgradtarg,ifhesstarg);
        
        out=transpose(U.pottarg);
        
    otherwise
        fprintf('Wrong number of arguments!\n')
        out=0;
end

