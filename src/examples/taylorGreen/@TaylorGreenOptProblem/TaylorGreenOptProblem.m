classdef TaylorGreenOptProblem
    properties
        problem
        prior
        options = optimoptions(@fminunc)
    end
    methods
        function obj = TaylorGreenOptProblem(problem, prior)
          obj.problem = problem;
            obj.prior = prior;
            obj.options.Algorithm                = 'trust-region';
            obj.options.Display                  = 'off';
            obj.options.SpecifyObjectiveGradient = true;
            obj.options.HessianFcn               = 'objective';
            obj.options.FunctionTolerance        = 1e-6;
            obj.options.OptimalityTolerance      = 1e-6;
            obj.options.StepTolerance            = 1e-6;
            obj.options.DerivativeCheck          = 'off';
            obj.options.FinDiffType              = 'central'
            obj.options.MaxFunEvals              = 500;
          end

        [varargout] = objFunRegularized(obj, g, target, noiseVar);
        [varargout] = objFunMisfit(obj, g, target);
        [d] = getQOI(obj, g);
        [map, varargout] = getMAP(obj, target, g0, noiseVar);
        [hess] = getHess(obj, g, target, noiseVar);
        [hess, varargout] = getGNHess(obj, g, noiseVar);
    end




end
