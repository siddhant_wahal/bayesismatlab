function [map, varargout] = getMAP(obj, target, g0, noiseVar)
    
    [map, ~, ~, output] = fminunc(@(g)obj.objFunRegularized(g, ...
                                                             target, ...
                                                             noiseVar), ...
                                                            g0, obj.options);

    if nargout > 1
        varargout(1) = {output.funcCount};
    end

end
