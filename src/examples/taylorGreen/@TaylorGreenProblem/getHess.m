function [hess] = getHess(obj, g)
    hess = [-sin(g(1)) * cos(g(2)), -cos(g(1)) * sin(g(2));...
            -cos(g(1)) * sin(g(2)), -sin(g(1)) * cos(g(2))];
end
