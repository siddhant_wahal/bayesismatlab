function [varargout] = getQOI(obj, g)
    n = size(g, 2);

    d = (sin(g(1, :)) .* cos(g(2, :)))';

    varargout(1) = {d};

    if nargout > 1

        jac = [cos(g(1)) * cos(g(2)), -sin(g(1)) * sin(g(2))];

        varargout(2) = {jac};
    end
end
