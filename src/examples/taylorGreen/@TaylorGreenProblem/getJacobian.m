function [jac] = getJacobian(obj, g)
    jac = [cos(g(1)) * cos(g(2)), -sin(g(1)) * sin(g(2))];
end
