function [grad] = getGrad(obj, g)
    grad = [cos(g(1)) * cos(g(2)); -sin(g(1)) * sin(g(2))];
end
