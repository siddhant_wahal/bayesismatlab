classdef TaylorGreenProblem
    methods
        [varargout] = getQOI(obj, g);
        [jac] = getJacobian(obj, g);
        [grad] = getGrad(obj, g);
        [hess] = getHess(obj, g);
    end
end




