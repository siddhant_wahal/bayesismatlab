function [hess_mat] = get_hessian(obj, x, target, noiseVar, poly_u, varargin)

%get final state from polynomial
u_tf = ppval(poly_u, obj.problem.tFinal);

%final value of adjoint variable, depends on whether full or partial observations were made
p_tf = -(obj.problem.obsOp' * u_tf - target) * obj.problem.obsOp;

if isempty(varargin)
  [~, tp, p] = obj.problem.solve_adj_eq(poly_u, p_tf);
  poly_p = pchip(tp, p');
else 
  poly_p = varargin{1};
end


I = eye(3);
e1 = I(:, 1);
e2 = I(:, 2);
e3 = I(:, 3);

e_fused = [e1; e2; e3];
%solve incremental forward and adjoint equations 
%inc_fwd and inc_adj eqs are linear. Fuse evaluation of each
%column of hessian into one big vector
[~, tubar, ubar] = obj.problem.solve_inc_fwd_eq(poly_u, e_fused);

%interpolate inc forward var
poly_ubar = pchip(tubar, ubar');

%set terminal condtion for inc adj var, depends on whether full or partial observations were made
pbar_tf = [-(ubar(end, 1 : 3) * obj.problem.obsOp) * obj.problem.obsOp; -(ubar(end, 4 : 6) * obj.problem.obsOp) * obj.problem.obsOp; -(ubar(end, 7 : 9) * obj.problem.obsOp) * obj.problem.obsOp];

pbar_tf = pbar_tf / noiseVar;
%solve inc adj eq
pbar = obj.problem.solve_inc_adj_eq(poly_u, poly_p, poly_ubar, pbar_tf);

%construct hessian
hess_reg = obj.prior.precisionMat;
hess_misfit = -[pbar(1 : 3) pbar(4 : 6) pbar(7 : 9)];

hess_mat = hess_reg + hess_misfit; 
hess_mat = 0.5 * (hess_mat + hess_mat');

end
