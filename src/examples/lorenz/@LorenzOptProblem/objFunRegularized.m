function [varargout] = objFunRegularized(obj, x, target, noiseVar)

%get final value and time series

[u_tf tu u] = obj.problem.solve_state_eq(x);

%interpolate, store polynomial data in object
poly_u = pchip(tu, u');

x_prime = x;

%check if full or partial observations are being used
val = 0.5 * (obj.problem.obsOp' * u_tf - target)^2 / noiseVar ...
        + 0.5 * obj.prior.sqMahal(x_prime);
varargout(1) = {val};

if nargout > 1
  [grad poly_p] = obj.get_grad(x, target, noiseVar, poly_u) ;
  varargout(2) = {grad};
  if nargout > 2
    hess = obj.get_hessian(x, target, noiseVar, poly_u, poly_p);
    varargout(3) = {hess};
  end
end

end

