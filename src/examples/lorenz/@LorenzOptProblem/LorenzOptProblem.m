classdef LorenzOptProblem 
    properties
        problem
        prior
        options = optimoptions(@fminunc)
    end

    methods
        function obj = LorenzOptProblem(problem, prior)
            obj.problem = problem;
            obj.prior = prior;
            obj.options.Algorithm = 'trust-region';
            obj.options.GradObj = 'on';
            obj.options.Display = 'iter';
            obj.options.Hessian = 'user-supplied';
            obj.options.TolFun = 1e-8;
            obj.options.TolX = 1e-8;
            obj.options.MaxFunEvals = 500;
            obj.options.DerivativeCheck = 'off';
        end

        [varargout] = objFunRegularized(obj, g, target, noiseVar);
        [grad varargout] = get_grad(obj, x, target, noiseVar, poly_u)
        [hess_mat] = get_hessian(obj, x, target, noiseVar, poly_u, varargin)
        [d] = getQOI(obj, g);
        [map, varargout] = getMAP(obj, target, g0, noiseVar);
        [hess] = getHess(obj, g, target, noiseVar);
        [hess, varargout] = getGNHess(obj, g, noiseVar);
    end
end




