function [hess] = getHess(obj, g, target, noiseVar)
    [~, ~, hess] = obj.objFunRegularized(g, target, noiseVar);
end
