function [grad varargout] = get_grad(obj, x, target, noiseVar, poly_u)

%extract final state from polynomial
u_tf = ppval(poly_u, obj.problem.tFinal);

%final value of adjoint variable, depends on whether full or partial observations were made
p_tf = -(obj.problem.obsOp' * u_tf - target) * obj.problem.obsOp / noiseVar;

%solve adjoint equation, get p0 and time series
[p0, tp, p] = obj.problem.solve_adj_eq(poly_u, p_tf);

%interpolate p using hermite polynomials
poly_p = pchip(tp, p');

%gradient
grad_misfit = -p0;

grad_reg = (obj.prior.precisionMat) * (x - obj.prior.meanVec);
grad = grad_reg + grad_misfit;

if nargout > 1
  varargout(1) = {poly_p};
end

end
