function [hess, varargout] = getGNHess(obj, g, noiseVar)
    jac = obj.problem.getJacobian(g);
    hess = 1.0 / noiseVar * jac' * jac + obj.prior.precisionMat;
    hess = 0.5 * (hess + hess');

    if nargout > 1
        invHess = obj.prior.covMat - 1 / (noiseVar + jac * obj.prior.covMat * jac') ...
                                     * (obj.prior.covMat * jac') ...
                                     * (obj.prior.covMat * jac')';
        varargout(1) = {invHess};
    end

end
