classdef LorenzProblem 
    properties
        odeOptions = odeset('RelTol', 1e-12, 'AbsTol', 1e-12, ...
                            'Vectorized', 'on');
        obsOp = [1; 0; 0];

        P = 10;
        R = 28;
        b = 8 / 3.0;

        tFinal

    end

    methods
        function obj = LorenzProblem(tFinal)
            obj.tFinal = tFinal;
        end

        [dg] = rhsFwd(obj, t, g);
        [dp] = rhsAdj(obj, t, g, poly_u);
        [dg] = rhsIncFwd(obj, t, g, poly_u);
        [dp] = rhsIncAdj(obj, t, p, poly_u, poly_b, poly_p, poly_ubar);
        [uFinal, varargout] = solve_state_eq(obj, g);

        [final_state_vec, varargout] = solve_adj_eq(obj, ...
                                                    poly_u, final_cond);

        [final_state_vec varargout] = solve_inc_fwd_eq(obj, ...
                                                    poly_u, init_cond);

        [final_state_vec, varargout] = solve_inc_adj_eq(obj, ...
                                                    poly_u, poly_p, ...
                                                    poly_ubar, final_cond);

        [qoi] = getQOI(obj, g);
        [jac] = getJacobian(obj, g);
        [hess] = getHess(obj, g);
    end
end
