function [qoiArr] = getQOI(obj, gArr)

n = size(gArr, 2);
dim = size(gArr, 1);

qoiArr = zeros(n, 1);

for i = 1 : n
    g = gArr(:, i);
    uFinal = obj.solve_state_eq(g);
    qoiArr(i) = obj.obsOp' * uFinal;
end
