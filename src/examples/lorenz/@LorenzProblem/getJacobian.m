function [jac, varargout] = getJacobian(obj, x)

%extract final state from polynomial
[u_tf, tu, u] = obj.solve_state_eq(x);
poly_u = pchip(tu, u');

%final value of adjoint variable, depends on whether full or partial observations were made
p_tf = - obj.obsOp;

%solve adjoint equation, get p0 and time series
[p0, tp, p] = obj.solve_adj_eq(poly_u, p_tf);

jac = -p0';
end
