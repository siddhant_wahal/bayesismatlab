function [dy] = rhsFwd(obj, t, y)

dy = zeros(size(y));

dy(1, :) = obj.P * (y(2, :) - y(1, :));
dy(2, :) = obj.R * y(1, :) - y(2, :) - y(1, :) .* y(3, :);
dy(3, :) = y(1, :) .* y(2, :) - obj.b * y(3, :);

end
