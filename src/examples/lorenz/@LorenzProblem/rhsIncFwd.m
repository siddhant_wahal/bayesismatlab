function [dv] = rhsIncFwd(obj, t, v, poly_u)

dv = zeros(size(v));

u = ppval(poly_u, t);

%Evaluation of three columns of hessian fused into one vector solve

dv(1, :) = -obj.P * v(1, :) + obj.P * v(2, :);
dv(2, :) = (obj.R - u(3)) * v(1, :) - v(2, :) - u(1) * v(3, :);
dv(3, :) = u(2) * v(1, :) + u(1) * v(2, :) - obj.b * v(3, :);

dv(4, :) = -obj.P * v(4, :) + obj.P * v(5, :);
dv(5, :) = (obj.R - u(3)) * v(4, :) - v(5, :) - u(1) * v(6, :);
dv(6, :) = u(2) * v(4, :) + u(1) * v(5, :) - obj.b * v(6, :);

dv(7, :) = -obj.P * v(7, :) + obj.P * v(8, :);
dv(8, :) = (obj.R - u(3)) * v(7, :) - v(8, :) - u(1) * v(9, :);
dv(9, :) = u(2) * v(7, :) + u(1) * v(8, :) - obj.b * v(9, :);

end
