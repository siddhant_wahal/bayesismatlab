function [dp] = rhsIncAdj(obj, t, p, poly_u, poly_p, poly_ubar)

dp = zeros(size(p));
%Evaluation of all three columns of hessian fused into one big vector evaluation
u = ppval(poly_u, obj.tFinal - t);
q = ppval(poly_p, t);
ubar = ppval(poly_ubar, obj.tFinal - t);

dp(1, :) = -obj.P * p(1, :) + (obj.R - u(3)) * p(2, :) + u(2) * p(3, :) + ubar(2) * q(3) - ubar(3) * q(2);
dp(2, :) = obj.P * p(1, :) - p(2, :) + u(1) * p(3, :) + ubar(1) * q(3);
dp(3, :) = - u(1) * p(2, :) - obj.b * p(3, :) - ubar(1) * q(2);

dp(4, :) = -obj.P * p(4, :) + (obj.R - u(3)) * p(5, :) + u(2) * p(6, :) + ubar(5) * q(3) - ubar(6) * q(2);
dp(5, :) = obj.P * p(4, :) - p(5, :) + u(1) * p(6, :) + ubar(4) * q(3);
dp(6, :) = - u(1) * p(5, :) - obj.b * p(6, :) - ubar(4) * q(2);

dp(7, :) = -obj.P * p(7, :) + (obj.R - u(3)) * p(8, :) + u(2) * p(9, :) + ubar(8) * q(3) - ubar(9) * q(2);
dp(8, :) = obj.P * p(7, :) - p(8, :) + u(1) * p(9, :) + ubar(7) * q(3);
dp(9, :) = - u(1) * p(8, :) - obj.b * p(9, :) - ubar(7) * q(2);


end
