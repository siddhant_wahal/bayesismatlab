function [final_state_vec, varargout] = solve_state_eq(obj, s0)

[t, y] = ode45(@(t, y) obj.rhsFwd(t, y), [0 obj.tFinal], s0, obj.odeOptions);

final_state_vec = y(end, :)';

if nargout > 1
  varargout(1) = {t};
  if nargout > 2
    varargout(2) = {y};
  end
end

end


