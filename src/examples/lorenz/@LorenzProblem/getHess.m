function [hess] = getHess(obj, x)

%extract final state from polynomial
[u_tf, tu, u] = obj.solve_state_eq(x);
poly_u = pchip(tu, u');

%final value of adjoint variable, depends on whether full or partial observations were made
p_tf = - obj.obsOp;

%solve adjoint equation, get p0 and time series
[p0, tp, p] = obj.solve_adj_eq(poly_u, p_tf);
poly_p = pchip(tp, p');

I = eye(3);
e1 = I(:, 1);
e2 = I(:, 2);
e3 = I(:, 3);

e_fused = [e1; e2; e3];

[~, tubar, ubar] = obj.solve_inc_fwd_eq(poly_u, e_fused);
poly_ubar = pchip(tubar, ubar');

pbar_tf = zeros(3 * 3, 1);
pbar = obj.solve_inc_adj_eq(poly_u, poly_p, poly_ubar, pbar_tf);

hess = -[pbar(1 : 3), pbar(4 : 6), pbar(7 : 9)];
hess = 0.5 * (hess + hess');

end
