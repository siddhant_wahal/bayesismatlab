function [final_state_vec varargout] = solve_inc_fwd_eq(obj, poly_u, init_cond)

v0 = init_cond;

[t, v] = ode45(@(t, v) obj.rhsIncFwd(t, v, poly_u), [0 obj.tFinal], v0, obj.odeOptions);

final_state_vec = v(end, :)';


if nargout > 1
  varargout(1) = {t};
  if nargout > 2
    varargout(2) = {v};
  end
end

end
