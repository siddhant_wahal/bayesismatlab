function [dp] = rhsAdj(obj, t, p, poly_u);

dp = zeros(size(p));

u = ppval(poly_u, obj.tFinal - t);

dp(1, :) = -obj.P * p(1, :) + (obj.R - u(3)) * p(2, :) + u(2) * p(3, :);
dp(2, :) = obj.P * p(1, :) - p(2, :) + u(1) * p(3, :);
dp(3, :) = - u(1) * p(2, :) - obj.b * p(3, :);

end
