function [final_state_vec, varargout] = solve_inc_adj_eq(obj, poly_u, poly_p, poly_ubar, final_cond)

p0 = final_cond;

[t, p] = ode45(@(t, p) obj.rhsIncAdj(t, p, poly_u, poly_p, poly_ubar), ...
                                      [0 obj.tFinal], p0, obj.odeOptions);


final_state_vec = p(end, :)';

if nargout > 1
  varargout(1) = {t};
  if nargout > 2
    varargout(2) = {p};
  end
end

end
