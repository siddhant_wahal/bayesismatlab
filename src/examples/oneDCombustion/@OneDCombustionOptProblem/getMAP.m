function [map, varargout] = getMAP(obj, target, g0, noiseVar)

    dim = length(obj.prior.meanVec);

    map = zeros(dim, 1);
    hess = zeros(dim);
    [map, objFunVal, exitFlag, output, lambda, grad, hess] = fmincon(@(g) obj.objFunRegularized(g, target, noiseVar), ...
                                                             g0, [], [], [], [], 0, 1, [],  obj.options);

    if nargout > 1
        varargout{1} = grad;
        if nargout > 2
            varargout{2} = hess;
        end
    end


    fprintf('Newton iter = %d\n', output.iterations);
    fprintf('CG iter = %d\n', output.cgiterations);
    fprintf('nFwdSolve = %d\n', obj.problem.nFwdSolve);
    fprintf('nAdjSolve = %d\n', obj.problem.nAdjSolve);
    fprintf('nIncFwdSolve = %d\n', obj.problem.nIncFwdSolve);
    fprintf('nIncAdjSolve = %d\n', obj.problem.nIncAdjSolve);

    obj.problem.nFwdSolve    = 0    
    obj.problem.nAdjSolve    = 0
    obj.problem.nIncFwdSolve = 0
    obj.problem.nIncAdjSolve = 0
end
