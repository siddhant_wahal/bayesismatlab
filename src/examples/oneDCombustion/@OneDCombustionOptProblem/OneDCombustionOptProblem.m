classdef OneDCombustionOptProblem
    properties
        problem
        prior
        options = optimoptions(@fmincon);
    end
    methods
        function optObj = OneDCombustionOptProblem(problem, prior)
            optObj.problem = problem;
            optObj.prior = prior;
            optObj.options.Algorithm = 'trust-region-reflective';
            optObj.options.GradObj = 'on';
            optObj.options.Display = 'iter'
            optObj.options.Hessian = 'user-supplied';
            optObj.options.TolFun = 1e-8;
            optObj.options.TolX = 1e-8;
            %optObj.options.TolFun = 1e-4;
            %optObj.options.TolX = 1e-4;
            optObj.options.TypicalX = 0.5;
            optObj.options.MaxFunEvals = 500;
        end

        [varargout] = objFunRegularized(obj, g, target, noiseVar);
        [d] = getQOI(obj, g);
        [map, varargout] = getMAP(obj, target, g0, noiseVar);
        [hess] = getHess(obj, g, target);
        [hess, varargout] = getGNHess(obj, g, noiseVar)
    end
end
