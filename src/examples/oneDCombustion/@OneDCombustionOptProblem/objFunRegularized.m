function [varargout] = objFunRegularized(obj, g, targetVal, noiseVar)

numSteps = 1e5;
dt = obj.problem.tFinal / 1e5;
tArr = [0 : dt : obj.problem.tFinal];

[~, cArr] = ode15s(@(t, y) obj.problem.rhsFwd(t, y), tArr, g, obj.problem.odeOptions);
obj.problem.nFwdSolve = obj.problem.nFwdSolve + 1;
cFinal = cArr(end);

val = 0.5 * (cFinal - targetVal) * (cFinal - targetVal) / noiseVar + 0.5 * (g - obj.prior.meanVec) * (g - obj.prior.meanVec) / (obj.prior.covMat);

varargout(1) = {val};

if nargout > 1
  
  pFinal = -(cFinal - targetVal);
  [~, pArr] = ode15s(@(t, y) obj.problem.rhsAdj(t, y, tArr, cArr), tArr, pFinal, obj.problem.odeOptions);
  obj.problem.nAdjSolve = obj.problem.nAdjSolve + 1;
  
  grad = -pArr(end) / noiseVar + (g - obj.prior.meanVec) / obj.prior.covMat;
  varargout(2) = {grad};

  if nargout > 2

    cInc0 = 1;
    [~, cIncArr] = ode15s(@(t, y) obj.problem.rhsIncFwd(t, y, tArr, cArr), tArr, cInc0, obj.problem.odeOptions);
    obj.problem.nIncFwdSolve = obj.problem.nIncFwdSolve + 1;
    
    pInc0 = - cIncArr(end);
    [~, pIncArr] = ode15s(@(t, y) obj.problem.rhsIncAdj(t, y, tArr, cArr, pArr, cIncArr), tArr, pInc0, obj.problem.odeOptions);
    obj.problem.nIncAdjSolve = obj.problem.nIncAdjSolve + 1;

    hess = -pIncArr(end) / noiseVar + 1 / obj.prior.covMat;
    varargout(3) = {hess};
    
    if nargout > 3
      varargout(4) = {abs(cFinal - targetVal)};
    end
  end
end

end
