function [d] = getQOI(obj, gArr)

n = size(gArr, 2);
d = zeros(n, 1);

for i = 1 : n
    g = gArr(:, i);
    [~, uArr] = ode15s(@(t, g) obj.rhsFwd(t, g), [0 obj.tFinal], g, obj.odeOptions);
    d(i) = uArr(end);
end

end
