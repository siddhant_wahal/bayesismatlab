function [jac] = getJacobian(obj, g)

numSteps = 1e5;
dt = obj.tFinal / 1e5;
tArr = [0 : dt : obj.tFinal];

[~, uArr] = ode15s(@(t, g) obj.rhsFwd(t, g), tArr, g, obj.odeOptions);
uFinal = uArr(end);

pFinal = -1.0;
[~, pArr] = ode15s(@(t, g) obj.rhsAdj(t, g, tArr, uArr), tArr, pFinal, obj.odeOptions);
jac = -pArr(end);
  
end
