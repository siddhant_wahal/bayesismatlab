function dg = rhsIncAdj(obj, t, g, tArr, uArr, pArr, uIncArr)


u = interp1(tArr, uArr, obj.tFinal - t);
p = interp1(tArr, pArr, t);
uInc = interp1(tArr, uIncArr, obj.tFinal - t);

dg = zeros(size(g));

expTerm = exp(-obj.eAct / (obj.tempU + obj.tempB * u));
sqTerm = (obj.eAct * obj.tempB / ((obj.tempU + obj.tempB * u) * (obj.tempU + obj.tempB * u)));

dSbydC = obj.normConst .* ((1 - u) .* expTerm - u .* expTerm + u .* (1 - u) .* expTerm .* sqTerm);

quadTerm = (obj.eAct * obj.eAct * obj.tempB * obj.tempB * (1 - u) * u) / (obj.tempU + obj.tempB * u)^4;
cubicTerm = (2 * obj.eAct * obj.tempB * (obj.tempB * u * u + obj.tempU * (2 * u - 1)) ) / (obj.tempU + obj.tempB * u)^3;

d2SbydC2 = obj.normConst * expTerm * (quadTerm - cubicTerm - 2);

dg(:) = dSbydC .* g(:) + uInc * p * d2SbydC2;


end
