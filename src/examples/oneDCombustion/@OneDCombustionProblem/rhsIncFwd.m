function dg = rhsIncFwd(obj, t, g, tArr, uArr)

u = interp1(tArr, uArr, t);

dg = zeros(size(g));

expTerm = exp(-obj.eAct / (obj.tempU + obj.tempB * u));
sqTerm = (obj.eAct * obj.tempB / ((obj.tempU + obj.tempB * u) * (obj.tempU + obj.tempB * u)));


dg(:) = obj.normConst .* ((1 - u) .* expTerm - u .* expTerm + u .* (1 - u) .* expTerm .* sqTerm) .* g(:);

end
