function dg = rhsFwd(obj, t, g)

dg = zeros(length(g), 1);

expTerm = zeros(length(g), 1);
expTerm = exp(-obj.eAct ./ (obj.tempU + obj.tempB * g(:)));

dg(:) = obj.normConst .* g(:) .* (1 - g(:)) .* expTerm;

end
