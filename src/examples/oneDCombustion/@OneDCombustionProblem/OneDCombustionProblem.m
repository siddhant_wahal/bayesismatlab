classdef OneDCombustionProblem < handle
    properties
        %ode parameters
        odeOptions = odeset('AbsTol', 1e-12, 'RelTol', 1e-10, 'Vectorized', 'on');
        tFinal = 1
    
        %physics parameters
        normConst = 6.11e7
        eAct = 3e4
        tempU = 3e2
        tempB = 18e2
        nFwdSolve = 0;
        nAdjSolve = 0;
        nIncFwdSolve = 0;
        nIncAdjSolve = 0;
    end
    methods
        %rhs of forward ode
        [dg] = rhsFwd(obj, t, g);

        %rhs of adj ode
        [dp] = rhsAdj(obj, t, g, tVec, uVec);

        %rhs of incremental forward ode
        [dg] = rhsIncFwd(obj, t, g, tVec, uVec);

        %rhs of incremental forward ode
        [dp] = rhsIncAdj(obj, t, g, tVec, uVec, pVec, uIncVec);

        %returns qoi at parameter g
        [qoi] = getQOI(obj, g);

        [jac] = getJacobian(obj, g) 

    end
end
