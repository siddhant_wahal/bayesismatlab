function [val, varargout] = objFunRegularized(obj, g, target, noiseVar)
    
    
    gPushFwd = obj.problem.getQOI(g);
    jac = obj.problem.getJacobian();
    
    misfit = 0.5 / (noiseVar) * (gPushFwd - target)^2;
    reg = 0.5 * obj.prior.sqMahal(g);
    
    val = misfit + reg;
     
    if nargout > 1
       misfit_grad = 1.0 / (noiseVar) * (gPushFwd - target) * jac';
       reg_grad = obj.prior.precisionMat * (g - obj.prior.meanVec);
       varargout{1} = misfit_grad + reg_grad;
     
       if nargout > 2
     
         misfit_hess = 1.0 / (noiseVar) * jac' * jac;
         reg_hess = obj.prior.precisionMat;
         hess = misfit_hess + reg_hess;
         hess = 0.5 * (hess + hess');
         varargout{2} = hess;
     
       end
    end
     
end
