classdef LinearMatOptProblem
    properties
        problem;
        prior;
        options = optimoptions(@fminunc);
    end

    methods
        function optObj = LinearMatOptProblem(problem, prior)
            optObj.problem = problem;
            optObj.prior = prior;
            optObj.options.Algorithm = 'trust-region';
            optObj.options.Display = 'off';
            optObj.options.SpecifyObjectiveGradient = true;
            optObj.options.HessianFcn = 'objective';
            optObj.options.TolFun = 1e-12;
            optObj.options.TolX = 1e-12;
        end

        [varargout] = objFunRegularized(obj, g, target, noiseVar);
        [d] = getQOI(obj, g);
        [map, varargout] = getMAP(obj, target, g0, noiseVar);
        [hess] = getHess(obj, g, target, noiseVar);
        [hess, varargout] = getGNHess(obj, g, noiseVar);

            
    end
end
