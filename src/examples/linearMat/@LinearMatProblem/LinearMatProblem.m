classdef LinearMatProblem
    
    properties
        %the forward operator
        B
        dim
    end

    methods
        function obj = LinearMatProblem(dim)
            %forward operator definition
            obj.dim = dim;
            obsOp = 1.0 / dim * ones(dim, 1);
            A = diag((linspace(1, dim, dim) .^ (-1)));
            obj.B = obsOp' * A; 
        end

        function [varargout] = getQOI(obj, g)
            %this function is vectorized
            %i.e., g can be a d x n matrix 
            d = obj.B * g;
            d = d';
            varargout(1) = {d};

            if nargout > 1
                varargout(2) = {obj.B};
            end

        end

        function [jac] = getJacobian(obj, g)
            jac = obj.B;
        end

        function [grad] = getGrad(obj, g)
            grad = obj.B';
        end

        function [hess] = getHess(obj, g)
            hess = sparse(obj.dim, obj.dim);
        end

        function [Hx] = getHess_mv(obj, g, x)
            Hx = sparse(size(x));
        end

    end

end
