function [map, varargout] = getMAP(obj, target, g0, noiseVar)

    dim = length(obj.prior.components(1).meanVec);

    map = zeros(dim, 1);
    hess = zeros(dim);

    [map, objFunVal, exitFlag, output, grad, hess] = fminunc(@(g) obj.objFunRegularized(g, target, noiseVar), g0, obj.options);

    if nargout > 1
        varargout{1} = grad;
        if nargout > 2
            varargout{2} = hess;
        end
    end
    
end
