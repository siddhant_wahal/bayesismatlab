function [hess, varargout] = getGNHess(obj, g, noiseVar)

    jac = obj.problem.getJacobian();
     
    misfit_hess = 1.0 / (noiseVar) * jac' * jac;

    [~, ~, reg_hess] = obj.prior.logpdf(g);
    reg_hess = -reg_hess;

    hess = misfit_hess + reg_hess;
    hess = 0.5 * (hess + hess');

    if nargout > 1
        inv_reg_hess = inv(reg_hess);
        invHess = inv_reg_hess - 1 / (noiseVar + jac * inv_reg_hess * jac') ...
                                     * (inv_reg_hess * jac') ...
                                     * (inv_reg_hess * jac')';
        varargout(1) = {invHess};
    end

end
