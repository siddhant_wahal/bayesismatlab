classdef GenLinearMatOptProblem
    properties
        problem
        prior
        options = optimoptions(@fminunc)
    end

    methods
        function optObj = GenLinearMatOptProblem(problem, prior)
            optObj.problem = problem;
            optObj.prior = prior;
            optObj.options.Algorithm = 'trust-region';
            optObj.options.Display = 'iter';
            optObj.options.GradObj = 'on';
            optObj.options.Hessian = 'off'
            optObj.options.DerivativeCheck = 'on'
            optObj.options.FinDiffType = 'central'
            optObj.options.TolFun = 1e-12;
            optObj.options.TolX = 1e-12;
        end

        [varargout] = objFunRegularized(obj, g, target, noiseVar);
        [d] = getQOI(obj, g);
        [map, varargout] = getMAP(obj, target, g0, noiseVar);
        [hess] = getHess(obj, g, target, noiseVar);
        [hess, varargout] = getGNHess(obj, g, noiseVar);

            
    end
end
