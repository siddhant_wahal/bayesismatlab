function [varargout] = objFunRegularized(obj, g, target, noiseVar)

    dim = 2;
    d = obj.problem.getQOI(g);
    misfit = ((target - d)^2) / (2 * noiseVar);
    reg = - obj.prior.logpdf(g);
    val = misfit + reg;
    varargout(1) = {val};

    if nargout > 1
        jac = obj.problem.getJacobian(g);
        misfit_grad = -1.0 / noiseVar * (target - d) * jac';

        [~, reg_grad] = obj.prior.logpdf(g);
        reg_grad = -reg_grad;
        
        grad = misfit_grad + reg_grad;
        varargout(2) = {grad};

        if nargout > 2
            misfit_hess = -1.0 / noiseVar * (target - d) ...
                           *  obj.problem.getHess(g) ...
                           + 1.0 / noiseVar * jac' * jac;

            [~, ~, reg_hess] = obj.prior.logpdf(g);
            reg_hess = -reg_hess;

            hess = misfit_hess + reg_hess;
            hss = 0.5 * (hess + hess');
            varargout(3) = {hess};
        end
    end

end
