function [map, varargout] = getMAP(obj, target, g0, noiseVar)
    
    dim = 2;
    map = zeros(dim, 1);
    hess = zeros(dim);
    [map, objFunVal, exitFlag, output, grad, hess] = ...
                                fminunc(@(g)obj.objFunRegularized(g, ...
                                            target, noiseVar), g0, obj.options);

    if nargout > 1
        varargout(1) = {grad};
        if nargout > 2 
            varargout(2) = {hess};
        end
    end

end
