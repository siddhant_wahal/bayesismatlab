function [hess] = getHess(obj, g)

    dim = numel(g);
    hess = zeros(dim);


    hess = [2 + 2 * obj.coeff * g(2) * g(2), 4 * obj.coeff * g(1) * g(2); ...
            4 * obj.coeff * g(1) * g(2), 2 + 2 * obj.coeff * g(1) * g(1)];
end
