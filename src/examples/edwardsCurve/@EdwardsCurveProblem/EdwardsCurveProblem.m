classdef EdwardsCurveProblem
    properties
        coeff
    end

    methods
        function obj = EdwardsCurveProblem(coeff)
            obj.coeff = coeff;
        end

        [d] = getQOI(obj, g);
        [jac] = getJacobian(obj, g);
        [grad] = getGrad(obj, g);
        [hess] = getHess(obj, g);
    end
end
