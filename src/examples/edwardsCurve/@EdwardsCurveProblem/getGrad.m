function [grad] = getGrad(obj, g)
    jac = obj.getJacobian(g);
    grad = jac';
end
