function [jac] = getJacobian(obj, g)
    jac = [2 * g(1) + 2 * obj.coeff * g(1) * g(2) * g(2), ...
           2 * g(2) + 2 * obj.coeff * g(1) * g(1) * g(2)];
end
