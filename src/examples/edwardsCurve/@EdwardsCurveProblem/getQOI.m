function [d] = getQOI(obj, g)
    %g is d x n
    n = size(g, 2);
    gSq = g .^2;
    d = zeros(n, 1);
    d = gSq(1, :) + gSq(2, :) + obj.coeff * (gSq(1, :) .* gSq(2, :));
end
