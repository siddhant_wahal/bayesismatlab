function [varargout] = objFunRegularized(obj, g, target, noiseVar)
    
    [d, jac] = obj.problem.getQOI(g);
    qoi_grad = jac';

    val = ((target - d) ^ 2) / (2 * noiseVar) + 0.5 * obj.prior.sqMahal(g);
    %val = ((target - d) ^ 2) / (2 * noiseVar);

    varargout(1) = {val};
    
    if nargout > 1
        
        misfit_grad = - 1 / noiseVar * (target - d) * qoi_grad;
        reg_grad    = obj.prior.precisionMat * (g - obj.prior.meanVec);

        grad = misfit_grad + reg_grad;
        varargout(2) = {grad};

        if nargout > 2

            misfit_hess = 1 / noiseVar * (qoi_grad * qoi_grad');

            if ~obj.use_GN_hess

                qoi_hess = obj.problem.getHess(g);
                misfit_hess = misfit_hess  - (target - d) * qoi_hess; 

            end

            reg_hess    = obj.prior.precisionMat;
            hess        = misfit_hess + reg_hess;
            hess        = 0.5 * (hess + hess');

            varargout(3) = {hess};
        end
    end

end
