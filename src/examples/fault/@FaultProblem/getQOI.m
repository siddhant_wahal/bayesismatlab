function [varargout] = getQOI(obj, g)

    n = size(g, 2);
    d = zeros(n, 1);

    for i = 1 : n
        u = obj.solveFwd(g(:, i), obj.b);
        d(i) = obj.obs_op' * u;
    end

    varargout(1) = {d};

    if nargout > 1
        p = obj.solveAdj(g, -obj.obs_op);

        jac = zeros(1, obj.num_faults);

        for i = 1 : obj.num_faults
            jac(i) = - p' * obj.dipole_sources{i} * u * exp(-g(i));
        end

        varargout(2) = {jac};
    end
end
