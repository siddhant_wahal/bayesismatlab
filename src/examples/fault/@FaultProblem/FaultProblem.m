classdef FaultProblem
    properties
        laplacian      % discrete laplacian
        dipole_sources % dipole source term
                       % 3D array
        b              % rhs
        v2d            % vertex to dof map
        obs_op         % observation operator
        x              % x coordinate of mesh vertices
        y              % y coordinate of mesh vertices
        dim            % dimension of the problem
        num_faults     % number of faults
        tri            % mesh triangulation
    end
    methods
        function obj = FaultProblem(input_file, num_faults)

            load(input_file);

            obj.laplacian      = laplacian;
            obj.dipole_sources = dipole_sources;
            obj.b              = b';
            obj.v2d            = v2d + 1;
            obj.x              = x;
            obj.y              = y;
            obj.dim            = length(b);
            rng(1);
            %obj.obs_op         = rand(obj.dim, 1) / obj.dim;
            obj.obs_op         = obs_op';
            rng('shuffle')
            obj.num_faults     = num_faults;
            obj.tri            = delaunay(x, y);
        end


        [varargout] = getQOI(obj, g);

        %solve the forward eq. A(g)u = rhs
        [u] = solveFwd(obj, g, rhs); 

        %solve the adjoint eq. A(g)^Tu = rhs
        [p] = solveAdj(obj, g, rhs);

        [jac] = getJacobian(obj, g);

        [hess] = getHess(obj, g);

        [Hx] = getHess_mv(obj, g, x);

        [p_vec] = order(obj, u);
    end
end




