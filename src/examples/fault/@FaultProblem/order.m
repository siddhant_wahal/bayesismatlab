function [pvec] = order(obj, u)

    pvec = zeros(size(u));
    pvec(obj.v2d(:)) = u;

end
