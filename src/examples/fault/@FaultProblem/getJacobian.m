function [jac] = getJacobian(obj, g)
    
    u = obj.solveFwd(g, obj.b);
    p = obj.solveAdj(g, -obj.obs_op);

    jac = zeros(1, obj.num_faults);

    for i = 1 : obj.num_faults
        jac(i) = - p' * obj.dipole_sources{i} * u * exp(-g(i));
    end

end
