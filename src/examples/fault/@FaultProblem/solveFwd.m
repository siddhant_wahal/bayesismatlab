function u = solveFwd(obj, g, rhs)
% Solve the forward equation:
% (exp(-g1) C1 + exp(-g2) C2 ...+ exp(-gn) Cn + L)u = rhs


lhs = obj.laplacian;

for i = 1 : obj.num_faults

    lhs = lhs + exp(-g(i)) * obj.dipole_sources{i};

end

u = lhs \ rhs;

end
