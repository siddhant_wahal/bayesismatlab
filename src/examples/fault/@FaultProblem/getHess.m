function [hess] = getHess(obj, g)

    u = obj.solveFwd(g, obj.b);
    p = obj.solveAdj(g, - obj.obs_op); 

    I = speye(obj.dim);

    inc_state_rhs = zeros(obj.dim, obj.num_faults);
    inc_adj_rhs   = zeros(obj.dim, obj.num_faults);

    for i = 1 : obj.num_faults

        inc_state_rhs(:, i) = - exp(-g(i)) ...
                              * obj.dipole_sources{i} ...
                              * u;
        inc_adj_rhs(:, i)   = exp(-g(i)) ...
                              * obj.dipole_sources{i}' ...
                              * p;
    end

    inc_state = obj.solveFwd(g, inc_state_rhs);
    inc_adj   = obj.solveAdj(g, inc_adj_rhs);

    hess = zeros(obj.num_faults);

    for i = 1 : obj.num_faults
        hess(i, i) = exp(-g(i)) * p' * obj.dipole_sources{i} * u;
    end

    temp1 = zeros(obj.num_faults, obj.dim);
    temp2 = zeros(obj.num_faults, obj.dim);

    for  i = 1 : obj.num_faults

        temp1(i, :) = exp(-g(i)) * p' * obj.dipole_sources{i};
        temp2(i, :) = - exp(-g(i)) * u' * obj.dipole_sources{i}';

    end

    hess = hess + temp1 * inc_state + temp2 * inc_adj;

    hess = 0.5 * (hess + hess');


end
