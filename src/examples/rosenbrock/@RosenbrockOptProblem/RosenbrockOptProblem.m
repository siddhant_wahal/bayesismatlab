classdef RosenbrockOptProblem < handle
    properties
        problem;
        prior;
        options = optimoptions(@fminunc);
    end
    methods
        function obj = RosenbrockOptProblem(problem, prior)
            obj.problem = problem;
            obj.prior = prior;
            
            obj.options.Algorithm                = 'trust-region';
            obj.options.Display                  = 'iter';
            obj.options.SpecifyObjectiveGradient = true;
            obj.options.HessianFcn               = 'objective';
            obj.options.StepTolerance            = 1e-4;
            obj.options.FunctionTolerance        = 1e-4;
            obj.options.OptimalityTolerance      = 1e-4;
            obj.options.DerivativeCheck          = 'off';
            obj.options.FinDiffType              = 'central'
            obj.options.MaxFunEvals              = 500;
        end

        [varargout]       = objFunRegularized(obj, g, target, noiseVar);
        [varargout]       = objFunMisfit(obj, g, target, noiseVar);
        [d]               = getQOI(obj, g);
        [map, varargout]  = getMAP(obj, target, g0, noiseVar);
        [hess]            = getHess(obj, g, target, noiseVar);
        [hess, varargout] = getGNHess(obj, g, noiseVar);

    end




end
