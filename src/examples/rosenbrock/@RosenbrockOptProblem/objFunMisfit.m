function [varargout] = objFunMisfit(obj, g, target, noiseVar)

    d = obj.problem.getQOI(g);
    val = ((target - d)^2) / (2 * noiseVar);
    varargout(1) = {val};

    if nargout > 1
        jac = obj.problem.getJacobian(g);
        misfit_grad = -1.0 * (target - d) * jac' / noiseVar;
        grad = misfit_grad; 
        varargout(2) = {grad};

        if nargout > 2
            misfit_hess = -1.0 * (target - d) ...
                          * obj.problem.getHess(g) / noiseVar ...
                           + 1.0 * jac' * jac / noiseVar;
            hess = misfit_hess;
            hess = 0.5 * (hess + hess');
            varargout(3) = {hess};
        end
    end

end
