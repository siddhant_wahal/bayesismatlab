function [map, varargout] = getMAP(obj, target, g0, noiseVar)
    
    dim = 2;
    map = zeros(dim, 1);
    hess = zeros(dim);
    [map, ~, ~, output] = fminunc(@(g)obj.objFunRegularized(g, ...
                                                             target, noiseVar), g0, obj.options);

    if nargout > 1
        varargout(1) = {output.funcCount};
    end

end
