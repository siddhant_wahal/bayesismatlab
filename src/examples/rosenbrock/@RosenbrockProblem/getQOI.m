function [varargout] = getQOI(obj, g)

    d = zeros(size(g, 2), 1);

    d = (obj.a - g(1, :)) .^ 2 + obj.b * (g(2, :) - g(1, :) .^2) .^ 2;

    varargout(1) = {d};

    if nargout > 1
        jac = obj.getJacobian(g);
        varargout(2) = {jac};
    end



end
