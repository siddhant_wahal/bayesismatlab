function jac = getJacobian(obj, g)
    
    jac = [- 2 * (obj.a - g(1)) - 4 * obj.b * g(1) * (g(2) - g(1) * g(1)),  ...
           2 * obj.b * (g(2) - g(1) * g(1)), ...
           zeros(1, obj.dim - 2)];

end
