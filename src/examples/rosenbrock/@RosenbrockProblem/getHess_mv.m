function [Hx] = getHess_mv(obj, g, x)

    H = obj.getHess(g);
    Hx = H * x;
end
