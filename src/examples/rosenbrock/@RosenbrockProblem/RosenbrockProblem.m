classdef RosenbrockProblem
    properties
        a = 1;
        b = 100;
        dim;
    end
    methods
        function obj = RosenbrockProblem(a, b, dim)
            obj.a = a;
            obj.b = b;
            obj.dim = dim;
        end

        [varargout] = getQOI(obj, g);
        [jac] = getJacobian(obj, g);
        [hess] = getHess(obj, g);
        [Hx] = getHess_mv(obj, g, x);
    end
end




