function hess = getHess(obj, g)

    b = obj.b;
    a = obj.a;

    hess_active = [2 - 4 * b * g(2) + 12 * b * g(1) * g(1), - 4 * b * g(1); ...
            - 4 * b * g(1), 2 * b];
    hess_active = 0.5 * (hess_active + hess_active');

    hess = zeros(obj.dim);
    hess(1 : 2, 1 : 2) = hess_active;
    hess = sparse(hess);
end
