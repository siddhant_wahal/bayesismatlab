function [hess] = getHess(obj, x)
    
    cubic_hess_1 = sparse(double(ttv(obj.S, x, 1)));
    cubic_hess_2 = sparse(double(ttv(obj.S, x, 2)));
    cubic_hess_3 = sparse(double(ttv(obj.S, x, 3)));

    %temp = tensor(cubic_hess_1 + cubic_hess_1' ...
    %       + cubic_hess_2 + cubic_hess_2' ...
    %       + cubic_hess_3 + cubic_hess_3');

    %hess = temp.data + 2 * obj.H;

    hess = cubic_hess_1 + cubic_hess_1' ...
           + cubic_hess_2 + cubic_hess_2' ...
           + cubic_hess_3 + cubic_hess_3' ...
           + 2 * obj.H;


end
