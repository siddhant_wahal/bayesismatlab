function [jac] = getJacobian(obj, x)

    [~, jac] = obj.getQOI(x);

end
