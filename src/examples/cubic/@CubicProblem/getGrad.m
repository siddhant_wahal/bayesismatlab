function [grad] = getGrad(obj, x)

    jac = obj.getJacobian(x);
    grad = jac';
end
