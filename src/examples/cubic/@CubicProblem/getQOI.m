function [varargout] = getQOI(obj, x)
%Return S_ijk x_i x_j x_k + H_kl x_k x_l + b_m x_m

    n = size(x, 2);

    d = zeros(n, 1);

    for i = 1 : size(x, 2)

        cubic_term = ttv(obj.S, {x(:,i), x(:,i), x(:,i)});
        quad_term  = x(:,i)' * obj.H * x(:,i);
        lin_term   = x(:,i)' * obj.b;

        d(i) = cubic_term + quad_term + lin_term; 

    end % for

    varargout(1) = {d};

    if nargout > 1

        assert(n == 1);

        cubic_grad = ttv(obj.S, {x, x}, [2, 3]) ...
                     + ttv(obj.S, {x, x}, [1, 3]) ...
                     + ttv(obj.S, {x, x}, [1, 2]);

        quad_grad = 2. * obj.H * x;

        lin_grad = obj.b;

        if strcmp(class(cubic_grad), 'sptensor')
            full_cubic_grad = full(cubic_grad);
            cubic_grad_data = full_cubic_grad.data;
        else
            cubic_grad_data = cubic_grad.data;
        end


        jac = (cubic_grad_data + quad_grad + lin_grad)';

        varargout(2) = {jac};

    end % 
end % getQOI
