classdef CubicProblem

    properties

    dim; %ambient dimension of the problem
    S; %cubic coefficient term
    H; %quadratic coefficient term
    b; %linear term

    end % properties

    methods
        function obj = CubicProblem(dim, S, H, b)

            obj.dim = dim;
            obj.S   = S;
            assert(issymmetric(H))
            obj.H   = H;
            obj.b   = b;

        end % constructor

        [varargout] = getQOI(obj, x);
        [jac]       = getJacobian(obj, x);
        [grad]      = getGrad(obj, x);
        [hess]      = getHess(obj, x);

    end % methods

end % CubicProblem
