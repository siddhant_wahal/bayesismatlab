classdef CubicOptProblem
    properties
        problem;
        prior;
        options = optimoptions(@fminunc);
    end
    methods
        function obj = CubicOptProblem(problem, prior)
            obj.problem = problem;
            obj.prior = prior;
            obj.options.Algorithm                = 'trust-region';
            obj.options.Display                  = 'off';
            obj.options.SpecifyObjectiveGradient = true;
            obj.options.HessianFcn               = 'objective';
            obj.options.FunctionTolerance        = 1e-6;
            obj.options.OptimalityTolerance      = 1e-6;
            obj.options.StepTolerance            = 1e-6;
            obj.options.DerivativeCheck          = 'off';
            obj.options.FinDiffType              = 'central'
            obj.options.MaxFunEvals              = 500;
        end

        [varargout] = objFunRegularized(obj, x, target, noiseVar);
        [d] = getQOI(obj, x);
        [map, varargout] = getMAP(obj, target, x0, noiseVar);
        [hess] = getHess(obj, x, target, noiseVar);
        [hess, varargout] = getGNHess(obj, x, noiseVar);
    end




end
