function [hess] = getHess(obj, x, target, noiseVar)

    [~, ~, hess] = obj.objFunRegularized(x, target, noiseVar);

end
