function [map, varargout] = getMAP(obj, target, x0, noiseVar)
    
    [map, ~, ~, output] = fminunc(@(x)obj.objFunRegularized(x, ...
                                                             target, ...
                                                             noiseVar), ...
                                                            x0, obj.options);

    if nargout > 1
        varargout(1) = {output.funcCount};
    end

end
