function [grad] = getGrad(obj, g)
    temp = g(1 : obj.intrinsicDim) ./ obj.coeffSq;
    grad = [2.0 * temp; obj.B'];
end
