function [varargout] = getQOI(obj, g)
    %g is d x n
    n = size(g, 2);
    temp = g(1 : obj.intrinsicDim, :) .^2;
    tempCoeff = repmat(obj.coeffSq, [1, n]);
    d = zeros(n, 1);
    d = (sum(temp ./ tempCoeff, 1) + obj.B * g(obj.intrinsicDim + 1 : end, :))';
    varargout(1) = {d};

    if nargout > 1
        temp = g(1 : obj.intrinsicDim) ./ obj.coeffSq;    
        jac = [2.0 * temp', obj.B];
        varargout(2) = {jac};
    end
end
