function [jac] = getJacobian(obj, g)
    temp = g(1 : obj.intrinsicDim) ./ obj.coeffSq;    
    jac = [2.0 * temp', obj.B];
end
