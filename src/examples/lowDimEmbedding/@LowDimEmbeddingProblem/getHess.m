function [hess] = getHess(obj, g)

    dim = numel(g);
    hess = zeros(dim);

    for i = 1 : obj.intrinsicDim
        hess(i, i) = 2.0 / obj.coeffSq(i);
    end

    hess = sparse(hess);

end
