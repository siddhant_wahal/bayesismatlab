classdef LowDimEmbeddingProblem
    properties
        ambientDim
        intrinsicDim
        coeffSq
        %Embedded linear matrix
        B     
    end

    methods
        function obj = LowDimEmbeddingProblem(ambientDim, intrinsicDim, B, coeff)
            obj.ambientDim = ambientDim;
            obj.intrinsicDim = intrinsicDim;
            obj.B = B;
            %coeff must be intrinsicDim x 1
            obj.coeffSq = coeff;
        end

        [varargout] = getQOI(obj, g);
        [jac] = getJacobian(obj, g);
        [grad] = getGrad(obj, g);
        [hess] = getHess(obj, g);
        [Hx] = getHess_mv(obj, g, x);
    end
end
