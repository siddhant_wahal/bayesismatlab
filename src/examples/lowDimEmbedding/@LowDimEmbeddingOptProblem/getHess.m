function [hess] = getHess(obj, g, target, noiseVar)

    dim = 2;
    hess = zeros(dim);
    [~, ~, hess] = obj.objFunRegularized(g, target, noiseVar);

end
