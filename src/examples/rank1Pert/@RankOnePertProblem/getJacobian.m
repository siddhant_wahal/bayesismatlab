function [jac] = getJacobian(obj, g)
    dim  = size(obj.S, 1);
    jac = zeros(1, dim);

    u = obj.solveFwd(g, obj.b);
    p = obj.solveAdj(g, -1 * obj.obsOp); 

    jac = obj.gamma * ((p' * g) * u + (g' * u) * p);
    jac = jac';
end
