classdef RankOnePertProblem
    properties
        %original matrix
        S
        %inverse of S
        invS
        %rhs
        b
        %magnitude of perturbation
        gamma
        %observation operator
        obsOp
    end
    methods
        function obj = RankOnePertProblem(S, b, gamma, obsOp)
            obj.S = S;
            obj.b = b;
            obj.gamma = gamma;
            obj.obsOp = obsOp; 
            obj.invS = inv(S);
        end

        [varargout] = getQOI(obj, g);
        %solve the forward eq. A(g)u = rhs
        [u] = solveFwd(obj, g, rhs);
        %solve the forward eq. A(g)^Tu = rhs
        [p] = solveAdj(obj, g, rhs);
        [jac] = getJacobian(obj, g);
        [hess] = getHess(obj, g);
        [Hx] = getHess_mv(obj, g, x);
    end
end




