function [varargout] = getQOI(obj, g)

    n = size(g, 2);
    d = zeros(n, 1);
    for i = 1 : n
        u = obj.solveFwd(g(:, i), obj.b);
        d(i) = obj.obsOp' * u;
    end

    varargout(1) = {d};

    if nargout > 1
        dim  = size(obj.S, 1);
        jac = zeros(1, dim);

        p = obj.solveAdj(g, -1 * obj.obsOp); 

        jac = obj.gamma * ((p' * g) * u + (g' * u) * p);
        jac = jac';

        varargout(2) = {jac};
    end
end
