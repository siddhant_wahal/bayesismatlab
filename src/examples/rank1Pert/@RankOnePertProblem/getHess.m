function [hess] = getHess(obj, g)

    dim  = size(obj.S, 1);

    u = obj.solveFwd(g, obj.b);
    p = obj.solveAdj(g, -1 * obj.obsOp); 

    I = eye(dim);

    incFwdRhs = - obj.gamma * (g * u' + g' * u * I);
    UTilde = obj.solveFwd(g, incFwdRhs);

    incAdjRhs = - obj.gamma * (g' * p * I + g * p');
    PTilde = obj.solveAdj(g, incAdjRhs);

    hess = obj.gamma * (p * u' + u * p') ...
           + obj.gamma * (u * g' + u' * g * I) * PTilde ...
           + obj.gamma * (p * g' + g' * p * I) * UTilde;

    hess = 0.5 * (hess + hess');


end
