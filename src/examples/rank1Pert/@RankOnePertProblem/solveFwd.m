function u = solveFwd(obj, g, rhs)
%note that u is automatically vectorized.
%if rhs is a matrix with each column corresponding
%to a different rhs, u is also a matrix with 
%each column the corresponding solution
%however, u is not vectorized w.r.t g

    u = obj.invS * rhs - (obj.gamma) / (1 + obj.gamma * g' * obj.invS * g) * ...
        (obj.invS * g * g' * obj.invS) * rhs;

end
