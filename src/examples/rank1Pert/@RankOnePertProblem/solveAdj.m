function u = solveAdj(obj, g, rhs)

    u = obj.invS' * rhs - (obj.gamma) / (1 + obj.gamma * g' * obj.invS * g) * ...
        (obj.invS' * g * g' * obj.invS') * rhs;

end
