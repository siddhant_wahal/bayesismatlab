classdef RankOnePertOptProblem
    properties
        problem
        prior
        options = optimset(@fminunc)
    end
    methods
        function obj = RankOnePertOptProblem(problem, prior)
            obj.problem = problem;
            obj.prior = prior;
            %optObj.options.Algorithm = 'trust-region-reflective';
            obj.options.Display = 'off';
            obj.options.GradObj = 'on';
            obj.options.Hessian = 'on';
            obj.options.TolFun = 1e-10;
            obj.options.TolX = 1e-10;
            obj.options.DerivativeCheck = 'off';
            obj.options.FinDiffType = 'central'

            %optObj.options.TolFun = 1e-4;
            %optObj.options.TolX = 1e-4;
            obj.options.MaxFunEvals = 500;
        end

        [varargout] = objFunRegularized(obj, g, target, noiseVar);
        [d] = getQOI(obj, g);
        [map, varargout] = getMAP(obj, target, g0, noiseVar);
        [hess] = getHess(obj, g, target, noiseVar);
        [hess, varargout] = getGNHess(obj, g, noiseVar);
    end




end
