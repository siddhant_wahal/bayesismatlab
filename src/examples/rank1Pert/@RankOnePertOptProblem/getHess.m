function [hess] = getHess(obj, g, target, noiseVar)

    dim = length(obj.prior.meanVec);
    hess = zeros(dim);
    [~, ~, hess] = obj.objFunRegularized(g, target, noiseVar);

end
