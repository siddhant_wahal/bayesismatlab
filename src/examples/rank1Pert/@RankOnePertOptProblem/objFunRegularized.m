function [varargout] = objFunRegularized(obj, g, target, noiseVar)
    
    dim = size(obj.problem.S, 2);
    d = obj.problem.getQOI(g);
    val = ((target - d) ^ 2) / (2 * noiseVar) + 0.5 * obj.prior.sqMahal(g);
    varargout(1) = {val};
    
    if nargout > 1
        u = obj.problem.solveFwd(g, obj.problem.b);
        adjRhs = (target - d) / noiseVar * obj.problem.obsOp;
        p = obj.problem.solveAdj(g, adjRhs);
    
        misfit_grad = obj.problem.gamma * ((g' * p) * u + (g' * u) * p);
        reg_grad = obj.prior.precisionMat * (g - obj.prior.meanVec);
        grad = misfit_grad + reg_grad;
        varargout(2) = {grad};

        if nargout > 2

            I = eye(dim);

            incFwdRhs = - obj.problem.gamma * (g * u' + g' * u * I);
            UTilde = obj.problem.solveFwd(g, incFwdRhs);

            incAdjRhs = - 1 / noiseVar * obj.problem.obsOp * obj.problem.obsOp' * ...
                        UTilde - obj.problem.gamma * (g' * p * I + g * p');
            PTilde = obj.problem.solveAdj(g, incAdjRhs);

            misfit_hess = obj.problem.gamma * (p * u' + u * p') ...
                          + obj.problem.gamma * (u * g' + u' * g * I) * PTilde ...
                          + obj.problem.gamma * (p * g' + g' * p * I) * UTilde;

            reg_hess = obj.prior.precisionMat;
            hess = misfit_hess + reg_hess;
            hess = 0.5 * (hess + hess');
            varargout(3) = {hess};
        end
    end

end
