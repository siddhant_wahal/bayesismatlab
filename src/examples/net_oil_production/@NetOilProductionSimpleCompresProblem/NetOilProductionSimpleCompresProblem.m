classdef NetOilProductionSimpleCompresProblem

    properties
        pykernel
        dim
    end % properties

    methods
        function obj = NetOilProductionSimpleCompresProblem(options_json_file)

            obj.pykernel = PythonKernelHelper()
            obj.pykernel.DEBUG = false

            preamble = strcat(...
                'import sys;', ...
                'sys.path.insert(0, "/workspace/scratch/");', ...
                'from nop_simple_compres import NetOilProductionSimpleCompres;', ...
                'import numpy as np;', ...
                'import pickle;', ...
                sprintf('problem = NetOilProductionSimpleCompres("%s")', options_json_file))

            obj.pykernel.initialize(preamble)

            obj.dim = obj.pykernel.wait_for_array(...
                'dim = np.array([problem.num_faults])', 'pickle.dumps(dim)')


        end % constructor

        function [varargout] = getQOI(obj, x)

            n = size(x, 2);
            d = zeros(n, 1);

            for i = 1 : n
                xi = x(:, i);

                code = sprintf('x = np.array(%s); q = problem.get_qoi(x)', ...
                               arr_to_string(xi));
                pickle_code = 'pickle.dumps(q)';

                d(i) = obj.pykernel.wait_for_array(code, pickle_code);
            end

            varargout(1) = {d};

            if nargout > 1
                assert(n == 1)
                jac = obj.getJacobian(x);
                varargout(2) = {jac}
            end
        end % getQOI

        function jac = getJacobian(obj, x)
            code = sprintf('x = np.array(%s); g = problem.get_grad(x)', ...
                           arr_to_string(x));
            pickle_code = 'pickle.dumps(g)';
            jac = obj.pykernel.wait_for_array(code, pickle_code);
        end % getGrad

        function jac = getGrad(obj, x)
           jac = obj.getJacobian(x)'; 
        end % getJacobian

        function hess = getHess(obj, x)
            
            identity = eye(obj.dim);
            hess = zeros(obj.dim);

            for i = 1 : obj.dim
                code = strcat(...
                    sprintf('x = np.array(%s);', arr_to_string(x)), ...
                    sprintf('h_mv = problem.get_hess_mv(x, %s)', arr_to_string(identity(:, i))))
                pickle_code = 'pickle.dumps(h_mv)';
                hess(:, i) = obj.pykernel.wait_for_array(code, pickle_code)';
            end 

        end

        function delete(obj)
            delete(obj.pykernel)
        end % delete

    end % methods


end % NetOilProductionSimpleCompresProblem
