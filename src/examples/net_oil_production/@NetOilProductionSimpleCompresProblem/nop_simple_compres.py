import json
import os
import sys
from collections import OrderedDict
from functools import wraps

HIPPYLIB_BASE_DIR = os.environ.get('HIPPYLIB_BASE_DIR')

sys.path.append(HIPPYLIB_BASE_DIR)
sys.path.append(HIPPYLIB_BASE_DIR + '/exxon/code/')

import numpy as np
import dolfin as dl
dl.parameters["form_compiler"]["quadrature_degree"] = 6
dl.parameters["ghost_mode"] = "shared_facet"
import mpi4py

from hippylib import (
        ReducedHessianQOI, ReducedQOI, STATE, PARAMETER, ADJOINT,
        reducedQOIVerify
    )

from timeDependentPDEProblem import TimeDependentPDEVariationalProblem
from timeDependentMisfit import PointwiseStateObservationTD
from Problem import TwoPhaseFlow, TwoPhaseFlowFullCompressible
from exxon_priors import *
from exxon_qois import NetOilProduction
from exxon_utils import par_random, utils
from exxon_utils.well import Well

def numpy_lru_cache(maxsize=32):
    def decorator(func):
        @wraps(func)
        def decorated(self, arr):
            arr = arr.astype(np.float64, copy=False)
            arr.flags.writeable = False
            key = hash(arr.data.tobytes())
            try:
                val = decorated.cache[key]
                # Delete then reintroduce to jump up in order
                del decorated.cache[key]
            except KeyError:
                val = func(self, arr)

            decorated.cache[key] = val
            if len(decorated.cache) > maxsize:
                decorated.cache.popitem(last=False)

            return val
        decorated.cache = OrderedDict()
        return decorated
    return decorator


class NetOilProductionSimpleCompres(object):
    """
    Class that exposes value, gradient and hessian of the NetPresentValue
    QOI for interfacing with AdaptiveManifoldBIMC.
    """

    def __init__(self, options_json_file):

        self.max_cache_size = 8

        self.cache = OrderedDict()

        options = utils.OptionsHandler(options_json_file, verbose=False)

        ######################################################################
        # Misc 
        ######################################################################
        self.num_faults = options.num_faults
        randseed = options.seed

        ######################################################################
        # Mesh setup
        ######################################################################
        mesh_name = options.mesh_name
        mesh = dl.Mesh()

        hdf = dl.HDF5File(mesh.mpi_comm(), mesh_name + ".h5", "r")
        hdf.read(mesh, "/mesh", False)

        dim = mesh.geometry().dim()
        subdomains = dl.MeshFunction("size_t", mesh, dim)
        hdf.read(subdomains, "/subdomains")

        facet_data = dl.MeshFunction("size_t", mesh, dim - 1)
        hdf.read(facet_data, "/facet_data")
        
        comm = mesh.mpi_comm()
        comm_rank = comm.Get_rank()
        comm_size = comm.Get_size()

        x0, x1 = 0., 1.
        y0, y1 = 0., 1.
        dim = mesh.geometry().dim()

        # Mark boundaries and set full no flux flag (if needed)
        class NoFluxBoundary(dl.SubDomain):
            def inside(self, x, on_boundary):
                return dl.near(x[0], x0) \
                       or dl.near(x[0], x1) \
                       or dl.near(x[1], y0) \
                       or dl.near(x[1], y1)
            
            def is_full_no_flux(self):
                return True

        nofluxbdry = NoFluxBoundary()
        nofluxbdry.mark(facet_data, self.num_faults+1)

        ######################################################################
        # Time discretization
        ######################################################################
        t_init = 0.
        dt = options.dt
        num_time_steps = options.num_time_steps
        num_obs_steps = options.num_obs_steps
        num_wells = options.wells
    
        if options.uniform_time:
            td = utils.TimeSequence(
                dt, t_init, n_t=num_time_steps, n_obs=num_obs_steps)
        else:
            #dt = [0.01, 0.04, 0.1, 0.025, 0.075]
            td = utils.TimeSequence(dt, t_init, n_obs=num_obs_steps)

        t_final = td.t_final
        t_obs = td.t_obs

        ######################################################################
        # Reservoir setup
        ######################################################################
        real_reservoir_length     = options.length
        real_flow_rate            = options.flow_rate
        real_init_pressure        = options.init_pressure
        real_bottom_hole_pressure = options.bottom_hole_pressure

        ref_perm    = options.ref_perm
        ref_visc    = options.ref_viscosity
        ref_density = options.ref_density

        global_hmin = dl.MPI.comm_world.allreduce(mesh.hmin(), mpi4py.MPI.MIN)
        
        well_std_dev = (1./32 if (global_hmin < 1./32 + 1.e-8) else global_hmin) 
        well_variance = well_std_dev ** 2.

        real_injection_area = ((real_reservoir_length * well_std_dev) ** 2) \
                              * np.pi * 2.

        units = utils.non_dimensionalizer(real_reservoir_length,
                                          real_flow_rate,
                                          real_injection_area,
                                          ref_perm,
                                          ref_visc, 
                                          ref_density)
        # Wells
        # Dict with possible well locations for different cases
        well_locations = {'injection' : [np.array([0.2,0.2])],
                          'production': [np.array([0.8,0.8]), 
                                         np.array([1.0,0.0]), 
                                         np.array([0.0,1.0])]}
   
        i_well_locations = [well_locations['injection'][0]]
        p_well_locations = well_locations['production'][:num_wells]

        #The coeffcient is 4 because these are corner wells
        i_well_patterns = [{(0, t_final): 4.}] 
        p_well_patterns = [{(0, t_final): -4.0}] * num_wells

        pbh = real_bottom_hole_pressure / units['p0']
        flow_rate = real_flow_rate / (units['q0'] * units['x0'] ** 2.0)

        i_wells = [Well(loc,
                        mesh, 
                        well_type='injection',
                        variance=well_variance,
                        mode='pressure',
                        time_pattern=pattern,
                        pbh=pbh,
                        injection_saturation=1.0)
                   for loc, pattern in zip(i_well_locations, i_well_patterns)]

        p_wells = [Well(loc,
                        mesh, 
                        well_type='production',
                        variance=well_variance,
                        mode='rate',
                        time_pattern=pattern,
                        flow_rate=flow_rate)
                   for loc, pattern in zip(p_well_locations, p_well_patterns)]

        wells_list = i_wells + p_wells
        
        pressure_well_exists = any([well.mode == 'pressure' 
                                    for well in wells_list])


        
        ######################################################################
        # Nonlinear solver
        ######################################################################
        nsolver_parameters = {
            'linear_solver': 'mumps',
            'absolute_tolerance': 1e-8,
            'relative_tolerance': 1e-5,
            'maximum_iterations': 25
        }

        nsolver = utils.CustomNonlinearSolver("newton", nsolver_parameters)

        ######################################################################
        # PDE setup
        #, #####################################################################
        # Define function spaces for flow problem
        RT = dl.FunctionSpace(mesh, "RT", 1)
        DG = dl.FunctionSpace(mesh, "DG", 0)
        Rh = dl.VectorFunctionSpace(mesh, 'R', 0, dim = self.num_faults)

        if not options.compressible \
           and nofluxbdry.is_full_no_flux() \
           and not pressure_well_exists:

            LM = dl.FunctionSpace(mesh, "R", 0)

            FE_state = dl.MixedElement([RT.ufl_element(), 
                                        DG.ufl_element(), 
                                        DG.ufl_element(), 
                                        LM.ufl_element()])

            fieldnames = ['Velocity', 'Pressure', 'Saturation', 'Multiplier']

        else:

            FE_state = dl.MixedElement([RT.ufl_element(), 
                                        DG.ufl_element(), 
                                        DG.ufl_element()])

            fieldnames = ['Velocity', 'Pressure', 'Saturation']
        
        Vh = [dl.FunctionSpace(mesh, FE_state), Rh, 
              dl.FunctionSpace(mesh, FE_state)]

        # Extra RHS mixed
        f = dl.Constant((0.,0.,0.))

        if options.perm_model == 'random':
            N = 30
            centers = np.random.uniform(0.1, 0.9, [N, 2])
            perm = utils.RandomPermeabilityBlobs(centers, degree=3)
        elif options.perm_model == 'uniform':
            perm = dl.Constant(options.real_perm_value / ref_perm)
        else:
            raise Exception("Not implemented")

        perm_Vh = dl.FunctionSpace(mesh, "CG", 3)
        perm_f = dl.project(perm, perm_Vh)
        Kinv = 1./perm_f

        # Boundary conditions
        # Natural BCs for pressure and saturationx
        bcs = {"pres_bc": dl.Constant(0.), 
               "sat_bc": dl.Constant(0.)}

        # Essential BCs for flux in forward and adjoint problems
        fwd_bc = [dl.DirichletBC(Vh[STATE].sub(0), 
                  dl.Constant((0.,0.)),
                  facet_data,
                  self.num_faults+1)]

        adj_bc = [dl.DirichletBC(Vh[STATE].sub(0), 
                  dl.Constant((0.,0.)),
                  facet_data,self.num_faults+1)]
        
        pressure_ic = real_init_pressure / units['p0']

        sat_ic_str = "0.0"
        pres_ic_str = "%1.14f" % pressure_ic

        if not options.compressible \
           and nofluxbdry.is_full_no_flux() \
           and not pressure_well_exists:

            ic_func = dl.interpolate(dl.Constant((0., 0., pressure_ic, 
                                                  0., 0.)), Vh[STATE])
        else:
            ic_func = dl.interpolate(dl.Expression(("0.", "0.0", 
                                                    pres_ic_str, sat_ic_str), 
                                                    degree=0), Vh[STATE])        
        
        conds = [ic_func, fwd_bc, adj_bc, dl.Constant(0.)]

        if options.compressible:
            varf = TwoPhaseFlow(Vh=Vh, bcs=bcs, f=f, dt=dt, wells=wells_list, 
                                facet_data=facet_data, num_faults=self.num_faults, compressible=True)

        else:
            noflux_flag = (nofluxbdry.is_full_no_flux() \
                                and not pressure_well_exists)

            varf = TwoPhaseFlow(Vh=Vh, bcs=bcs, f=f, dt=dt, wells=wells_list, 
                                facet_data=facet_data, num_faults=self.num_faults, compressible=False,
                                noflux=noflux_flag)        


        varf.parameter['kappa'] = Kinv
        varf.parameter['density_w'] = dl.Constant(options.real_density_w) / ref_density
        varf.parameter['density_n'] = dl.Constant(options.real_density_n) / ref_density
        varf.parameter['viscosity_w'] = dl.Constant(options.real_viscosity_w) / ref_visc
        varf.parameter['viscosity_n'] = dl.Constant(options.real_viscosity_n) / ref_visc
        varf.parameter['comp_w'] = dl.Constant(options.real_comp_w) / units['c0']
        varf.parameter['comp_n'] = dl.Constant(options.real_comp_n) / units['c0']
        varf.parameter['comp_r'] = dl.Constant(options.real_comp_r) / units['c0']
        #reference pressure for density calculation = atmosphereic pressure
        varf.parameter['ref_pressure_density'] = 1.01325e5 / units['p0'] 

        #NOTE: Having s_wc >> 0 leads (why?) to much stiffer system, use smaller dt
        varf.parameter['rel_perm_model'] = utils.RelativePermeabilityModel(
            model=options.rel_perm_model, 
            s_or=options.s_or, 
            s_wc=options.s_wc, 
            n_o=options.n_o, 
            n_w=options.n_w,
            lmbda=options.lmbda)


        ######################################################################
        # Time dependent problem definition
        ######################################################################
        self.problem = TimeDependentPDEVariationalProblem(
            Vh, varf_handler=varf, conds=conds, timedata=td,
            is_fwd_linear=False, nonlinear_solver=nsolver, 
            to_clip=[(2, 0, 1)])

        ######################################################################
        # QOI definition
        ######################################################################
        self.qoi = ReducedQOI(self.problem, NetOilProduction(self.problem))

    @numpy_lru_cache(maxsize=32)
    def solve_fwd(self, m):
        x = self.qoi.generate_vector()
        x[PARAMETER].set_local(m)
        self.qoi.solveFwd(x[STATE], x)
        return x

    @numpy_lru_cache(maxsize=32)
    def solve_adj(self, m):
        x = self.solve_fwd(m)
        self.qoi.solveAdj(x[ADJOINT], x)
        return x

    @numpy_lru_cache(maxsize=32)
    def get_qoi(self, m):
        # To maintain compatibility with AdaptiveManifoldBIMC, m must be a numpy
        # array.
        x = self.solve_fwd(m)
        return np.array([self.qoi.eval(x)])

    @numpy_lru_cache(maxsize=32)
    def get_grad(self, m):
        # The forward solution should be memoized. 
        grad = self.qoi.generate_vector(component=PARAMETER)
        x = self.solve_adj(m)
        self.qoi.evalGradientParameter(x, grad)
        return grad.get_local()

    def get_hess_mv(self, m, dir):
        x = self.solve_adj(m)
        self.qoi.setLinearizationPoint(x)
        hess = ReducedHessianQOI(self.qoi, 1e-10)
        
        mv = self.qoi.generate_vector(component=PARAMETER)

        _dir = self.qoi.generate_vector(component=PARAMETER)
        _dir.set_local(dir)
        hess.mult(_dir, mv)
        return mv.get_local()

if __name__ == '__main__':

    m_np = np.array([2, 2])

    nop = NetOilProductionSimpleCompres('options_real_parameters.json')

    m = nop.qoi.generate_vector(component=PARAMETER)
    m.set_local(m_np)

    reducedQOIVerify(nop.qoi, m)



