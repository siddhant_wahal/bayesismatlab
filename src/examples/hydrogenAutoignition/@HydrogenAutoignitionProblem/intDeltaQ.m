function [int] = intDeltaQ(obj, t, y)

numTPts = length(t);
deltaQ = zeros(numTPts, 1);

for tIdx = 1 : numTPts
  deltaQ(tIdx) = obj.getCoeffsConstPressure(y(tIdx, :)');
end

int = trapz(t, -deltaQ .* y(:, obj.enum.V));

end
