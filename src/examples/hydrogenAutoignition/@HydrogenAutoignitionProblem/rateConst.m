function k = rateConst(obj, rxnId, y)

switch rxnId
  case 1
    A = 2.69e12;
    b = 0.36;
    E = 231.86e3;
    k = obj.arrhenius(A, y(obj.enum.T, :), b, E);
  case 2
    A = 3.52e16;
    b = -0.7;
    E = 71.4e3;
    k = obj.arrhenius(A, y(obj.enum.T, :), b, E);
  case 3
    A = 5.06e4;
    b = 2.7;
    E = 26.3e3;
    k = obj.arrhenius(A, y(obj.enum.T, :), b, E);
  case 4
    A = 1.17e9;
    b = 1.3;
    E = 15.2e3;
    k = obj.arrhenius(A, y(obj.enum.T, :), b, E);
  case 5
    A0 = 5.75e19;
    b0 = -1.4;
    E = 0;
    k0 = obj.arrhenius(A0, y(obj.enum.T, :), b0, E);

    AInf = 4.65e12;
    bInf = 0.4;
    kInf = obj.arrhenius(AInf, y(obj.enum.T, :), bInf, E);
    
    H2Eff = 2.5;
    H2OEff = 16.0;
    nonDimP = k0 .* (y(obj.enum.N2, :) + y(obj.enum.H, :) + y(obj.enum.O2, :) + H2Eff * y(obj.enum.H2, :) +  y(obj.enum.O, :) + y(obj.enum.OH, :) + H2OEff * y(obj.enum.H2O, :) + y(obj.enum.HO2, :)) ./ kInf;
    
    %fC = 5;
    %C = - 0.4 - 0.67 * log10(fC);
    %N = 0.75 - 1.27 * log10(fC);
    
    %f1 = (log10(nonDimP) + C) / (N - 0.14 * (log10(nonDimP) + C));
    %fallOffFunc = fC^(1 / (1 + f1 * f1)); 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Added to compare with cantera 
    fallOffFunc = 1;
    %% 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    k = kInf .* fallOffFunc .* nonDimP ./ (1 + nonDimP);
 
  otherwise
    disp('Invalid reaction ID in rateConst\n');
end  
end
