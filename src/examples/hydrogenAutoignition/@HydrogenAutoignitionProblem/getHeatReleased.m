function q = getHeatReleased(obj, y)

q = obj.qDot(1, y) + obj.qDot(2, y) + obj.qDot(3, y) + obj.qDot(4, y) + obj.qDot(5, y);

end
