function [u varargout] = getIntEnergy(obj, speciesId, temp)

[h cp] = obj.getEnthalpy(speciesId, temp);
u = h - obj.enum.univGasConst * temp;

if nargout > 1
    cv = cp - obj.enum.univGasConst;
    varargout(1) = {cv};
end

end
