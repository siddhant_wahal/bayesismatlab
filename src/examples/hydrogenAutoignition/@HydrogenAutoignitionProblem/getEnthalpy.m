function [h varargout] = getEnthalpy(obj, speciesId, temp)

cpCoeffs = obj.getCPCoeff(speciesId, temp);
h = obj.enum.univGasConst .* temp .* (cpCoeffs(1, :) + cpCoeffs(2, :) .* temp / 2 + cpCoeffs(3, :) .* temp.^2 / 3 + cpCoeffs(4, :) .* temp.^3 / 4 + cpCoeffs(5, :) .* temp.^4 / 5 + cpCoeffs(6, :) ./ temp);

if nargout > 1
  cp = obj.enum.univGasConst * (cpCoeffs(1, :) + cpCoeffs(2, :) .* temp + cpCoeffs(3, :) .* temp.^2 + cpCoeffs(4, :) .* temp.^3 + cpCoeffs(5, :) .* temp.^4);
  varargout(1) = {cp};
end

end
