function [jac, err] = getJacobian(obj, g)

addpath ~/bayesISMATLAB/src/utils/
h = 0.1 * [1; 100; 1];
dim = length(g);
grad = zeros(dim, 1);
jac = zeros(1, dim);

[grad, err] = myGrad(@(x) obj.getQOI(x), g, h);
err ./ grad
jac = grad';

end
