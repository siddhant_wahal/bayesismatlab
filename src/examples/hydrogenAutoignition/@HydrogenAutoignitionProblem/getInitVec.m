function yInit = getInitVec(obj, phi, temp, press)

    len = length(fieldnames(obj.enum)) - 1;
    yInit = zeros(len, 1);
    
    yInit(obj.enum.T) = temp;
    yInit(obj.enum.H2) = (2 * phi ./ (2 .* phi + 4.76)) .* (press ./ (10 * obj.enum.univGasConst .* yInit(obj.enum.T)'));
    yInit(obj.enum.O2) = yInit(obj.enum.H2)' ./ (2 * phi);
    yInit(obj.enum.N2) = 3.76 * yInit(obj.enum.O2);
    yInit(obj.enum.V) = 1e6;

end
