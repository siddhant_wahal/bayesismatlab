function q = qDot(obj, rxnId, y)

temp = y(obj.enum.T);

switch rxnId
  case 1
    q = obj.getRxnRate(rxnId, y) * (obj.getEnthalpy(obj.enum.H2, temp) + obj.getEnthalpy(obj.enum.O2, temp) - obj.getEnthalpy(obj.enum.H, temp) - obj.getEnthalpy(obj.enum.HO2, temp));
  case 2
    q = obj.getRxnRate(rxnId, y) * (obj.getEnthalpy(obj.enum.H, temp) + obj.getEnthalpy(obj.enum.O2, temp) - obj.getEnthalpy(obj.enum.OH, temp) - obj.getEnthalpy(obj.enum.O, temp);
  case 3
    q = obj.getRxnRate(rxnId, y) * (obj.getEnthalpy(obj.enum.O, temp) + obj.getEnthalpy(obj.enum.H2, temp) - obj.getEnthalpy(obj.enum.H, temp) - obj.getEnthalpy(obj.enum.OH, temp));
  case 4
    q = obj.getRxnRate(rxnId, y) * (obj.getEnthalpy(obj.enum.OH, temp) + obj.getEnthalpy(obj.enum.H2, temp) - obj.getEnthalpy(obj.enum.H, temp) - obj.getEnthalpy(obj.enum.H2O, temp));
  case 5
    q = obj.getRxnRate(rxnId, y) * (obj.getEnthalpy(obj.enum.H, temp) + obj.getEnthalpy(obj.enum.O2, temp) - obj.getEnthalpy(obj.enum.HO2, temp));
  otherwise
    disp('Invalid reaction ID in qDot\n');
end
end
