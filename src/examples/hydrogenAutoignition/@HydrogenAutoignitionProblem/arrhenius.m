function k = arrhenius(obj, A, temp, b, E)


k = A .* (temp.^b) .* exp( - E ./ (obj.enum.univGasConst .* temp));

end
