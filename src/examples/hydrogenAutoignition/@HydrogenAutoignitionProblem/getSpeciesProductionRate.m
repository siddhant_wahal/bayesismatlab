function [rate] = getSpeciesProductionRate(obj, speciesId, y)

numRxn = 5;
numColY = size(y, 2);
omega = zeros(numRxn, numColY);

for i = 1 : numRxn
  omega(i, :) = obj.getRxnRate(i, y);
end


switch speciesId
  case obj.enum.H2
    rate = - omega(1, :) - omega(3, :) - omega(4, :);
  case obj.enum.O2
    rate = - omega(1, :) - omega(2, :) - omega(5, :);
  case obj.enum.H
    rate = omega(1, :) - omega(2, :) + omega(3, :) + omega(4, :) - omega(5, :);
  case obj.enum.O
    rate = omega(2, :) - omega(3, :);
  case obj.enum.HO2
    rate = omega(1, :) + omega(5, :);
  case obj.enum.OH
    rate = omega(2, :) + omega(3, :) - omega(4, :);
  case obj.enum.H2O
    rate = omega(4, :);
  case obj.enum.N2
    rate = zeros(1, numColY);
  otherwise
    disp('Invalid species ID in getSpeciesProductionRate\n');
end

end
