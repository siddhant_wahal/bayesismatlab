function [coeff1 coeff2] = getCoeffsConstPressure(obj, y)


H2Prod = obj.getSpeciesProductionRate(obj.enum.H2, y);
O2Prod = obj.getSpeciesProductionRate(obj.enum.O2, y);
HProd = obj.getSpeciesProductionRate(obj.enum.H, y);
OProd = obj.getSpeciesProductionRate(obj.enum.O, y);
H2OProd = obj.getSpeciesProductionRate(obj.enum.H2O, y);
HO2Prod = obj.getSpeciesProductionRate(obj.enum.HO2, y);
OHProd = obj.getSpeciesProductionRate(obj.enum.OH, y);
N2Prod = obj.getSpeciesProductionRate(obj.enum.N2, y);

temp = y(obj.enum.T, :);

[hH2, hH2Prime] = obj.getEnthalpy(obj.enum.H2, temp);
[hO2, hO2Prime] = obj.getEnthalpy(obj.enum.O2, temp);
[hH, hHPrime] = obj.getEnthalpy(obj.enum.H, temp);
[hO, hOPrime] = obj.getEnthalpy(obj.enum.O, temp);
[hH2O, hH2OPrime] = obj.getEnthalpy(obj.enum.H2O, temp);
[hHO2, hHO2Prime] = obj.getEnthalpy(obj.enum.HO2, temp);
[hOH, hOHPrime] = obj.getEnthalpy(obj.enum.OH, temp);
[hN2, hN2Prime] = obj.getEnthalpy(obj.enum.N2, temp);

coeff1 = H2Prod .* hH2 + O2Prod .* hO2 + HProd .* hH + OProd .* hO ...
         + HO2Prod .* hHO2 + H2OProd .* hH2O + OHProd .* hOH + hN2 .* N2Prod;
coeff2 = y(obj.enum.H2, :) .* hH2Prime + y(obj.enum.O2, :) .* hO2Prime + y(obj.enum.H, :) .* hHPrime + y(obj.enum.O, :) .* hOPrime ...
         + y(obj.enum.HO2, :) .* hHO2Prime + y(obj.enum.H2O, :) .* hH2OPrime + y(obj.enum.OH, :) .* hOHPrime + y(obj.enum.N2, :) .* hN2Prime;
end
