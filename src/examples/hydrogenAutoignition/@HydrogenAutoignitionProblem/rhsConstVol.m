function [dy] = rhsConstVol(t, y, enum)

dy = zeros(size(y));

[coeff1, coeff2] = getCoeffsConstVol(y, enum);

dy(enum.H2, :) = getSpeciesProductionRate(enum.H2, y, enum);
dy(enum.O2, :) = getSpeciesProductionRate(enum.O2, y, enum);
dy(enum.H, :) = getSpeciesProductionRate(enum.H, y, enum);
dy(enum.O, :) = getSpeciesProductionRate(enum.O, y, enum);
dy(enum.HO2, :) = getSpeciesProductionRate(enum.HO2, y, enum);
dy(enum.OH, :) = getSpeciesProductionRate(enum.OH, y, enum); 
dy(enum.H2O, :) = getSpeciesProductionRate(enum.H2O, y, enum); 
dy(enum.N2, :) = getSpeciesProductionRate(enum.N2, y, enum);
dy(enum.T, :) = (- coeff1) ./ coeff2;

end
