function [dy] = rhsConstPressure(obj, t, y)

dy = zeros(size(y));

[coeff1, coeff2] = obj.getCoeffsConstPressure(y);
totalSpeciesProductionRate = obj.getSpeciesProductionRate(obj.enum.H2, y) + obj.getSpeciesProductionRate(obj.enum.O2, y) ...
                             + obj.getSpeciesProductionRate(obj.enum.H, y) + obj.getSpeciesProductionRate(obj.enum.O, y) ...
                             + obj.getSpeciesProductionRate(obj.enum.HO2, y) + obj.getSpeciesProductionRate(obj.enum.OH, y) ...
                             + obj.getSpeciesProductionRate(obj.enum.H2O, y) + obj.getSpeciesProductionRate(obj.enum.N2, y);

totalSpeciesConc = y(obj.enum.H2, :) + y(obj.enum.O2, :) + y(obj.enum.H, :) + y(obj.enum.O, :) + y(obj.enum.HO2, :) + y(obj.enum.OH, :) + y(obj.enum.H2O, :) + y(obj.enum.N2, :);

dy(obj.enum.T, :) = (- coeff1) ./ coeff2;
volumeDilation = totalSpeciesProductionRate ./ totalSpeciesConc + dy(obj.enum.T, :) / y(obj.enum.T, :);

dy(obj.enum.H2, :) = obj.getSpeciesProductionRate(obj.enum.H2, y) - y(obj.enum.H2, :) .* volumeDilation;
dy(obj.enum.O2, :) = obj.getSpeciesProductionRate(obj.enum.O2, y) - y(obj.enum.O2, :) .* volumeDilation;
dy(obj.enum.H, :) = obj.getSpeciesProductionRate(obj.enum.H, y) - y(obj.enum.H, :) .* volumeDilation;
dy(obj.enum.O, :) = obj.getSpeciesProductionRate(obj.enum.O, y) - y(obj.enum.O, :) .* volumeDilation;
dy(obj.enum.HO2, :) = obj.getSpeciesProductionRate(obj.enum.HO2, y) - y(obj.enum.HO2, :) .* volumeDilation;
dy(obj.enum.OH, :) = obj.getSpeciesProductionRate(obj.enum.OH, y) - y(obj.enum.OH, :) .* volumeDilation; 
dy(obj.enum.H2O, :) = obj.getSpeciesProductionRate(obj.enum.H2O, y) - y(obj.enum.H2O, :) .* volumeDilation; 
dy(obj.enum.N2, :) = obj.getSpeciesProductionRate(obj.enum.N2, y) - y(obj.enum.N2, :) .* volumeDilation;
dy(obj.enum.V, :) = y(obj.enum.V, :) .* volumeDilation;

end
