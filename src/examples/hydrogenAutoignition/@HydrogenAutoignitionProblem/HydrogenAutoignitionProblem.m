classdef HydrogenAutoignitionProblem

    properties

        enum = struct('H2', 1, ...
                  'O2', 2, ...
                  'H', 3, ...
                  'O', 4, ...
                  'OH', 5', ...
                  'HO2', 6, ...
                  'H2O', 7, ...
                  'N2', 8, ...
                  'T', 9, ...
                  'V', 10, ...
                  'univGasConst', 8.3144598);

        options = odeset('AbsTol', [1e-12, 1e-14, 1e-14, 1e-14, 1e-14, 1e-14, 1e-14, 1e-14, 1e-8, 1e-8], ...
                         'RelTol', 1e-6, 'Stats', 'off', 'Vectorized', 'on');
        tFinal = 5e-5;
    end

    methods
        [dy] = rhsConstPressure(obj, t, y);
        [d] = getQOI(obj, g);
        [yInit] = obj.getInitVec(obj, phi, temp, press);
        k = arrhenius(obj, A, temp, b, E)
        [coeff1 coeff2] = getCoeffsConstPressure(obj, y)
        coeffs = getCPCoeff(obj, speciesId, temp)
        [h varargout] = getEnthalpy(obj, speciesId, temp)
        q = getHeatReleased(obj, y)
        yInit = getInitVec(obj, phi, temp, press)
        [u varargout] = getIntEnergy(obj, speciesId, temp)
        [rate] = getRxnRate(obj, rxnId, y)
        [rate] = getSpeciesProductionRate(obj, speciesId, y)
        [int] = intDeltaQ(obj, t, y)
        q = qDot(obj, rxnId, y)
        k = rateConst(obj, rxnId, y)
        [jac, err] = getJacobian(obj, g)
    end
end
