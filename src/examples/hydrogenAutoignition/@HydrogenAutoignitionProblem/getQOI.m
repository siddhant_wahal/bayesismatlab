function [d] = getQOI(obj, gArr)


    numSpecies = 10;
    
    yInit = zeros(numSpecies, 1);
    
    numTPts = 501;
    tArr = linspace(0, obj.tFinal, numTPts);
    
    n = size(gArr, 2);
    d = zeros(n, 1);
    for i = 1 : n
        g = gArr(:, i);

        phi = g(1);
        temp = g(2);
        press = g(3);
        
        yInit = obj.getInitVec(phi, temp, press);
          
        [~, y] = ode15s(@(t, y) obj.rhsConstPressure(t, y), tArr, yInit, obj.options);
        d(i) = obj.intDeltaQ(tArr, y);
    end

end
