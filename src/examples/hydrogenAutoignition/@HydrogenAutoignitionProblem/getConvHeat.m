function [convHeat] = getConvHeat(t, y, enum, heatCoeff, area, tempInf)

numTPts = length(t);
integrand = zeros(numTPts, 1);

integrand = heatCoeff * area * (y(:, enum.T) - tempInf);
convHeat = trapz(t, integrand);

end
