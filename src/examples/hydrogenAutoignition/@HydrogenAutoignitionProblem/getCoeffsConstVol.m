function [coeff1 coeff2] = getCoeffsConstVol(y, enum)


H2Prod = getSpeciesProductionRate(enum.H2, y, enum);
O2Prod = getSpeciesProductionRate(enum.O2, y, enum);
HProd = getSpeciesProductionRate(enum.H, y, enum);
OProd = getSpeciesProductionRate(enum.O, y, enum);
H2OProd = getSpeciesProductionRate(enum.H2O, y, enum);
HO2Prod = getSpeciesProductionRate(enum.HO2, y, enum);
OHProd = getSpeciesProductionRate(enum.OH, y, enum);
N2Prod = getSpeciesProductionRate(enum.N2, y, enum);

temp = y(enum.T, :);

[uH2, uH2Prime] = getIntEnergy(enum.H2, temp, enum);
[uO2, uO2Prime] = getIntEnergy(enum.O2, temp, enum);
[uH, uHPrime] = getIntEnergy(enum.H, temp, enum);
[uO, uOPrime] = getIntEnergy(enum.O, temp, enum);
[uH2O, uH2OPrime] = getIntEnergy(enum.H2O, temp, enum);
[uHO2, uHO2Prime] = getIntEnergy(enum.HO2, temp, enum);
[uOH, uOHPrime] = getIntEnergy(enum.OH, temp, enum);
[uN2, uN2Prime] = getIntEnergy(enum.N2, temp, enum);

coeff1 = H2Prod .* uH2 + O2Prod .* uO2 + HProd .* uH + OProd .* uO ...
         + HO2Prod .* uHO2 + H2OProd .* uH2O + OHProd .* uOH + uN2 .* N2Prod;
coeff2 = y(enum.H2, :) .* uH2Prime + y(enum.O2, :) .* uO2Prime + y(enum.H, :) .* uHPrime + y(enum.O, :) .* uOPrime ...
         + y(enum.HO2, :) .* uHO2Prime + y(enum.H2O, :) .* uH2OPrime + y(enum.OH, :) .* uOHPrime + y(enum.N2, :) .* uN2Prime;
end
