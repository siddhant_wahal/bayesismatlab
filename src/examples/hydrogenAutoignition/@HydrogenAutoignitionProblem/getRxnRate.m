function [rate] = getRxnRate(obj, rxnId, y)

switch rxnId
  case 1
    rate = obj.rateConst(rxnId, y) .* y(obj.enum.H2, :) .* y(obj.enum.O2, :);
  case 2
    rate = obj.rateConst(rxnId, y) .* y(obj.enum.H, :) .* y(obj.enum.O2, :);
  case 3
    rate = obj.rateConst(rxnId, y) .* y(obj.enum.O, :) .* y(obj.enum.H2, :);
  case 4
    rate = obj.rateConst(rxnId, y) .* y(obj.enum.OH, :) .* y(obj.enum.H2, :);
  case 5
    rate = obj.rateConst(rxnId, y) .* y(obj.enum.H, :) .* y(obj.enum.O2, :);
  otherwise
    disp('Invalid reaction ID in getRxnRate\n');
end


end
