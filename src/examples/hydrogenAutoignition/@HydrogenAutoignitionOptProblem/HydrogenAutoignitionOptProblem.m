classdef HydrogenAutoignitionOptProblem

    properties
        problem
        prior
        options = optimoptions(@fminunc);
    end

    methods

        function optObj = HydrogenAutoignitionOptProblem(problem, prior)
            optObj.problem = problem;
            optObj.prior = prior;
            optObj.options.Algorithm = 'quasi-newton';
            optObj.options.GradObj = 'off';
            optObj.options.Display = 'iter'
            optObj.options.Hessian = 'off';
            %optObj.options.LargeScale = 'off';
            optObj.options.TolFun = 1e-8;
            optObj.options.TolX = 1e-8;
            optObj.options.FinDiffType = 'central'
            %optObj.options.TolFun = 1e-4;
            %optObj.options.TolX = 1e-4;
            %optObj.options.DiffMaxChange = 0.5;
            optObj.options.TypicalX = [1; 1000; 1];
            optObj.options.MaxFunEvals = 500;
            optObj.options.HessUpdate = 'bfgs'
        end

        [val] = objFunRegularized(obj, g, target, noiseVar);
        [d] = getQOI(obj, g);
        [map, varargout] = getMAP(obj, target, g0, noiseVar);
        [hess] = getHess(obj, g, target);
        [hess, varargout] = getGNHess(obj, g, noiseVar)

    end
end
