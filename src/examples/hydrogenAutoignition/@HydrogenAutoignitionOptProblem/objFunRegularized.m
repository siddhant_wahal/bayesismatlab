function [val] = objFunRegularized(obj, g, targetVal, noiseVar)

%filename = 'temp.out';
%dlmwrite(filename, g, '-append', 'delimiter', ' ', 'precision', '%1.14f');
totalHeat = obj.getQOI(g);

val = 0.5 * (totalHeat - targetVal)^2 / noiseVar + 0.5 * obj.prior.sqMahal(g);

end
