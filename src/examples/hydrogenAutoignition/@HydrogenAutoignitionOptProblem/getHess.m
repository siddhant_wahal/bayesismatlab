function [hess] = getHess(obj, g, target)

    dim = length(obj.prior.meanVec);
    hess = zeros(dim);
    [~, ~, hess] = obj.objFunRegularized(g, target);

end
