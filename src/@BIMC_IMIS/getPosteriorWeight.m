function [w] = getPosteriorWeight(obj)

    n = size(obj.samples, 2);
    w = zeros(n, 1);

    for i = 1 : n
        sample = obj.samples(:, i);
        %sumTerm = 0;
        %for j = 1 : obj.normals.nComp
        %    sumTerm = sumTerm ...
        %              + exp(0.5 * (obj.data - obj.obs(i)) ^ 2.0 / obj.noiseVar ...
        %                    + 0.5 * obj.optProblem.prior.sqMahal(sample) ...
        %                    - 0.5 * obj.normals.components(j).sqMahal(sample) ...
        %                    + 0.5 * obj.dets(j) + log(obj.normals.weightArr(j)));
        %end
        %w(i) = 1.0 / sumTerm;

        d = obj.obs(i);
        if d < obj.limits(2) && d > obj.limits(1)

            w(i) = exp(obj.likelihood.logpdf(d) ...
                    + obj.optProblem.prior.logpdf(sample) ...
                    - obj.normals.logpdf(sample));
        end
    end

    w = w / sum(w);


end
