classdef BIMC_IMIS < BayesianImpSampler
    
    properties
        initialized = false
        maxMixtures = 5e2
        nSamplesPerMix = 2e3;
        samples;
        obs;
        funcCount;
        dim;
        likelihood;
    end

    methods
        function obj = BIMC_IMIS(limits, optProblem, initNoiseVar)
            obj@BayesianImpSampler(limits, optProblem, initNoiseVar);
            obj.dim = numel(optProblem.prior.rnd(1));
            obj.funcCount = zeros(obj.maxMixtures, 1);
        end


        createNormals(obj);
        initialize(obj);
        adapt(obj);
        tunedGaussian = tune(obj, inputGaussian, quadEstimate);
        [weights] = getPosteriorWeight(obj);
        appendDets(obj, inputGaussian);

        [bool] = converged(obj);
        var = getOptVar(obj, x);
    end
end
