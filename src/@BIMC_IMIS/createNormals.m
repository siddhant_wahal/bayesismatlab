function createNormals(obj)
    if obj.normalsExist == false
        if obj.loadNormalsFromFile
            obj.createNormalsFromFile()
        else
            if obj.initialized == false
                obj.initialize();
            end

            obj.adapt();
        end
    end
end
