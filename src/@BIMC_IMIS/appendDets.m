function appendDets(obj, inputGaussian)
    appendTerm = sum(log(eig(obj.optProblem.prior.covMat ...
                             * inputGaussian.precisionMat)));
    obj.dets = [obj.dets, appendTerm];
end
