function tunedGaussian = tune(obj, inputGaussian, quadEstimate)

    samples = inputGaussian.rnd(1000);
    %figure(3)
    %h1 = scatter(samples(1, :), samples(2, :), 'o', ...
    %        'MarkerFaceAlpha', 0.1, ...
    %        'MarkerFaceColor', 'b', ...
    %        'MarkerEdgeColor', 'none');

    %pause
    %delete(h1);

    obs = quadEstimate.evaluate(samples);
    inside = obs < obj.limits(2) & obs > obj.limits(1);

    inSamples = samples(:, inside);
    %h2 = scatter(inSamples(1, :), inSamples(2, :), 'o', ...
    %        'MarkerFaceAlpha', 0.1, ...
    %        'MarkerFaceColor', 'r', ...
    %        'MarkerEdgeColor', 'none');

    %pause
    %delete(h2)
    tunedGaussian = Gaussian(mean(inSamples')', cov(inSamples'));

end
