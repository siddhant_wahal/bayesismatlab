function adapt(obj)
    
    if obj.initialized == false
        obj.initialize();
    end

    %initGuess = obj.normals.components(1).rnd(1);
    
    fprintf('Adaptively searching on manifold ...')
    
    j = 1;
    currSamples = zeros(obj.dim, obj.nSamplesPerMix);
    figure(2)
            
    for i = 1 : obj.normals.nComp
        plot(obj.normals.components(i).meanVec(1), ...
            obj.normals.components(i).meanVec(2), ...
            '*b', 'markersize', 20);
    end

    axis equal
    hold on
    pause


    k = obj.normals.nComp;
    totalSamplesGenerated = 0;
    while j < obj.maxMixtures
           
        %magic
        if j > 1
            if sum(1 - (1 - wPost) .^ (3000)) / 3000 >= 0.9
                break;
            end
        end
        currSamples = obj.normals.components(j).rnd(obj.nSamplesPerMix);
        totalSamplesGenerated = totalSamplesGenerated + obj.nSamplesPerMix;
        currObs = obj.getQOI(currSamples);

        obj.samples = [obj.samples, currSamples];
        obj.obs = [obj.obs, currObs];

        wPost = obj.getPosteriorWeight();
        [~, maxIdx] = max(wPost);
        newPt = obj.samples(:, maxIdx);

        figure(2)
        plot(newPt(1), newPt(2), '*b', 'markersize', 20);

        axis equal
        hold on
        pause

        %if obsMax > obj.limits(2) || obsMax < obj.limits(1)
        %    continue;
        %else
            j = j + 1;
            
            noiseVar = obj.getOptVar(newPt);

            [newGNHess, invNewGNHess] = obj.optProblem.getGNHess(newPt, noiseVar);

            quadEstimate = QuadraticEstimator(newPt, obj.optProblem.problem);
            quadEstimate.build();
            tunedGaussian = obj.tune(Gaussian(newPt, inv(newGNHess)), ...
                                     quadEstimate);
            weightArr = 1.0 / (k + j - 1) * ones(k + j - 1, 1);
            obj.normals.append(tunedGaussian, weightArr);
            %obj.appendDets(tunedGaussian);
        %end

    end

    obj.normalsExist = true;
    obj.nOpt = obj.normals.nComp;
    fprintf('Done.\n')
end
