function initialize(obj)
    
    fprintf('Initializing ... ')

    dim = length(obj.solverInitGuess);

    mid = mean(obj.limits);
    midMAP = obj.optProblem.getMAP(mid, ...
                                   obj.optProblem.prior.rnd(1), ...
                                   obj.initNoiseVar);
    
    optParamVec = obj.getOptParams(midMAP);
    
    obj.noiseVar = optParamVec(1);
    obj.data = optParamVec(2);
    obj.likelihood = Gaussian(obj.data, obj.noiseVar);

    if obj.useGNHess == true

        map = obj.optProblem.getMAP(obj.data, ...
                                    obj.solverInitGuess, ...
                                    obj.noiseVar);

        hess = obj.optProblem.getGNHess(map, obj.noiseVar);
        invHess = inv(hess);
    else
        [map, grad, hess] = obj.optProblem.getMAP(obj.data, ...
                                                  obj.solverInitGuess, ...
                                                  obj.noiseVar);
        invHess = inv(hess);
    end

    quadEstimate = QuadraticEstimator(map, obj.optProblem.problem);
    quadEstimate.build();
    tunedGaussian = tune(obj, Gaussian(map, invHess), quadEstimate);
    obj.normals = GaussianMixture(tunedGaussian.meanVec, ...
                                  tunedGaussian.covMat, 1);

    obj.normals.append(obj.optProblem.prior, [0.5, 0.5]);
    %obj.appendDets(obj.normals.components(1));
    %tempSampler = AdaptiveManifoldBIMC(obj.limits, obj.optProblem, ...
    %                                    obj.initNoiseVar);
    %tempSampler.initialize();
    %obj.likelihood = Gaussian(tempSampler.data, tempSampler.noiseVar);
    %obj.data = tempSampler.data;
    %obj.noiseVar = tempSampler.noiseVar;
    %nCharges = 7;
    %tempSampler.createNormals(nCharges);
    %obj.normals = tempSampler.normals;
    obj.initialized = true;
    fprintf('Done.\n')

end
