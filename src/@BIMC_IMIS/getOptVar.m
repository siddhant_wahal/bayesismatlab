function [var] = getOptVar(obj, x)

    linApprox = LinearizedEstimator(x, obj.optProblem.problem);
    linApprox.build();
    
    if isa(obj.optProblem.prior, 'Gaussian')
        paramOpt = ParamsOptimizer(linApprox, obj.limits, obj.optProblem.prior);
    else
        paramOpt = ParamsOptimizer(linApprox, obj.limits, ...
                                   obj.optProblem.prior.equivalentGaussian);
    end

    var = paramOpt.getOptVar(obj.getQOI(x), obj.noiseVar, ...
                                [], 1e-12, 1e-12);

end
