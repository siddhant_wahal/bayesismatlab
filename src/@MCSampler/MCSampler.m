classdef MCSampler < handle
    properties
        targetObj
        odeProblem
        prior
        nSamples = 1
        saveSamples = false
        loadFromFile = false
        samplesFileName = []
    end

    methods
        function obj = MCSampler(prior, targetObj, odeProblem)
            obj.prior = prior;
            obj.targetObj = targetObj;
            obj.odeProblem = odeProblem;
        end
        [avg, rmse] = run(obj);
        [d] = getQOI(obj, samples);
    end
end
