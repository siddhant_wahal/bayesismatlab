function [avg, rmse] = run(obj)


    if isa(obj.prior, 'Gaussian')
        dim = length(obj.prior.meanVec);
    elseif isa(obj.prior, 'GaussianMixture')
        dim = length(obj.prior.components(1).meanVec);
    end

    if obj.loadFromFile
        unpackMat = dlmread(obj.samplesFileName, ' ');
        weightArr = unpackMat(:, end);
    else
        fprintf('Generating %d MC samples\n', obj.nSamples);
        samples = zeros(dim, obj.nSamples);
        obsArr = zeros(obj.nSamples, 1);

        samples = obj.prior.rnd(obj.nSamples);
        obsArr = zeros(obj.nSamples, 1);
        weightArr = obsArr;

        obsArr = obj.getQOI(samples);
        isInsideIdx = obsArr < obj.targetObj.limits(2) & obsArr > obj.targetObj.limits(1);

        weightArr(isInsideIdx) = 1.0;

        if obj.saveSamples 
            dlmwrite(obj.samplesFileName, [samples', obsArr], 'delimiter', ' ');
        end
    end

    avg = mean(weightArr);
    rmse = sqrt(var(weightArr) / obj.nSamples) / avg;

end
