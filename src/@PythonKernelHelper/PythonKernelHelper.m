classdef PythonKernelHelper < handle

    properties
        DEBUG = true;
        kernel_mgr;
        kernel_client;
        max_msg_probe_count = 100;
    end % properties

    methods
        % TODO: Error checking for preamble, user_expression
        function obj = PythonKernelHelper()
            obj.kernel_mgr = py.jupyter_client.KernelManager();
            obj.kernel_client = [];
        end
    
        function initialize(obj, preamble)
            if ~obj.kernel_mgr.is_alive()
                obj.kernel_mgr.start_kernel();
            end
            obj.kernel_client = obj.kernel_mgr.client();
            if ~obj.kernel_client.is_alive()
                obj.kernel_client.start_channels();
            end
        
            if strlength(preamble) > 0
                % Should check for errors in preable
                msg_id = obj.kernel_client.execute(preamble);
            end
        end

        function arr = wait_for_array(obj, code, pickle_code)
            % Example:
            % code = 'var1 = np.random.rand(10)'
            % pickle_code = 'pickle.dumps(var1)'
            success = false;
            expr = py.dict(pyargs('_output', pickle_code));
            msg_id = obj.kernel_client.execute(...
                code, pyargs('user_expressions', expr));
            count = 0;
            while count < obj.max_msg_probe_count
                msg = obj.kernel_client.get_shell_msg(msg_id);
                count = count + 1;

                if ~strcmp(string(msg{'parent_header'}{'msg_id'}), string(msg_id))
                    continue
                end

                try
                    msg{'content'}{'user_expressions'}{'_output'};
                catch me
                    if obj.DEBUG
                        me.message
                    end
                    try
                        status = string(msg{'content'}{'status'});
                        if ~strcmp(status, 'ok')
                            tb = msg{'content'}{'traceback'};
                            fprintf('Code %s return with non-ok status. Traceback:\n', pickle_code);
                            cell_tb = cellfun(@char, cell(tb), 'UniformOutput', false);
                            for n = 1 : length(cell_tb)
                                disp(cell_tb{n})
                            end
                            break
                        else
                            continue
                        end
                    catch e
                        if obj.DEBUG
                            e.message
                        end 
                        continue
                    end
                end
                output_status = string(...
                    msg{'content'}{'user_expressions'}{'_output'}{'status'});
                if ~strcmp(output_status, 'ok')
                    tb = msg{'content'}{'user_expressions'}{'_output'}{'traceback'};
                    fprintf('Code %s return with non-ok status. Traceback:\n', pickle_code);
                    cell_tb = cellfun(@char, cell(tb), 'UniformOutput', false);
                    for n = 1 : length(cell_tb)
                        disp(cell_tb{n})
                    end
                    break
                end
                bytestr = msg{'content'}{'user_expressions'}{'_output'}{'data'}{'text/plain'};
                success = true;
                break
            end
            if success
                arr = double(py.pickle.loads(py.eval(bytestr, py.dict)));
            else 
                error('An error occured in the Python kernel')
                arr = [];
            end
        end

        function delete(obj)
            if obj.kernel_mgr.is_alive()
                obj.kernel_mgr.shutdown_kernel()
            end
        end % delete


    end % methods 

end % classdef
