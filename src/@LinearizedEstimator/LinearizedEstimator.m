classdef LinearizedEstimator < handle
%Class that holds linearized approximation to some forward 
%operator  A(g). That is A(g) ~~ A(g0) + J * (g - g0)
%
    properties 
        g0;
        odeProblem;
        jac = [];
        b = [];
    end

    methods
        function obj = LinearizedEstimator(g0, odeProblem)
            if nargin ~= 0
                obj.g0 = g0;
                obj.odeProblem = odeProblem;
            end
            
            [Ag0, obj.jac] = obj.odeProblem.getQOI(obj.g0); 
            obj.b = Ag0 - obj.jac * obj.g0;
        end

        function d = evaluate(obj, g)
            d = obj.jac * g + obj.b;
            d = d';
        end

        function e = err(obj, g)
            e = abs(obj.odeProblem.getQOI(g) - obj.evaluate(g));
        end

        function [p, pushFwdMean, pushFwdVar] = getLinearizedProb(obj, prior, targetMin, targetMax)
            pushFwdMean = obj.evaluate(prior.meanVec);
            pushFwdVar = obj.jac * prior.covMat * obj.jac';
            p = mvncdf(targetMax, pushFwdMean, pushFwdVar) - mvncdf(targetMin, pushFwdMean, pushFwdVar);
        end

    end
end
