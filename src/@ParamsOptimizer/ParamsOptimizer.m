classdef ParamsOptimizer
    %
    %Class to obtain the optimal parameters
    %to perform Bayesian IS with. The tunable parameters are the likelihood
    %variance and the observation data point..

    %Attributes:
    %    - interval: interval over which probability is being obtained
    %Methods:
    %    - klDist - returns KL divergence between 
    %    current IS distribution and the ideal importance distribution
    %    as a function of the likelihood variance and observation point
    %    - getOptParams - optimizes the KL divergence with respect to 
    %    the likelihood variance and observation point
    
    properties 
        linearEstimator;
        interval;
        pushFwdMean;
        pushFwdVar;
    end

    methods

        function obj = ParamsOptimizer(linearEstimator, interval, prior)
    
            obj.linearEstimator = linearEstimator;
            obj.interval = interval;
            obj.pushFwdMean = obj.linearEstimator.evaluate(prior.meanVec);
            obj.pushFwdVar = obj.linearEstimator.jac * prior.covMat * obj.linearEstimator.jac';
    
        end

        function [dKL, varargout] = getKLDist(obj, paramVec)
            
            %Returns KL dist between the current IS distribution and the ideal one
            %as a function of the vector of parameters (likelihood variance and
            %data point).
            
            %Input:
            %-- paramVec: 2x1 vector of the variance and data point
            
            %Output:
            %-- dKL: KL divergence
            %-- varargout(1): Gradient of the KL divergence w.r.t. the
            %                 parameter vector

            variance = paramVec(1);
            data = paramVec(2);
    
            pushMean = obj.pushFwdMean;
            pushVar = obj.pushFwdVar;
    
            alphaSq = variance / (variance + pushVar);
            
            [mu, truncMean, truncVar] = getTruncStats(pushMean, pushVar, obj.interval(1), obj.interval(2));
    
            dKL = (0.5 * log(alphaSq) - log(mu) + ((data - truncMean) ^ 2.0 + truncVar) / (2.0 * variance) - ...
                   ((data - pushMean) ^ 2.0) / (2.0 * (variance + pushVar)));

            if nargout > 1
    
                grad = zeros(2, 1);
                grad(1) = - ((data - truncMean) ^ 2.0 + ...
                             truncVar) / (2 * variance * variance) ...
                          + (data - pushMean) ^ 2.0 / (2.0 * (variance + ...
                          pushVar)^2.0) ...
                          + pushVar / (2.0*variance*(variance + pushVar));
                grad(2) = (data - truncMean) / variance ...
                           - (data - pushMean) / (variance + pushVar);

                varargout(1) = {grad};
            end
        end

        function [dkl, varargout] = getKLDistVsVar(obj, variance)
            %Returns KL dist between the affine push forward of the 
            %current IS distribution and the affine push forward of the 
            %ideal one
            %as a function of the likelihood variance only.             
            
            %Input:
            %-- var - likelihood variance
            
            %Output:
            %-- dkl: KL divergence between current IS distribution and the ideal
            %        one.
            %-- varargout(1): Gradient of the KL divergence w.r.t. the
            %                 the likelihood variance.
            pushMean = obj.pushFwdMean;
            pushVar = obj.pushFwdVar;
            pushMeanIS = obj.linearEstimator.evaluate(obj.linearEstimator.g0);
    
            alphaSq = variance / (variance + pushVar);
            
            [mu, truncMean, truncVar] = getTruncStats(pushMean, ...
                                                          pushVar, ...
                                                          obj.interval(1), ...
                                                          obj.interval(2));

    
            temp = (truncMean - pushMeanIS)^2  ...
                    - alphaSq * (truncMean - pushMean)^2 ...
                    + (1 - alphaSq) * truncVar;

            dkl = 0.5 * log(alphaSq) - log(mu) + 0.5 / (alphaSq * pushVar) * temp;


            if nargout > 1
                    grad = pushVar / (variance + pushVar)^2;
                    term1 = 0.5 / alphaSq;
                    term2 = - 0.5 / (pushVar * alphaSq) ...
                            * ((truncMean - pushMean)^2 + truncVar);
                    term3 = - 0.5 / (alphaSq^2 * pushVar) ...
                            * ((truncMean - pushMeanIS)^2 ...
                                + (1 - alphaSq) * truncVar ...
                               - alphaSq * (truncMean - pushMean)^2);
                    grad = grad * (term1 + term2 + term3);
                varargout(1) = {grad};
            end
        end

    
        function optParam = getOptParams(obj, initGuess, varargin)
            
            %Function to find optimal parameters by minimizing the KL divergence

            %Inputs:
            %-- initGuess: 2x1 vector of the initial guesses for 
            %   the variance and the data point
            %-- varargin(1): upper and lower bounds for constrained
            %   minimization
            %-- varargin(2): Step tolerance
            %-- varargin(3): Function tolerance
            
            bounds = [];
            tolX = 1e-6;
            tolFun = 1e-6;
            if nargin > 2, bounds = varargin{1}; end
            if nargin > 3, tolX = varargin{2}; end
            if nargin > 4, tolFun = varargin{3}; end
    
            if isempty(bounds)
                options = optimset(@fminsearch);
                options.TolFun = tolFun;
                options.TolX = tolX;
                optParam = fminsearch(@(paramVec) obj.getKLDist(paramVec), initGuess, options);
            else %fmincon
                options = optimset(@fmincon);
                options.Display = 'off';
                options.TolFun = tolFun;
                options.TolX = tolX;
                optParam = fmincon(@(paramVec) obj.getKLDist(paramVec), initGuess, [], [], [], [], bounds(1), bounds(2), [], options);
            end
        end
    
    function optParam = getOptVar(obj, initGuess, varargin)
            %Function to find optimal variance by minimizing the KL divergence

            %Inputs:
            %-- initGuess: initial guess for the variance
            %-- varargin(1): upper and lower bounds for constrained
            %   minimization
            %-- varargin(2): Step tolerance
            %-- varargin(3): Function tolerance
            

            bounds = [];
            tolX = 1e-6;
            tolFun = 1e-6;
            if nargin > 2, bounds = varargin{1}; end
            if nargin > 3, tolX = varargin{2}; end
            if nargin > 4, tolFun = varargin{3}; end
    
            if isempty(bounds)
                options = optimset(@fminsearch);
                options.TolFun = tolFun;
                options.TolX = tolX;
                options.Display = 'off';
                optParam = fminsearch(@(var) obj.getKLDistVsVar(var), ...
                                      initGuess, options);
            else
                %fmincon
                options = optimset(@fmincon);
                options.Display = 'off';
                options.TolFun = tolFun;
                options.TolX = tolX;
                optParam = fmincon(@(var) obj.getKLDistVsVar(var), ...
                                   initGuess, [], [], [], [], ...
                                   bounds(1), bounds(2), [], options);
            end
        end
 

    end
end
            

