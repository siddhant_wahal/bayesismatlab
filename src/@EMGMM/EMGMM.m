classdef EMGMM < handle

    properties
        dim;
        data;
        mixture;
        %display iterations?
        display         = false;
        %log-likelihood tolerance
        tol             = 1e-6;
        %tolerance for posterior label probabilities. All mixture components below
        %this tolerance are set to 0. If resetting probabilities is not desired,
        %set probability_tol to 0
        probability_tol = 1e-8;
        max_iter = 500;
        %log-likelihood iteration history
        llh      = [];
    end % properties

    methods 

        function obj = EMGMM(data, init_mixture)
            % Constructor
            % Inputs:
            % -- data         : input data to fit using a GMM
            % -- init_mixture : initial guess for the GMM parameters, passed as
            %                   a GaussianMixture object
            
            obj.dim     = size(data, 1);
            obj.data    = data;
            obj.mixture = init_mixture;
        end

        label_post = getLabelPosterior(obj);
        llh        = getLogLikelihood(obj, label_post);

        updateModelWtsOnly(obj, label_post);
        updateModel(obj, label_post);
        run(obj, run_wts_only);
        removeLowProbabilityComponents(obj);

    end % methods

end % classdef 
