function updateModel(obj, label_post)
% Updates the mixture. This is the M-step in the EM algorithm.
% Inputs:
% -- label_post    : Probability distribution over which the expected
%                    log-likelihood is computed, the posterior distribution of
%                    the label (hidden variable)

    weights = mean(label_post, 1); %1: average each column

    k = obj.mixture.nComp;

    means       = zeros(obj.dim, k);
    covariances = zeros(obj.dim, obj.dim, k);

    for i = 1 : k

        %new parameters are just weighted means and covariances of the data with
        %the label posteriors as the weights.
        means(:, i)          = mymean(obj.data, label_post(:, i)');
        covariances(:, :, i) = mycov(obj.data, label_post(:, i)');

    end

    non_zero = (weights > obj.probability_tol);
    
    mixture_weights = weights(non_zero);
    means = means(:, non_zero);
    covariances = covariances(:, :, non_zero);
    obj.mixture = GaussianMixture(means, covariances, weights);

end
