function llh = getLogLikelihood(obj, label_post)
% Returns the expected log-likelihood. Also, the E-step in the EM algorithm.
% Inputs:
% -- label_post    : Probability distribution over which the expected
%                    log-likelihood is computed, the posterior distribution of
%                    the label (hidden variable)
% Outputs:
% -- llh           : Expected log-likelihood

    n = size(obj.data, 2);
    k = obj.mixture.nComp;

    temp = zeros(n, k);

    for i = 1 : k
        temp(:, i) = obj.mixture.components(i).logpdf(obj.data) ...
                     + log(obj.mixture.weightArr(i));
    end

    llh = sum(sum(temp .* label_post));

end
