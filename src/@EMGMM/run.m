function run(obj, run_wts_only)
% Runs the EMGMM algorithm till convergence
% Inputs:
% -- run_wts_only : Flag that decides if only weights are to be modified

    iter_count = 0;
    obj.llh        = -Inf * ones(obj.max_iter, 1);

    while iter_count < obj.max_iter

        label_post = obj.getLabelPosterior();
        
        %M-step
        if run_wts_only
            obj.updateModelWtsOnly(label_post);
        else
            obj.updateModel(label_post);
        end
        
        iter_count = iter_count + 1;
        
        %E-step
        obj.llh(iter_count) = obj.getLogLikelihood(label_post);
        
        if obj.display 
            if iter_count == 1
                fprintf('Iter \t Log-likelihood\n\n');
            end

            fprintf('%d \t %1.6f\n', iter_count, obj.llh(iter_count));
            pause(0.01);
        end

        if iter_count > 1
            rel_err = ...
                abs(obj.llh(iter_count) - obj.llh(iter_count - 1)) / abs(obj.llh(iter_count));
            
            if rel_err < obj.tol || iter_count > obj.max_iter
                break
            end
        end
        

    end

    obj.removeLowProbabilityComponents();
end
