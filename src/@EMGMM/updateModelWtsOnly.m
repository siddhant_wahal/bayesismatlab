function updateModel(obj, label_post)
% Updates the mixture. This is the M-step in the EM algorithm.
% Inputs:
% -- label_post    : Probability distribution over which the expected
%                    log-likelihood is computed, the posterior distribution of
%                    the label (hidden variable)
    

    obj.mixture.weightArr = mean(label_post, 1); %1: average each column
    obj.mixture.weightArr = obj.mixture.weightArr / sum(obj.mixture.weightArr);

    obj.removeLowProbabilityComponents();

    if any(isnan(obj.mixture.weightArr))
        keyboard
    end


end
