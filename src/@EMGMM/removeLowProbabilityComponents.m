function removeLowProbabilityComponents(obj)

    idx = obj.mixture.weightArr > obj.probability_tol;

    obj.mixture.nComp      = sum(idx);
    obj.mixture.components = obj.mixture.components(idx);
    obj.mixture.weightArr  = obj.mixture.weightArr(idx);

    obj.mixture.weightArr  = obj.mixture.weightArr / sum(obj.mixture.weightArr);
end
