classdef GaussianMixture < matlab.mixin.Copyable
    properties
        dim
        %array of Gaussian objects
        components
        %number of components
        nComp
        %array of weights, should sum to 1
        weightArr
        %ratio of determinants : log(|(covMat_i)| / |covMat_j|)
        %is stratified sampling enabled?
        stratSampling = false;
    end

    methods
        function obj = GaussianMixture(means, covMat, weightArr)
            %means should be dim x nComp
            %covMat should be dim x dim x nComp
            %weightArr should sum to 1

            if nargin > 0
                obj.dim = size(means, 1);

                obj.nComp = size(means, 2);
                obj.weightArr = weightArr / sum(weightArr);

                %TODO: check size of means and covMat
                %TODO: assert: sum(weightArr) == 1.0
                obj.components = Gaussian(means(:, 1), covMat(:, :, 1));

                for i = 2 : obj.nComp
                    obj.components = [obj.components, ...
                                      Gaussian(means(:, i), ...
                                      covMat(:, :, i))];
                end

            end
        end

        function eqGauss = getEquivalentGaussian(obj)
            meanVec = zeros(obj.dim, 1);
            for i = 1 : obj.nComp
                meanVec = meanVec ...
                          + obj.weightArr(i) * obj.components(i).meanVec;
            end

            covMat = zeros(obj.dim);
            for i = 1 : obj.nComp
                diff = obj.components(i).meanVec - meanVec;
                covMat = covMat + obj.weightArr(i) ...
                         * (obj.components(i).covMat + diff * diff');
                             
            end

            eqGauss = Gaussian(meanVec, covMat);
        end


        function append(obj, gaussianObj, weightArr)
            %appends a component to the mixture
            %need to specify the full weightArr while appending
            obj.components = [obj.components, gaussianObj];
            obj.weightArr = weightArr;
            obj.nComp = obj.nComp + 1;

        end
        
        function [val] = pdf(obj, g)
            %val = zeros(size(g, 2), 1);

            %for i = 1 : obj.nComp
            %    val = val + obj.weightArr(i) .* obj.components(i).pdf(g);
            %end

            val = exp(obj.logpdf(g));
        end


        function [val, varargout] = logpdf(obj, g)
            %returns logarithm of the GMM PDF
            %val = log(obj.pdf(g));

            %What follows is slighly more accurate than the naive version

            n      = size(g, 2);
            logp_i = zeros(n, obj.nComp);

            for i = 1 : obj.nComp
                logp_i(:, i) = obj.components(i).logpdf(g);
            end


            logp_max = max(logp_i, [], 2);
            diff     = logp_i - logp_max;
            temp1    = exp(diff + log(obj.weightArr));
            val      = logp_max + log(sum(temp1, 2));


            if nargout > 1
                grad = zeros(obj.dim, 1);

                for i = 1 : obj.nComp
                    grad = grad - obj.weightArr(i) ... 
                                  * exp(obj.components(i).logpdf(g) - val) ...
                                  * obj.components(i).precisionMat ...
                                  * (g - obj.components(i).meanVec);
                end


                varargout{1} = grad;

                if nargout > 2
                    hess = zeros(obj.dim);

                    hess = - grad * grad';

                    for i = 1 : obj.nComp
                        temp = exp(obj.components(i).logpdf(g) - val);
                        diff = g - obj.components(i).meanVec;
                        R = obj.components(i).precisionMat;

                        hess = hess - obj.weightArr(i) * temp ...
                                      * (R - R * diff * diff' * R);

                    end

                    varargout{2} = hess;
                end

            end

        end

        function [Hx] = getHessLogPDF_mv(obj, g, x)

            [~, ~, H] = obj.logpdf(g);
            Hx = H * x;

        end



        function samples = rnd(obj, n)
            %generate n samples from the mixture
            %samples is dim x n

            if obj.stratSampling
                if rem(n, obj.nComp) == 0
                    samples = obj.stratifiedSampling(n);
                else
                    disp('Stratified sampling not supported when n % nComp != 0\n. Generating samples sequentially.\n');
                    samples = obj.sequentialSampling(n);
                end
            else
                samples = obj.sequentialSampling(n);
            end
        end

        function samples = stratifiedSampling(obj, n)
            
            m = floor(n / obj.nComp);

            samples = zeros(obj.dim, n);
            for i = 1 : obj.nComp
                samples(:, (i - 1) * m + 1 : i * m) = obj.components(i).rnd(m);
            end
        end

        function samples = sequentialSampling(obj, n)

            numSamples = mnrnd(n, obj.weightArr);
            samples = zeros(obj.dim, n);
            for i = 1 : obj.nComp
                if i == 1
                    startIdx = 1;
                else 
                    startIdx = sum(numSamples(1 : i - 1)) + 1;
                end
                endIdx = sum(numSamples(1 : i));
                samples(:, startIdx : endIdx) = obj.components(i).rnd(numSamples(i));
            end
        end

        function new = deepCopy(obj)
            new = GaussianMixture;

            prop = properties(obj);

            for i = 1 : length(prop)
                new.(prop{i}) = obj.(prop{i});
            end
        end

        function h = plot_ellipses(obj, prob_mass, c)

            h = zeros(obj.nComp, 1);

            for i = 1 : obj.nComp
                h(i) = obj.components(i).plot_ellipse(prob_mass, [c, obj.weightArr(i)]);
            end
        end
    end
end
