classdef QuadraticEstimator < handle
%Class that holds quadratic approximation to some forward 
%operator  A(g). That is A(g) ~~ A(g0) + J * (g - g0) + 0.5 (g - g0)' * H * (g - g0)
%
    properties 
        g0;
        odeProblem;
        jac;
        Ag0;
        hess;
    end

    methods
        function obj = QuadraticEstimator(g0, odeProblem)
            obj.g0             = g0;
            obj.odeProblem     = odeProblem;
            [obj.Ag0, obj.jac] = obj.odeProblem.getQOI(obj.g0); 
            [obj.hess]         = obj.odeProblem.getHess(obj.g0);
        end


        function d = evaluate(obj, g)
            
            %compute H(g0) * (g - g0) using hessian matvecs
            %hess_prod = obj.odeProblem.getHess_mv(obj.g0, g - obj.g0);
            hess_prod = obj.hess * (g - obj.g0);

            d = obj.Ag0 + obj.jac * (g - obj.g0) + 0.5 * mydot(g - obj.g0, ...
                                                         hess_prod);
            d = d';
        end

        function d = evaluateTrueMap(obj, g)
           d = obj.odeProblem.getQOI(g);
        end

        function e = err(obj, g)
            e = abs(obj.evaluateTrueMap(g) - obj.evaluate(g));
        end

    end

end
