classdef Gaussian
    properties
        meanVec;
        covMat;
        precisionMat;
        logDetCovMat;
        cholFactor;
        cholInv;
        dim;
    end
    methods
        function obj = Gaussian(meanVec, covMat)
            if nargin ~= 0
                obj.meanVec = meanVec;
                obj.covMat = sparse(covMat);
                obj.dim = numel(meanVec);


                try
                    obj.cholFactor = sparse(chol(obj.covMat, 'lower'));
                catch ME
                    keyboard
                    error('In class Gaussian: covMat is not positive definite.');
               end
                
                id = speye(obj.dim);

                cholInv          = obj.cholFactor \ id;
                obj.precisionMat = obj.cholFactor' \ cholInv;
                obj.logDetCovMat = 2 * sum(log(diag(obj.cholFactor)));
            end
        end

        function r = rnd(obj, n)
            %return n samples from the Gaussian
            %r has dimensions d x n
            % each column is a sample
            rStd = randn(obj.dim, n);
            r = obj.meanVec + (obj.cholFactor * rStd);
        end

        function [val, varargout] = logpdf(obj, g)
            val = -0.5 * (obj.dim * log(2 * pi) ...
                          + obj.logDetCovMat ...
                          + obj.sqMahal(g));

            if nargout > 1
                grad = - obj.precisionMat * (g - obj.meanVec);
                varargout{1} = grad;

                if nargout > 2
                    hess = - obj.precisionMat;
                    varargout{2} = hess;
                end
            end
        end

        function [Hx] = getHessLogPDF_mv(obj, g, x)

            [~, ~, H] = obj.logpdf(g);
            Hx = H * x;
        end

        function [val, varargout] = pdf(obj, g)
            %return pdf evaluated at g
            val = exp(obj.logpdf(g));

            if nargout > 1
                grad = - val * obj.precisionMat * (g - obj.meanVec);
                varargout{1} = grad;

                if nargout > 2
                    Hx = obj.precisionMat * (g - obj.meanVec);
                    hess = - val * obj.precisionMat + val * Hx * Hx';
                    varargout{2} = hess;
                end
            end

        end
        
        function w = sqMahal(obj, g)
            %square of the Mahalanobis distance of g from the Gaussian
            diff = g - obj.meanVec;
            w = sum(diff .* (obj.precisionMat * diff), 1).'; % 1: sum along columns

        end

        function h = plot_ellipse(obj, prob_mass, c)
            %plots ellipse corresponding to prob_mass probability mass region.
            %Only valid for 2 dimensions
            if c(4) > 0.99
                c(4) = 0.85
            end
            param = getEllipseParamFromGaussian(obj, prob_mass);
            h = plotEllipse(param, c);
        end

    end
end
