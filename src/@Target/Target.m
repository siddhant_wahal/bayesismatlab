classdef Target

    properties
        limits
        grid
        width
    end

    methods
        function targetObj = Target(limits, grid)
            targetObj.limits = limits;
            assert(limits(1) < limits(2), 'Error: limits property of Target instance must be in increasing order\n.')
            targetObj.grid = grid;
            targetObj.width = max(limits) - min(limits);
        end
    end
end
