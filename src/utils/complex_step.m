function [deriv] = complex_step(fun, x0, h)

    deriv = imag( fun(x0 + sqrt(-1) * h) ) / sqrt(h' * h);

end
