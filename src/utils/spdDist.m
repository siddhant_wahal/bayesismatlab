function dist = spdDist(A, B)
%returns distance between two spd matrices

    logLambda = log(eig(A, B));
    dist = sqrt(sum(logLambda .^2));

end
