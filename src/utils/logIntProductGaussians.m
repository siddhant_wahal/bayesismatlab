function logProduct = logIntProductGaussians(gauss1, gauss2)

    %Returns log(\int gauss1(x) * gauss2(x) dx) where gauss1 and gauss2 are two 
    %Gaussians. See Maxtrix Cookbook 8.1.8 for reference.

    newGauss   = Gaussian(gauss2.meanVec, gauss1.covMat + gauss2.covMat);
    logProduct = newGauss.logpdf(gauss1.meanVec);
end
