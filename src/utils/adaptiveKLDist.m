function [dkl, rel_err] = adaptiveKLDist(mix1, mix2, tol)
% Returns the KL divergence between the mix1 and mix2 
% The number of samples required is determined adaptively by observing
% the change in the computed value for dkl and terminating when change is below
% tol
% Inputs:
% -- mix1 : Mixture 1
% -- mix2 : Mixture 2
% -- tol  : tolerance for establishing convergence
% Outputs:
%   - dkl: The KL divergence between mix1 and mix2;

    newNum  = 1e3;
    samples = mix1.rnd(newNum);
    
    logp1 = mix1.logpdf(samples);
    logp2 = mix2.logpdf(samples);

    integrand = logp1 - logp2;
    newMean   = mean(integrand);
    newCov    = cov(integrand, 1);

    if newMean ~= 0
        rel_err = sqrt(newCov / newNum) / newMean;
    else
        rel_err = 1;
    end
   
    while rel_err > tol && newNum < 1e6

        oldMean = newMean;
        oldCov  = newCov;
        oldNum  = newNum;

        %Compute how bad the error is 
        factor = rel_err / tol;

        %Estimate *additional* number of samples required to bring rel_err below
        %tol
        increment = round((factor * factor - 1) * oldNum); % -1 : because oldNum
                                                           %      samples have
                                                           %      already been
                                                           %      generated

        % depending on tol, increment can be a really high number, 
        %so bound it below 1e4 to avoid memory issues
        if increment > 1e4
            increment = 1e4;
        end

        samples = mix1.rnd(increment);
        logp1 = mix1.logpdf(samples);
        logp2 = mix2.logpdf(samples);
        integrand = logp1 -logp2;

        [newMean, newCov] = updateMeanAndCov(integrand', oldNum, oldMean, oldCov);

        newNum = increment + oldNum;

        rel_err = sqrt(newCov / newNum) / newMean;

        if newMean ~= 0
            rel_err = sqrt(newCov / newNum) / newMean;
        else
            %if newMean is 0 again, stop iterating
            break;
        end

    end

    dkl = newMean;

end
