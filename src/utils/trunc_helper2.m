function val = trunc_helper2(x, y)

    diff = exp(x * x - y * y);

    if abs(x) > abs(y)
        val = trunc_helper2(y, x);
    elseif abs(x - y) < 1e-7
        val = trunc_helper2_taylor(x, y - x);
    elseif min(x, y) > 0
        val = (x - diff * y) / (erfcx(x) - diff * erfcx(y));
    elseif max(x, y) < 0
        val = (x - diff * y) / (diff * erfcx(-y) - erfcx(-x));
    else
        val = exp(-x * x) * (x - diff * y) / (erf(y) - erf(x));
    end

end
