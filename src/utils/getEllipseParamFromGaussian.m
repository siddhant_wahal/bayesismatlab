function [param] = getEllipseParamFromGaussian(g, prob_mass)
% Returns the parameters of the ellipse that contains the prob_mass probability
% mass of the Gaussian object g. g must be defined in R^2, and prob_mass must be
% less than 1. Parameters are defined as: x0, y0, angle, a, b. x0, y0 are the
% centers of the ellipse, angle is the angle in radians of the major axis. a and
% b are the lengths of the semimajor and semiminor axes respectively.
%
% Inputs:
% -- g         : Gaussian object. g.dim must be 2
% -- prob_mass : Probability mass value, must be less than 1
% Outputs:
% param : Parameters of the ellipse

[U, D] = eig(full(g.precisionMat));

angle = atan2(U(2, 1), U(1, 1))

factor = chi2inv(prob_mass, g.dim);
a      = sqrt(factor / D(1, 1));
b      = sqrt(factor / D(2, 2));

x0 = g.meanVec(1);
y0 = g.meanVec(2);

param = [x0, y0, angle, a, b];
