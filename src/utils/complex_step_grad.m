function [grad] = complex_step_grad(fun, x0, hInit)

dim = length(x0);
if length(hInit) == 1
    hInit = hInit * ones(dim, 1);
end

grad = zeros(dim, 1);
I = eye(dim);

for i = 1 : dim
    e_i = I(:, i);
    [grad(i)] = complex_step(fun, x0, hInit(i) * e_i);
end


end
