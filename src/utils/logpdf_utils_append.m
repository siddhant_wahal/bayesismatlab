function [lp_append] = logpdf_utils_append(newWeightArr, newComponent, samples, lp_i_old)

    lp_new = newComponent.logpdf(samples);
    lp_i_new = [lp_i_old, lp_new];

    lp_max    = max(lp_i_new, [], 2);
    diff      = lp_i_new - lp_max;
    temp1     = exp(diff + log(newWeightArr));
    lp_append = lp_max + log(sum(temp1, 2));

end
