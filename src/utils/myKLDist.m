function dkl = myKLDist(p, q, nSamples)

    samples = p.rnd(nSamples);

    logp = p.logpdf(samples);
    logq = q.logpdf(samples);

    dkl = mean(logp - logq);

end
