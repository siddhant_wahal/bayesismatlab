function [truncMu, truncMean, truncVar] = getTruncStats(...
                                                avg, variance, dMin, dMax)
% Function to get 0th, 1st, and 2nd moments of a truncated normal
% distribution. The Normal PDF is assumed to be univariate and
% truncated between dMin and dMax

% Inputs:
% -- avg: mean of original Normal distribution
% -- variance: variance of original Normal distribution
% -- dMin, dMax: Truncation limits

% Outputs:
% -- truncMu, truncAvg, truncVar: the 0th, 1st and 2nd moments of
%                                  the truncated Normal respectively
             

    sqrt2 = sqrt(2);
    sqrtpi = sqrt(pi);

    stdDev = sqrt(variance);

    alpha = (dMin - avg) / stdDev;
    beta  = (dMax - avg) / stdDev;

    alphaScaled = alpha / sqrt2;
    betaScaled  = beta / sqrt2;

    truncMu = 0.5 * erfdiff(alphaScaled, betaScaled);

    truncMean = avg + sqrt2 / sqrtpi ...
                    * stdDev * trunc_helper1(alphaScaled, betaScaled);

    truncVar = variance * (1 ...
                + 2 / sqrtpi * trunc_helper2(alphaScaled, betaScaled) ...
                - 2 / pi * (trunc_helper1(alphaScaled, betaScaled)) ^2);

end
