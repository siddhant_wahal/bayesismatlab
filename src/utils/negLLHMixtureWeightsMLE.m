function [negLLH, grad] = negLLHMixtureWeightsMLE(w, mixture, data)
% Returns the negative log likelihood of observing data given a Gaussian
% mixture, as well as the gradient of the negative log likelihood with respect
% to the mixture weights. Intended to be used in a MLE maximzation program.
% Inputs:
% -- w       : current mixture weight estimate
% -- mixture : Gaussian mixture whose log likelihood is to be estimated
% -- data    : Observed data to be used in likelihood computation
% Outputs:
% -- negLLH : Negative log likelihood
% -- grad   : gradient of the negative log likelihood w.r.t mixture weights

   k = mixture.nComp; 
   n = size(data, 2);

   mixture.weightArr = w';
   mixture_logpdf = mixture.logpdf(data);
   negLLH = - sum(mixture_logpdf);
   
   grad = zeros(k, 1);

   for i = 1 : k
       grad(i) = - sum(exp(mixture.components(i).logpdf(data) ...
                 - mixture_logpdf));
   end





end
