function diff = erfdiff_taylor(x, delta)
% Returns erf(x + delta) - erf(x), where delta is a small number


    x2 = x * x;

    c1 = 1 - x2 * (1 + 0.5 * x2);
    c2 = - x * (1 + x2);
    c3 = - 1 / 3 + x2;
    c4 = 0.5 * x;
    c5 = 0.1;

    diff = 2 / sqrt(pi) * (c1 + (c2 + (c3 ...
               + (c4 + c5 * delta) * delta) * delta) * delta) * delta;

    

end
