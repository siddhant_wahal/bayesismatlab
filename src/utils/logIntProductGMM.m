function log_product = logIntProductGMM(gaussian, gmm)

    %Returns \int gaussian(x) * gmm(x) dx where gaussian is a Gaussian
    %distribution and gmm is a Gaussian Mixture. 

    %product = 0;

    %for i = 1 : gmm.nComp

    %    product = product ...
    %              + gmm.weightArr(i) ...
    %              * intProductGaussians(gaussian, gmm.components(i));
    %end

    logp_i = zeros(gmm.nComp, 1);

    for i = 1 : gmm.nComp
        logp_i(i) = logIntProductGaussians(gaussian, gmm.components(i));
    end

    logp_max    = max(logp_i);
    p_i_scaled  = exp(logp_i - logp_max + log(gmm.weightArr)');
    log_product = logp_max + log(sum(p_i_scaled));


end
