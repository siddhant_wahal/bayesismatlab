function [val, grad, hess] = electrostaticPotential2D(x, fixedCharges)
% Function that compute the electrostatic potential in 2 dimensional space 
% due to unit charges at fixedCharges at x. 
% Inputs:
%   -- x            : query location
%   -- fixedCharges : Charges already present
% Outputs:
%   -- val  : Value
%   -- grad : Gradient of the potential function
%   -- hess : Hessian of the potential function

    val = 0.0;
    grad = zeros(size(x));

    dim = numel(x);
    hess = zeros(dim);

    constant = -1.0 / (2.0 * pi);

    for i = 1 : size(fixedCharges, 2)
        diff = x - fixedCharges(:, i);
        normDiff = sqrt(mydot(diff, diff));

        val = val + constant * log(normDiff);
        grad = grad + constant / (normDiff * normDiff) * diff;

        hessUpdate = constant * (1.0 / (normDiff * normDiff) * eye(dim) ...
                     - 2.0 / (normDiff ^ 4.0) * diff * diff');
        hess = hess + hessUpdate;

    end

end
