function diff = trunc_helper1_taylor(x, delta)
% Returns erf(x + delta) - erf(x), where delta is a small number


    sqrtpi = sqrt(pi);

    c0 = sqrtpi * x;
    c1 = sqrtpi / 2;
    c2 = - 1 / 6 * sqrtpi * x;
    c3 = - 1 / 12 * sqrtpi;
    c4 = 1 / 90 * sqrtpi * x * (x * x + 1);

    diff = c0 + (c1 + (c2 + (c3 + c4 * delta) * delta) * delta) * delta;    

end
