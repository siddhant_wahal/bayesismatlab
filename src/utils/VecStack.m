classdef VecStack < handle
    properties
        stack;
        capacity;
        sizeIncrement = 100;
        head;
        dim;
    end

    methods
        function obj = VecStack(dim)
            obj.dim = dim;
            obj.stack = zeros(obj.dim, obj.sizeIncrement);
            obj.capacity = obj.sizeIncrement;
            obj.head = 0;
        end

        function y = pop(obj)
            if obj.head > 0
                y = obj.stack(:, obj.head);
                obj.head = obj.head - 1;
            end
        end

        function y = peek(obj)
            if obj.head > 0
                y = obj.stack(:, obj.head);
            end
        end
            

        function push(obj, x)
            if obj.head >= obj.capacity
                obj.ensureExtraCapacity();
            end
            
            obj.stack(:, obj.head + 1) = x;
            obj.head = obj.head + 1;

        end

        function ensureExtraCapacity(obj)
            newStack = zeros(obj.dim, obj.capacity + obj.sizeIncrement);
            newStack(:, 1 : obj.head) = obj.stack(:, 1 : obj.head);
            obj.stack = newStack;
        end

        function out = isEmpty(obj)
            out = obj.head == 0;
        end
            
        
    end
end
