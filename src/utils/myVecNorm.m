function d = myVecNorm(a)
%Returns the 2 norm of a

    d = sqrt(a' * a);
end
