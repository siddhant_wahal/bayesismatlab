function [hess, err] = myHess_complex_step(fun, x0, hInit)

dim = length(x0);

hess = zeros(dim);
err = zeros(dim);
I = eye(dim);

for i = 1 : dim
    [hessCol, errCol] = myGrad(@(x) complex_step_grad_i(fun, x, 1e-100, i), x0, hInit);
    hess(i, :) = hessCol';
    err(i, :) = errCol';
end


end
