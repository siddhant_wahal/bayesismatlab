function [diff] = erfdiff(x, y)
% Returns erf(y) - erf(x). Modified to avoid underflow.

    if abs(x) > abs(y)

        diff = - erfdiff(y, x);

    elseif abs(x - y) < 1e-7
        
        %compute diff using taylor expansion
        diff = erfdiff_taylor(x, y - x);

    elseif min(x, y) >= 0
        
        err = exp(x * x - y * y);
        
        diff = exp(-x * x) * ( erfcx(x) - err * erfcx(y) );
    elseif max(x, y) < 0

        err = exp(x * x - y * y);

        diff = exp(-x * x) * (err * erfcx(-y) - erfcx(-x));
    else
        diff = erf(y) - erf(x);
    end

end
