function [grad] = complex_step_grad_i(fun, x0, hInit, i)

dim = length(x0);

I = eye(dim);
e_i = I(:, i);
grad = complex_step(fun, x0, hInit * e_i);


end
