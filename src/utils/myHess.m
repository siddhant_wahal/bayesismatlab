function [hess, err] = myHess(fun, x0, hInit)

dim = length(x0);

hess = zeros(dim);
err = zeros(dim);
I = eye(dim);

for i = 1 : dim
    [hessCol, errCol] = myGrad(@(x) myGrad_i(fun, x, hInit, i), x0, hInit);
    hess(i, :) = hessCol';
    err(i, :) = errCol';
end


end
