function d = mydot(A, B)
%returns the inner product of the columns of A with the columns of B

    d = sum(A .* B, 1); %1: ensures columns are summed
end
