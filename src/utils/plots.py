import numpy as np
from scipy import stats
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.colors as colors
import matplotlib.patches as mpatches
from matplotlib import rc
from sklearn import mixture
from matplotlib.patches import Ellipse 

mycolors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', 
            '#9467bd', '#8c564b', '#e377c2', 
            '#7f7f7f', '#bcbd22', '#17becf']

def varRMSEConvFigNOpt1(file1, nRuns, nVar, outFileName):
    
    #mpl.rcParams.update({'axes.labelsize': 24})
    #mpl.rcParams.update({'xtick.labelsize': 20})
    #mpl.rcParams.update({'ytick.labelsize': 20})
    #mpl.rcParams.update({'legend.fontsize': 18})
    

    pAnalytical1  = np.mean(file1[nRuns * nVar : nRuns * (nVar + 1), 3])
    
    varMin = file1[-1, 0]
    rmseMinAvg1NL  = np.mean(file1[nRuns * nVar : nRuns * (nVar + 1), 4]) / pAnalytical1
    
    file1 = file1[file1[:,0].argsort()]

    rmseAvg1NL  = [np.mean(file1[i * nRuns : (i + 1) * nRuns, 4]) / pAnalytical1 for i in range(nVar + 1)]
    
    plt.figure(figsize=[7, 2])
    #plt.figure()
    rmse1NL, = plt.loglog(file1[0::nRuns, 0], rmseAvg1NL, label='$n = 1$')
    
    plt.plot(varMin, rmseMinAvg1NL, 'o', color=mycolors[0], markersize=10)
    plt.gca().fill_between([1e-11,1e-8], 1e-3, 1, color=mycolors[0], alpha=0.2)
    plt.xlabel('$\sigma^2$')
    #plt.ylabel('$\\tilde{e}_{\mathrm{RMS}}$')
    plt.ylabel('$e_{\mathrm{IS}}$')
    #plt.loglog(nonLinN1D100[:,0], nonLinN1D100[:, 2], 'ob', alpha=0.2)
    plt.savefig(outFileName)
 

def varProbConvFig(file1, nRuns, nVar, outFileName):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})

    plt.figure()
    plt.semilogx(file1[:nRuns * nVar, 0], file1[:nRuns * nVar, 3], marker='o', linestyle='None',
            color=mycolors[0], alpha=0.5)

    pAnalytical = 0.00316933704140554
    plt.plot([file1[0, 0], file1[nRuns * nVar - 1, 0]], [pAnalytical, pAnalytical], 
            color=mycolors[0], alpha=0.5)

    plt.gca().fill_between([1e-11,1e-8], 7e-3, color=mycolors[0], alpha=0.2)

    plt.semilogx(file1[nRuns * nVar:, 0], file1[nRuns * nVar:, 3], marker='o', 
            linestyle='None', color=mycolors[1], alpha=1)

    plt.xlabel('$\sigma^2$')
    plt.ylabel('$\\tilde{\mu}$')
    plt.savefig(outFileName)


def varRMSEConvFig(file1, file5, file10, file25, nRuns, nVar, outFileName):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    
    pAnalytical1  = np.mean(file1[nRuns * nVar : nRuns * (nVar + 1), 3])
    pAnalytical5  = np.mean(file5[nRuns * nVar : nRuns * (nVar + 1), 3])
    pAnalytical10 = np.mean(file10[nRuns * nVar : nRuns * (nVar + 1), 3])
    pAnalytical25 = np.mean(file25[nRuns * nVar : nRuns * (nVar + 1), 3])
    
    varMin = file1[-1, 0]
    rmseMinAvg1NL  = np.mean(file1[nRuns * nVar : nRuns * (nVar + 1), 4]) / pAnalytical1
    rmseMinAvg5NL  = np.mean(file5[nRuns * nVar : nRuns * (nVar + 1), 4]) / pAnalytical5
    rmseMinAvg10NL = np.mean(file10[nRuns * nVar : nRuns * (nVar + 1), 4]) / pAnalytical10
    rmseMinAvg25NL = np.mean(file25[nRuns * nVar : nRuns * (nVar + 1),  4]) / pAnalytical25
    
    file1 = file1[file1[:,0].argsort()]
    file5 = file5[file5[:,0].argsort()]
    file10 = file10[file10[:,0].argsort()]
    file25 = file25[file25[:,0].argsort()]

    rmseAvg1NL  = [np.mean(file1[i * nRuns : (i + 1) * nRuns, 4]) / pAnalytical1 for i in range(nVar + 1)]
    rmseAvg5NL  = [np.mean(file5[i * nRuns : (i + 1) * nRuns, 4]) / pAnalytical5 for i in range(nVar + 1)]
    rmseAvg10NL = [np.mean(file10[i * nRuns : (i + 1) * nRuns, 4]) / pAnalytical10 for i in range(nVar + 1)]
    rmseAvg25NL = [np.mean(file25[i * nRuns : (i + 1) * nRuns, 4]) / pAnalytical25 for i in range(nVar + 1)]
    
    plt.figure()
    rmse1NL, = plt.loglog(file1[0::nRuns, 0], rmseAvg1NL, label='$n = 1$')
    rmse5NL, = plt.loglog(file5[0::nRuns, 0], rmseAvg5NL, label='$n = 5$')
    rmse10NL, = plt.loglog(file10[0::nRuns, 0], rmseAvg10NL, label='$n = 10$')
    rmse25NL, = plt.loglog(file25[0::nRuns, 0], rmseAvg25NL, label='$n = 25$')
    
    #plt.plot(varMin, rmseMinAvg1NL, 'o', color=mycolors[0], markersize=10)
    #plt.plot(varMin, rmseMinAvg5NL, 'o', color=mycolors[1], markersize=10)
    #plt.plot(varMin, rmseMinAvg10NL, 'o', color=mycolors[2], markersize=10)
    #plt.plot(varMin, rmseMinAvg25NL, 'o', color=mycolors[3], markersize=10)
    plt.legend(handles=[rmse1NL, rmse5NL, rmse10NL, rmse25NL])
    plt.xlabel('$\sigma^2$')
    plt.ylabel('$\\tilde{e}_{\mathrm{RMS}}$')
    #plt.loglog(nonLinN1D100[:,0], nonLinN1D100[:, 2], 'ob', alpha=0.2)
    plt.savefig(outFileName)
 

def varRMSEConvFigAlt(file1, file5, file10, file25, nRuns, nVar, outFileName):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    pAnalytical1  = np.mean(file1[nRuns * nVar : nRuns * (nVar + 1), 1])
    pAnalytical5  = np.mean(file5[nRuns * nVar : nRuns * (nVar + 1), 1])
    pAnalytical10 = np.mean(file10[nRuns * nVar : nRuns * (nVar + 1), 1])
    pAnalytical25 = np.mean(file25[nRuns * nVar : nRuns * (nVar + 1), 1])
    
    varMin = file1[-1, 0]
    rmseMinAvg1NL  = np.mean(file1[nRuns * nVar : nRuns * (nVar + 1), 2]) / pAnalytical1
    rmseMinAvg5NL  = np.mean(file5[nRuns * nVar : nRuns * (nVar + 1), 2]) / pAnalytical5
    rmseMinAvg10NL = np.mean(file10[nRuns * nVar : nRuns * (nVar + 1), 2]) / pAnalytical10
    rmseMinAvg25NL = np.mean(file25[nRuns * nVar : nRuns * (nVar + 1),  2]) / pAnalytical25
    
    file1 = file1[file1[:,0].argsort()]
    file5 = file5[file5[:,0].argsort()]
    file10 = file10[file10[:,0].argsort()]
    file25 = file25[file25[:,0].argsort()]

    rmseAvg1NL  = [np.mean(file1[i * nRuns : (i + 1) * nRuns, 2]) / pAnalytical1 for i in range(nVar + 1)]
    rmseAvg5NL  = [np.mean(file5[i * nRuns : (i + 1) * nRuns, 2]) / pAnalytical5 for i in range(nVar + 1)]
    rmseAvg10NL = [np.mean(file10[i * nRuns : (i + 1) * nRuns, 2]) / pAnalytical10 for i in range(nVar + 1)]
    rmseAvg25NL = [np.mean(file25[i * nRuns : (i + 1) * nRuns, 2]) / pAnalytical25 for i in range(nVar + 1)]
    
    plt.figure()
    rmse1NL, = plt.loglog(file1[0::nRuns, 0], rmseAvg1NL, label='$n = 1$')
    rmse5NL, = plt.loglog(file5[0::nRuns, 0], rmseAvg5NL, label='$n = 5$')
    rmse10NL, = plt.loglog(file10[0::nRuns, 0], rmseAvg10NL, label='$n = 10$')
    rmse25NL, = plt.loglog(file25[0::nRuns, 0], rmseAvg25NL, label='$n = 25$')
    
    #plt.plot(varMin, rmseMinAvg1NL, 'o', color=mycolors[0], markersize=8)
    #plt.plot(varMin, rmseMinAvg5NL, 'o', color=mycolors[1], markersize=8)
    #plt.plot(varMin, rmseMinAvg10NL, 'o', color=mycolors[2], markersize=8)
    #plt.plot(varMin, rmseMinAvg25NL, 'o', color=mycolors[3], markersize=8)
    plt.legend(handles=[rmse1NL, rmse5NL, rmse10NL, rmse25NL])
    plt.xlabel('$\sigma^2$')
    plt.ylabel('$\\tilde{e}_{\mathrm{RMS}}$')
    #plt.loglog(nonLinN1D100[:,0], nonLinN1D100[:, 2], 'ob', alpha=0.2)
    plt.savefig(outFileName)
 

def varRMSEConvFigAlt(file1, file5, file10, file25, nRuns, nVar, outFileName):

    pAnalytical1  = np.mean(file1[nRuns * nVar : nRuns * (nVar + 1), 1])
    pAnalytical5  = np.mean(file5[nRuns * nVar : nRuns * (nVar + 1), 1])
    pAnalytical10 = np.mean(file10[nRuns * nVar : nRuns * (nVar + 1), 1])
    pAnalytical25 = np.mean(file25[nRuns * nVar : nRuns * (nVar + 1), 1])
    
    varMin = file1[-1, 0]
    rmseMinAvg1NL  = np.mean(file1[nRuns * nVar : nRuns * (nVar + 1), 2]) / pAnalytical1
    rmseMinAvg5NL  = np.mean(file5[nRuns * nVar : nRuns * (nVar + 1), 2]) / pAnalytical5
    rmseMinAvg10NL = np.mean(file10[nRuns * nVar : nRuns * (nVar + 1), 2]) / pAnalytical10
    rmseMinAvg25NL = np.mean(file25[nRuns * nVar : nRuns * (nVar + 1),  2]) / pAnalytical25
    
    file1 = file1[file1[:,0].argsort()]
    file5 = file5[file5[:,0].argsort()]
    file10 = file10[file10[:,0].argsort()]
    file25 = file25[file25[:,0].argsort()]

    rmseAvg1NL  = [np.mean(file1[i * nRuns : (i + 1) * nRuns, 2]) / pAnalytical1 for i in range(nVar + 1)]
    rmseAvg5NL  = [np.mean(file5[i * nRuns : (i + 1) * nRuns, 2]) / pAnalytical5 for i in range(nVar + 1)]
    rmseAvg10NL = [np.mean(file10[i * nRuns : (i + 1) * nRuns, 2]) / pAnalytical10 for i in range(nVar + 1)]
    rmseAvg25NL = [np.mean(file25[i * nRuns : (i + 1) * nRuns, 2]) / pAnalytical25 for i in range(nVar + 1)]
    
    plt.figure()
    rmse1NL, = plt.loglog(file1[0::nRuns, 0], rmseAvg1NL, label='$n = 1$')
    rmse5NL, = plt.loglog(file5[0::nRuns, 0], rmseAvg5NL, label='$n = 5$')
    rmse10NL, = plt.loglog(file10[0::nRuns, 0], rmseAvg10NL, label='$n = 10$')
    rmse25NL, = plt.loglog(file25[0::nRuns, 0], rmseAvg25NL, label='$n = 25$')
    
    plt.plot(varMin, rmseMinAvg1NL, 'o', color=mycolors[0])
    plt.plot(varMin, rmseMinAvg5NL, 'o', color=mycolors[1])
    plt.plot(varMin, rmseMinAvg10NL, 'o', color=mycolors[2])
    plt.plot(varMin, rmseMinAvg25NL, 'o', color=mycolors[3])
    plt.legend(handles=[rmse1NL, rmse5NL, rmse10NL, rmse25NL], loc=4)
    plt.xlabel('$\sigma^2$')
    plt.ylabel('Relative $e_{RMS}$')
    #plt.loglog(nonLinN1D100[:,0], nonLinN1D100[:, 2], 'ob', alpha=0.2)
    plt.savefig(outFileName)
    

def varRMSEConvFigNoRelative(file1, file5, file10, file25, nRuns, nVar, outFileName):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})

    pAnalytical1  = np.mean(file1[nRuns * nVar : nRuns * (nVar + 1), 3])
    pAnalytical5  = np.mean(file5[nRuns * nVar : nRuns * (nVar + 1), 3])
    pAnalytical10 = np.mean(file10[nRuns * nVar : nRuns * (nVar + 1), 3])
    pAnalytical25 = np.mean(file25[nRuns * nVar : nRuns * (nVar + 1), 3])
    
    varMin = file1[-1, 0]
    rmseMinAvg1NL  = np.mean(file1[nRuns * nVar : nRuns * (nVar + 1), 4]) 
    rmseMinAvg5NL  = np.mean(file5[nRuns * nVar : nRuns * (nVar + 1), 4]) 
    rmseMinAvg10NL = np.mean(file10[nRuns * nVar : nRuns * (nVar + 1), 4])
    rmseMinAvg25NL = np.mean(file25[nRuns * nVar : nRuns * (nVar + 1),  4])    
    file1 = file1[file1[:,0].argsort()]
    file5 = file5[file5[:,0].argsort()]
    file10 = file10[file10[:,0].argsort()]
    file25 = file25[file25[:,0].argsort()]

    rmseAvg1NL  = [np.mean(file1[i * nRuns : (i + 1) * nRuns, 4]) for i in range(nVar + 1)]
    rmseAvg5NL  = [np.mean(file5[i * nRuns : (i + 1) * nRuns, 4]) for i in range(nVar + 1)]
    rmseAvg10NL = [np.mean(file10[i * nRuns : (i + 1) * nRuns, 4]) for i in range(nVar + 1)]
    rmseAvg25NL = [np.mean(file25[i * nRuns : (i + 1) * nRuns, 4]) for i in range(nVar + 1)]
    
    plt.figure()
    rmse1NL, = plt.loglog(file1[0::nRuns, 0], rmseAvg1NL, label='$n = 1$')
    rmse5NL, = plt.loglog(file5[0::nRuns, 0], rmseAvg5NL, label='$n = 5$')
    rmse10NL, = plt.loglog(file10[0::nRuns, 0], rmseAvg10NL, label='$n = 10$')
    rmse25NL, = plt.loglog(file25[0::nRuns, 0], rmseAvg25NL, label='$n = 25$')
    
    #plt.plot(varMin, rmseMinAvg1NL, 'o', color=mycolors[0], markersize=8)
    #plt.plot(varMin, rmseMinAvg5NL, 'o', color=mycolors[1], markersize=8)
    #plt.plot(varMin, rmseMinAvg10NL, 'o', color=mycolors[2], markersize=8)
    #plt.plot(varMin, rmseMinAvg25NL, 'o', color=mycolors[3], markersize=8)
    plt.legend(handles=[rmse1NL, rmse5NL, rmse10NL, rmse25NL], loc=4)
    plt.xlabel('$\sigma^2$')
    plt.ylabel('$\\tilde{e}_{\mathrm{RMS}}$')
    plt.gca().set_ylim([1e-3, 1e0])
    #plt.loglog(nonLinN1D100[:,0], nonLinN1D100[:, 2], 'ob', alpha=0.2)
    plt.savefig(outFileName)
 
def varDKLConvFig(file1, file5, file10, file25, nRuns, nVar, outFileName):
   
    dKLMinAvg1NL = np.mean(file1[nRuns * nVar : nRuns * (nVar + 1), -1])
    dKLMinAvg5NL = np.mean(file5[nRuns * nVar : nRuns * (nVar + 1), -1])
    dKLMinAvg10NL = np.mean(file10[nRuns * nVar : nRuns * (nVar + 1), -1])
    dKLMinAvg25NL = np.mean(file25[nRuns * nVar : nRuns * (nVar + 1),  -1])
    
    varMin = file1[-1, 0]
    file1 = file1[file1[:,0].argsort()]
    file5 = file5[file5[:,0].argsort()]
    file10 = file10[file10[:,0].argsort()]
    file25 = file25[file25[:,0].argsort()]
    
    dklAvg1NL = [np.mean(file1[i * nRuns : (i + 1) * nRuns, -1])  for i in range(nVar + 1)]
    dklAvg5NL = [np.mean(file5[i * nRuns : (i + 1) * nRuns, -1])  for i in range(nVar + 1)]
    dklAvg10NL = [np.mean(file10[i * nRuns : (i + 1) * nRuns, -1]) for i in range(nVar + 1)]
    dklAvg25NL = [np.mean(file25[i * nRuns : (i + 1) * nRuns, -1]) for i in range(nVar + 1)]

    plt.ioff()
    plt.figure()
    dkl1NL, = plt.loglog(file1[0::nRuns, 0], dklAvg1NL, '-b', label='$n_{opt} = 1$')
    dkl5NL, = plt.loglog(file5[0::nRuns, 0], dklAvg5NL, '-g', label='$n_{opt} = 5$')
    dkl10NL, = plt.loglog(file10[0::nRuns, 0], dklAvg10NL, '-c', label='$n_{opt} = 10$')
    dkl25NL, = plt.loglog(file25[0::nRuns, 0], dklAvg25NL, '-r', label='$n_{opt} = 25$')
    
    plt.plot(varMin, dKLMinAvg1NL, 'ob')
    plt.plot(varMin, dKLMinAvg5NL, 'og')
    plt.plot(varMin, dKLMinAvg10NL, 'oc')
    plt.plot(varMin, dKLMinAvg25NL, 'or')

    plt.legend(handles=[dkl1NL, dkl5NL, dkl10NL, dkl25NL], loc=4)
    plt.xlabel('$\sigma^2$')
    plt.ylabel('$D_{KL}$')
    #plt.loglog(nonLinN1D100[:,0], nonLinN1D100[:, 2], 'ob', alpha=0.2)
 
def normalizedDeviations(arr):

    avg = np.mean(arr)
    return (arr - avg) / avg

def controlVariateViolinPlot(file1DComb, fileRank1PertDim5, fileRank1PertDim10, outFileName):
    
    pCV1DComb = normalizedDeviations(file1DComb[:, 0])
    pNoCV1DComb = normalizedDeviations(file1DComb[:, 2])

    pCVr1d5 = normalizedDeviations(fileRank1PertDim5[:,0])
    pNoCVr1d5 = normalizedDeviations(fileRank1PertDim5[:,2])
    
    pCVr1d10 = normalizedDeviations(fileRank1PertDim10[:, 0])
    pNoCVr1d10 = normalizedDeviations(fileRank1PertDim10[:, 2])

    pos = [-5, 0, 5, -5, 0, 5]
    x_pos = [-5, 0, 5]
    x_labels = ['1D combustion', 'Rank 1 Pert, d = 5', 'Rank 1 Pert, d = 10']
    plt.ioff()
    plt.figure()

    parts = plt.violinplot([pCV1DComb, pCVr1d5, pCVr1d10, pNoCV1DComb, pNoCVr1d5, pNoCVr1d10], pos, showmeans=False, showextrema=False)

    for i, part in enumerate(parts['bodies']):
        part.set_color('blue')
        if (i < 3):
            part.set_alpha(0.8)
        else:
            part.set_alpha(0.3)

    #parts['cmaxes'].set_color('blue')
    #parts['cmins'].set_color('blue')
    #parts['cbars'].set_color('blue')

    plt.ylabel('$\\frac{p - \\bar{p}}{\\bar{p}}$')
    dark_patch = mpatches.Patch(color='blue', label='With CV')
    dark_patch.set_alpha(0.8)
    light_patch = mpatches.Patch(color='blue', label='Without CV')
    light_patch.set_alpha(0.3)

    plt.legend(handles=[light_patch, dark_patch])
    plt.gca().set_xticks(x_pos)
    plt.gca().set_xticklabels(x_labels)
    plt.savefig(outFileName)
    

def viz_linear_inversion_2d(mapPt, B, mc_samples, is_samples, targetMin, targetMax, mu, sigma, p, filename):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    mu = np.array(mu)
    sigma = np.array(sigma)
    xMin = 0
    xMax = 3
    yMin = 0
    yMax = 3

    n = 100
    targetArr = np.linspace(targetMin, targetMax, n)
    xArr = np.linspace(xMin, xMax, n)

    xMat = np.array([list(xArr[:]) for j in range(n)])
    yMat = np.array([[(targetArr[j] - xArr[i] * B[0]) / B[1] for j in range(n)] for i in range(n)])


    zMat = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            zMat[i, j] = stats.multivariate_normal.pdf(np.array([xArr[i], yMat[i, j]]), mean=mu, cov=sigma)

    zMat = zMat/p
    x2High1 = (targetMax - xMin * B[0]) / B[1];
    x2High2 = (targetMax - xMax * B[0]) / B[1];
    x2Low1 = (targetMin - xMin * B[0]) / B[1];
    x2Low2 = (targetMin - xMax * B[0]) / B[1];
    mc, = plt.plot(mc_samples[:, 0], mc_samples[:, 1], '+b', alpha=1, 
            label='MC', markersize=8)
    imp1, = plt.plot(is_samples[:, 0], is_samples[:, 1], '.r', alpha=1,
            label='BIMC', markersize=8)
    analytical, = plt.plot([xMin, xMax], [x2High1, x2High2], '--k', linewidth=2.5)
    plt.plot([xMin, xMax], [x2Low1, x2Low2], '--k', linewidth=2.5)
    idealDist = plt.contourf(xMat, yMat.T, zMat.T, 5, cmap='binary')# label='$q*$')
    mappt, = plt.plot(mapPt[0], mapPt[1], '*y', markersize=16,
            label='$\\boldsymbol{m}_{\mathrm{MAP}}$')
    plt.gca().set_xlim([xMin, xMax])
    plt.gca().set_ylim([yMin, yMax])
    plt.xlabel('$m_1$')
    plt.ylabel('$m_2$')

    plt.legend(handles=[mc, imp1, mappt], loc=2)

    plt.savefig(filename)

def viz_linear_inversion_2d_gmm(mapList, B, mc_samples, is_samples_1,
        is_samples_2, targetBdry, gmdist, filename):

    xMin = -0.5
    xMax = 2.5
    yMin = 0
    yMax = 3.5

    n = 100

    targetMin = targetBdry[0]
    targetMax = targetBdry[1]

    targetArr = np.linspace(targetMin, targetMax, n)
    xArr = np.linspace(xMin, xMax, n)

    xMat = np.array([list(xArr[:]) for j in range(n)])
    yMat = np.array([[(targetArr[j] - xArr[i] * B[0]) / B[1] for j in range(n)] for i in range(n)])


    zMat = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            X = np.array([xArr[i], yMat[i, j]])
            temp = gmdist.score_samples(X.reshape(1, -1))
            zMat[i, j] = temp[0]

    #zMat = zMat/p
    x2High1 = (targetMax - xMin * B[0]) / B[1];
    x2High2 = (targetMax - xMax * B[0]) / B[1];
    x2Low1 = (targetMin - xMin * B[0]) / B[1];
    x2Low2 = (targetMin - xMax * B[0]) / B[1];
    
    mc, = plt.plot(mc_samples[:, 0], mc_samples[:, 1], '+k', alpha=1, 
            markersize=6, label='Prior MC')
    
    imp1, = plt.plot(is_samples_1[:, 0], is_samples_1[:, 1], '.k', alpha=1,
            markersize=6, label='BIMC, $n$ = 1')
    
    imp2, = plt.plot(is_samples_2[:, 0], is_samples_2[:, 1], '.k', markersize=6, 
            alpha=1, label='BIMC, $n$ = 1')
    
    analytical, = plt.plot([xMin, xMax], [x2High1, x2High2], 'k')
    
    plt.plot([xMin, xMax], [x2Low1, x2Low2], 'k')
    
    idealDist = plt.contourf(xMat, yMat.T, np.exp(zMat.T), 5, cmap='binary')# label='$q*$')

    
    mappt1, = plt.plot(mapList[0][0], mapList[0][1], '*y', markersize=10, label='$x^{\mathrm{MAP}}$')
    
    mappt2, = plt.plot(mapList[1][0], mapList[1][1], '*y', markersize=10, label='$x^{\mathrm{MAP}}$')
    
    plt.gca().set_xlim([xMin, xMax])
    plt.gca().set_ylim([yMin, yMax])
    plt.xlabel('$x_1$')
    plt.ylabel('$x_2$')

    #plt.legend(handles=[mc, imp1, mappt])

    plt.savefig(filename)




def viz_nonlinear_inversion_2d(mapPt, mc_samples_inside, mc_samples, is_samples, filename):
    
    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    mc_inside, = plt.plot(mc_samples_inside[:, 0], mc_samples_inside[:, 1],
            'ok', alpha=0.07, markeredgecolor='None', label='MC')
    mc, = plt.plot(mc_samples[:, 0], mc_samples[:, 1], '+b', alpha=1, 
            markersize=8, label='MC')
    imp1, = plt.plot(is_samples[:, 0], is_samples[:, 1], marker='.',
                     linestyle='None', color='r', alpha=1,
                     label='BIMC')
    mappt, = plt.plot(mapPt[0], mapPt[1], '*y', markersize=16,
            label='$\\boldsymbol{a}_{\mathrm{MAP}}$')
    #plt.gca().set_xlim([xMin, xMax])
    #plt.gca().set_ylim([yMin, yMax])
    plt.xlabel('$a_1$')
    plt.ylabel('$a_2$')

    plt.legend(handles=[mc, imp1, mappt], loc=2)
    plt.close()

    plt.savefig(filename)



def viz_auto_inversion_2d(mapPt, mc_samples_inside, mc_samples, is_samples,
                          filename, labels):
    
    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    mc_inside, = plt.plot(mc_samples_inside[:, 0], mc_samples_inside[:, 1],
            'ok', alpha=0.2, markeredgecolor='None', label='MC')
    mc, = plt.plot(mc_samples[:, 0], mc_samples[:, 1], '+b', alpha=1, 
            markersize=8, label='MC')
    imp1, = plt.plot(is_samples[:, 0], is_samples[:, 1], marker='.',
                     linestyle='None', color='r', alpha=1,
                     label='BIMC')
    mappt, = plt.plot(mapPt[0], mapPt[1], '*y', markersize=16, label='$\\vect{x}_{\mathrm{MAP}}$')
    #plt.gca().set_xlim([xMin, xMax])
    #plt.gca().set_ylim([yMin, yMax])
    plt.xlabel(labels[0])
    plt.ylabel(labels[1])

    plt.legend(handles=[mc, imp1, mappt], loc=2)

    plt.savefig(filename)

def viz_auto_inversion_3d(mapPt, mc_samples_inside, 
                          mc_samples, is_samples, filename):
    
    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(mc_samples_inside[:,0], mc_samples_inside[:,1],
            mc_samples_inside[:,2], 'ok', alpha=0.2)#, markeredgecolor='None')
    
    mc = ax.scatter(mc_samples[:, 0], mc_samples[:, 1], mc_samples[:, 2])
                     #'+b', alpha=1, label='MC')
    imp1 = ax.scatter(is_samples[:, 0], is_samples[:, 1], is_samples[:, 2], marker='.')
                     #linestyle='None', color='r', alpha=1, label='BIMC')
    mappt = ax.scatter(mapPt[0], mapPt[1], mapPt[2], s=16, marker='*')
    # label='$x^{\mathrm{MAP}}$')
    #plt.gca().set_xlim([xMin, xMax])
    ##plt.gca().set_ylim([yMin, yMax])
    #plt.xlabel(labels[0])
    #plt.ylabel(labels[1])

    #plt.legend(handles=[mc, imp1, mappt], loc=2)

    ax.set_xlabel('$x_1$')
    ax.set_ylabel('$x_2$')
    ax.set_zlabel('$x_3$')
    plt.savefig(filename)








def err_vs_num_samples(numSamples, errIS, errMC, filename):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    imp1,  = plt.loglog(numSamples, errIS[:, 0], label='BIMC, $n$ = 1')
    imp5,  = plt.loglog(numSamples, errIS[:, 1], label='BIMC, $n$ = 5')
    imp10, = plt.loglog(numSamples, errIS[:, 2], label='BIMC, $n$ = 10')
    imp25, = plt.loglog(numSamples, errIS[:, 3], label='BIMC, $n$ = 25')
    mc,    = plt.loglog(numSamples, errMC, label='MC')

    plt.xlabel('$N$')
    plt.ylabel('$\hat{e}_{\mathrm{RMS}}, \\tilde{e}_{\mathrm{RMS}}$')
    plt.legend(handles=[mc, imp1, imp5, imp10, imp25], loc=3)

    plt.savefig(filename)

def err_vs_num_samples_siam(numSamples, errIS, errMC, filename):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    imp1,  = plt.loglog(numSamples, errIS, label='BIMC')
    mc,    = plt.loglog(numSamples, errMC, label='MC')

    plt.xlabel('$N$')
    plt.ylabel('${e}_{\mathrm{MC}}, {e}_{\mathrm{IS}}$')
    plt.legend(handles=[mc, imp1], loc=3)

    plt.savefig(filename)
    plt.cla()



def prob_level_conv(numSamples, errIS, errMC, filename):
    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    
    imp1, = plt.loglog(numSamples, errIS[:, 0], color=mycolors[0], 
                       label='$\mathcal{O}(10^{-2})$')
    imp2, = plt.loglog(numSamples, errIS[:, 1], color=mycolors[1],
                       label='$\mathcal{O}(10^{-3})$')
    imp3, = plt.loglog(numSamples, errIS[:, 2], color=mycolors[2],
                       label='$\mathcal{O}(10^{-4})$')
    imp4, = plt.loglog(numSamples, errIS[:, 3], color=mycolors[3], 
                       label='$\mathcal{O}(10^{-5})$')
    #imp5, = plt.loglog(numSamples, errIS[:, 4], color=mycolors[4], 
    #                   label='$\mathcal{O}(10^{-5})$')

    mc1, = plt.loglog(numSamples, errMC[:, 0], color=mycolors[0], linestyle='--', 
                      label='MC, $\mathcal{O}(10^{-2})$')
    mc2, = plt.loglog(numSamples, errMC[:, 1], color=mycolors[1], linestyle='--', 
                      label='MC, $\mathcal{O}(10^{-4})$')
    mc3, = plt.loglog(numSamples, errMC[:, 2], color=mycolors[2], linestyle='--', 
            label='MC, $\mathcal{O}(10^{-6})$')
    mc4, = plt.loglog(numSamples, errMC[:, 3], color=mycolors[3], linestyle='--', 
            label='MC, $\mathcal{O}(10^{-8})$')
    #mc5, = plt.loglog(numSamples, errMC[:, 4], color=mycolors[4], linestyle='--', 
    #        label='MC, $\mathcal{O}(10^{-5})$')
    plt.xlabel('$N$')
    #plt.ylabel('Relative $e_{\mathrm{RMS}}$')
    plt.ylabel('$e_{\mathrm{MC}}, e_{\mathrm{IS}}$')
    plt.legend(handles=[imp1, imp2, imp3, imp4], loc='best')
    #plt.ylabel('$\\tilde{e}_{\mathrm{RMS}}$')
    plt.savefig(filename) 
    plt.cla()

def prob_level_conv_prob(numSamples, probIS, probMC, filename):
    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    
    plt.figure(figsize=[7, 3])
    imp1, = plt.loglog(numSamples, probIS[:, 0], color=mycolors[0], 
                       label='$\mathcal{O}(10^{-2})$')
    imp2, = plt.loglog(numSamples, probIS[:, 1], color=mycolors[1],
                       label='$\mathcal{O}(10^{-3})$')
    imp3, = plt.loglog(numSamples, probIS[:, 2], color=mycolors[2],
                       label='$\mathcal{O}(10^{-4})$')
    imp4, = plt.loglog(numSamples, probIS[:, 3], color=mycolors[3], 
                       label='$\mathcal{O}(10^{-5})$')
    #imp5, = plt.loglog(numSamples, errIS[:, 4], color=mycolors[4], 
    #                   label='$\mathcal{O}(10^{-5})$')

    mc1, = plt.loglog(numSamples, probMC[:, 0], color=mycolors[0], linestyle='--', 
                      label='MC, $\mathcal{O}(10^{-2})$')
    mc2, = plt.loglog(numSamples, probMC[:, 1], color=mycolors[1], linestyle='--', 
                      label='MC, $\mathcal{O}(10^{-4})$')
    mc3, = plt.loglog(numSamples, probMC[:, 2], color=mycolors[2], linestyle='--', 
            label='MC, $\mathcal{O}(10^{-6})$')
    mc4, = plt.loglog(numSamples, probMC[:, 3], color=mycolors[3], linestyle='--', 
            label='MC, $\mathcal{O}(10^{-8})$')
    #mc5, = plt.loglog(numSamples, errMC[:, 4], color=mycolors[4], linestyle='--', 
    #        label='MC, $\mathcal{O}(10^{-5})$')
    plt.xlabel('$N$')
    plt.ylabel('$\mu$')
    plt.legend(handles=[imp1, imp2, imp3, imp4], loc='best')
    #plt.ylabel('$\\tilde{e}_{\mathrm{RMS}}$')
    plt.savefig(filename) 
    plt.cla()


def dkl_vs_sigma_y(yArr, dklArr, optParams, filename):

    NUM_COLORS = len(yArr)
    
    cm = plt.get_cmap('coolwarm')
    yMin = min(yArr)
    yMax = max(yArr)
    levels = np.linspace(yMin, yMax, 50)

    print yArr[:-1]

    Z = [[0,0], [0,0]]
    cs = plt.contourf(Z, levels, cmap=cm)
    plt.clf()

    tempColors = [cm(1. * yArr[i]) for i in range(NUM_COLORS)]

    varArr = dklArr[:, 0]
    y1,   = plt.loglog(varArr, dklArr[:, 1], color=tempColors[0])
    y2,   = plt.loglog(varArr, dklArr[:, 2], color=tempColors[1])
    y3,   = plt.loglog(varArr, dklArr[:, 3], color=tempColors[2])
    y4,   = plt.loglog(varArr, dklArr[:, 4], color=tempColors[3])
    y5,   = plt.loglog(varArr, dklArr[:, 5], color=tempColors[4])
    yOpt, = plt.loglog(varArr, dklArr[:, 6], color=tempColors[5])

    plt.plot(optParams[0], optParams[1], marker='o', color=tempColors[-1])
    plt.colorbar(cs, ticks=[0.0, 0.25, 0.5, 0.75, 1.0], label='$d$')
    #cbar = mpl.colorbar.ColorbarBase(plt.gca(), cmap=cm, ticks=[0, 0.25, 0.5, 0.75, 1])

    plt.xlabel('$\sigma^2$')
    plt.ylabel('$D_{\mathrm{KL}}$')
    plt.savefig(filename)

def scatter_contours(xArr, yArr, outfilename, isSamples=None):

    plt.figure()
    ax = plt.gca()

    cm = plt.cm.get_cmap('YlGnBu');

    sc = ax.scatter(xArr[:, 0], xArr[:, 1], c=yArr, alpha=0.5, linewidths=0.2, cmap=cm)
    if isSamples is not None:
        ax.scatter(isSamples[:, 0], isSamples[:, 1], s=5, c='red', linewidths=0.2)

    plt.colorbar(sc)
    plt.savefig(outfilename)

def null_space_var(outfilename, labelList, *args):

    for count, arr in enumerate(args):
        inside = (arr[:, 2] == 1)
        outside = ~inside
        plt.semilogy(arr[inside, 0], arr[inside, 1], linestyle='-',
                color=mycolors[count], label=labelList[count])
        plt.semilogy(arr[outside, 0], arr[outside, 1], linestyle='None', 
                marker='.', color=mycolors[count], markevery=3)

    plt.legend()
    plt.savefig(outfilename)

def frac_plot(outfilename, labelList, nOptList, *models):


    for count, arr in enumerate(models):
        plt.plot(nOptList, arr, linestyle='-', marker='o', label=labelList[count])



    ax = plt.gca()
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
    
    # Put a legend to the right of the current axis
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.75))
    plt.xlabel('$n$')
    plt.ylabel('Acceptance ratio')
    #plt.gca().set_ylim([0, 1])
    #plt.gca().set_xlim([0, 50])
    plt.savefig(outfilename)

def frac_bar_plot(outfilename, labelList, fracList, *models):


    fig = plt.figure(figsize=[7, 3])
    ax = plt.gcf().add_subplot(111)
    
    n_groups = len(labelList)
    index = np.arange(n_groups)
    bar_width = 0.5
    
    opacity = 0.4
    error_config = {'ecolor': '0.3'}
    
    rects1 = plt.bar(index, fracList, bar_width,
                     alpha=opacity,
                     color=mycolors[0])
    
    
    plt.ylabel('Acceptance ratio')
    plt.gca().set_yticks(np.linspace(0, 1, 11))
    plt.gca().set_xticklabels(labelList, rotation=0)
    plt.gca().grid('on', axis='y')
    plt.xticks(index + bar_width / 2, labelList)
    
    plt.tight_layout()

    plt.savefig(outfilename)


def taylor_green_failure_plot(outfilename, mc_samples, 
                              is_samples, mapPt, 
                              targetMin, targetMax):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    mc_samples_inside = mc_samples[np.logical_and(mc_samples[:, 2] < targetMax,
                                                  mc_samples[:, 2] > targetMin),
                                  0 : 2]
    mc_inside, = plt.plot(mc_samples_inside[:700, 0], mc_samples_inside[:700, 1],
            'ok', alpha=0.07, markeredgecolor='None', label='MC')
    mc, = plt.plot(mc_samples[:100, 0], mc_samples[:100, 1], '+b', alpha=1, 
            markersize=8, label='MC')
    imp1, = plt.plot(is_samples[:100, 0], is_samples[:100, 1], marker='.',
                     linestyle='None', color='r', alpha=1,
                     label='BIMC')
    mappt, = plt.plot(mapPt[0], mapPt[1], '*y', markersize=16,
            label='$\\boldsymbol{m}_{\mathrm{MAP}}$')
    plt.xlabel('$m_1$')
    plt.ylabel('$m_2$')
    plt.gca().set_xlim([-3, 5])
    plt.axis('equal')
    #plt.gca().set_ylim([-3, 5])

    plt.legend(handles=[mc, imp1, mappt], loc=2)
    

    plt.savefig(outfilename)

def test_non_linearity(outfilename, x, f):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    plt.figure(figsize=[3, 3])
    plt.plot(x, f, linewidth=4)
    frame1 = plt.gca()
    plt.ylabel('$f(\\bm{x}_0 + \\alpha \\bm{z})$')
    plt.xlabel('$\\alpha / \sigma$')
    plt.savefig(outfilename)
    plt.clf()
 
def test_non_linearity_1d(outfilename, x, f):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    plt.figure(figsize=[3, 3])
    plt.plot(x, f, linewidth=4)
    frame1 = plt.gca()
    plt.ylabel('$f(\\bm{x})$')
    plt.xlabel('$\\bm{x}$')
    plt.savefig(outfilename)
    plt.clf()
    
def viz_linear_ideal_IS_pdf_2d(B, \
                               targetMin, targetMax, \
                               mu, sigma, \
                               p, filename):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    mu = np.array(mu)
    sigma = np.array(sigma)
    xMin = 0
    xMax = 3
    yMin = 0
    yMax = 7

    n = 100
    targetArr = np.linspace(targetMin, targetMax, n)
    xArr = np.linspace(xMin, xMax, n)

    xMat = np.array([list(xArr[:]) for j in range(n)])
    yMat = np.array([[(targetArr[j] - xArr[i] * B[0]) / B[1] for j in range(n)] for i in range(n)])


    zMat = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            zMat[i, j] = stats.multivariate_normal.pdf(np.array([xArr[i], yMat[i, j]]), mean=mu, cov=sigma)

    zMat = zMat/p
    x2High1 = (targetMax - xMin * B[0]) / B[1];
    x2High2 = (targetMax - xMax * B[0]) / B[1];
    x2Low1 = (targetMin - xMin * B[0]) / B[1];
    x2Low2 = (targetMin - xMax * B[0]) / B[1];
    analytical, = plt.plot([xMin, xMax], [x2High1, x2High2], '--k', linewidth=2.5)
    plt.plot([xMin, xMax], [x2Low1, x2Low2], '--k', linewidth=2.5)
    
    idealDist = plt.contourf(xMat, yMat.T, zMat.T, 5, cmap='binary')# label='$q*$')
    plt.gca().set_xlim([xMin, xMax])
    plt.gca().set_ylim([yMin, yMax])
    plt.xlabel('$a_1$')
    plt.ylabel('$a_2$')

    plt.savefig(filename)
    plt.close()


def viz_linear_IS_mixture_2d(B, \
                               targetMin, targetMax, \
                               mu, sigma, \
                               centers, widths, 
                               heights, angles,
                               p, filename):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    mu = np.array(mu)
    sigma = np.array(sigma)
    xMin = 0
    xMax = 3
    yMin = 0
    yMax = 7

    n = 100
    targetArr = np.linspace(targetMin, targetMax, n)
    
    x2High1 = (targetMax - xMin * B[0]) / B[1];
    x2High2 = (targetMax - xMax * B[0]) / B[1];
    x2Low1 = (targetMin - xMin * B[0]) / B[1];
    x2Low2 = (targetMin - xMax * B[0]) / B[1];
    analytical, = plt.plot([xMin, xMax], [x2High1, x2High2], '--k', linewidth=2.5)
    plt.plot([xMin, xMax], [x2Low1, x2Low2], '--k', linewidth=2.5)
    
    #make ellipses here

    ellipses = [Ellipse(xy=centers[:, i], width=widths[i], \
                        height=heights[i], angle=angles[i]) \
                        for i in range(len(angles))]

    ax = plt.gca() 
    for ellipse in ellipses:
        ax.add_artist(ellipse)
        ellipse.set_alpha(0.2)
        ellipse.set_facecolor('b')


    plt.gca().set_xlim([xMin, xMax])
    plt.gca().set_ylim([yMin, yMax])
    plt.xlabel('$a_1$')
    plt.ylabel('$a_2$')

    print widths
    print heights
    print angles
    plt.savefig(filename)
    plt.close()


def viz_circle_ideal_IS_pdf_2d(mc_samples_inside, \
                               targetMin, targetMax, \
                               filename):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    xMin = -5
    xMax = 5
    yMin = -5
    yMax = 5


    mc_inside, = plt.plot(mc_samples_inside[0, :], mc_samples_inside[1, :],
            'ok', alpha=0.2, markeredgecolor='None', label='MC')
    ring = mpatches.Wedge([0, 0], np.sqrt(targetMax),\
                          0, 360, width=(np.sqrt(targetMax) -\
                          np.sqrt(targetMin)), facecolor='none', \
                          linestyle='dashed', linewidth=2.5)

    plt.gca().add_artist(ring)
    plt.gca().set_xlim([xMin, xMax])
    plt.gca().set_ylim([yMin, yMax])
    plt.xlabel('$a_1$')
    plt.ylabel('$a_2$')

    plt.savefig(filename)
    plt.close()


def viz_circle_IS_mixture_2d(targetMin, targetMax, \
                             centers, widths, \
                             heights, angles,\
                             filename):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})
    
    xMin = -5
    xMax = 5
    yMin = -5
    yMax = 5
    
    ring = mpatches.Wedge([0, 0], np.sqrt(targetMax),\
                          0, 360, width=np.sqrt(targetMax) -\
                          np.sqrt(targetMin), facecolor='none', \
                          linestyle='dashed', linewidth=2.5)

    plt.gca().add_artist(ring)
    
    ellipses = [Ellipse(xy=centers[:, i], width=widths[i], \
                        height=heights[i], angle=angles[i]) \
                        for i in range(len(angles))]

    ax = plt.gca() 
    for ellipse in ellipses:
        ax.add_artist(ellipse)
        ellipse.set_alpha(0.2)
        ellipse.set_facecolor('b')


    plt.gca().set_xlim([xMin, xMax])
    plt.gca().set_ylim([yMin, yMax])
    plt.xlabel('$a_1$')
    plt.ylabel('$a_2$')

    plt.savefig(filename)
    plt.close()



def essHistory(essArr, filename):

    mpl.rcParams.update({'axes.labelsize': 24})
    mpl.rcParams.update({'xtick.labelsize': 20})
    mpl.rcParams.update({'ytick.labelsize': 20})
    mpl.rcParams.update({'legend.fontsize': 18})

    plt.plot(essArr, 'ok')
    plt.ylabel('ESS')
    plt.xlabel('$k$')

    plt.savefig(filename)
    plt.close()
