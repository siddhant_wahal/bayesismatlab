function diff = trunc_helper2_taylor(x, delta)
% Returns erf(x + delta) - erf(x), where delta is a small number


    sqrtpi = sqrt(pi);

    x2 = x * x;

    c0 = 0.5 * sqrtpi * (2 * x2 - 1);
    c1 = sqrtpi * x;
    c2 = - 1 / 3 * sqrtpi * (x2 - 1);
    c3 = - 1 / 3 * sqrtpi * x;
    c4 = 1 / 90 * sqrtpi * ((2 * x2 + 3) * x2 - 8);

    diff = c0 + (c1 + (c2 + (c3 + c4 * delta) * delta) * delta) * delta;    

end
