function [lp, lp_i] = logpdf_utils(mix, samples)

    n = size(samples, 2);
    lp_i = zeros(n, mix.nComp);

    for i = 1 : mix.nComp
        lp_i(:, i) = mix.components(i).logpdf(samples);
    end

    lp_max = max(lp_i, [], 2);
    diff   = lp_i - lp_max;
    temp1  = exp(diff + log(mix.weightArr));
    lp     = lp_max + log(sum(temp1, 2));

end
