function [weights] = getMixtureWeightsMLE(data, init_mixture, varargin)
% Returns the ML estimate of mixture weights given some data. Direct
% (constrained) optimization is used to obtain this ML estimate, as opposed to
% other iterative algorithms such as EM.
%
% Inputs:
% -- data         : observed data, should be dim x nObs
% -- init_mixture : initial guess for the mixture, a GaussianMixture object
% -- varargin     : tolerances for the optimization routine
% Output:
% -- weights : MLE weights for the Gaussian mixture

    %default tolerances
    tol_x   = 1e-6;
    tol_fun = 1e-6;

    %override default tolerances above if nargin > 2
    %too bad MATLAB doesn't do default arguments
    if nargin > 2, tol_x   = varargin{1}; end
    if nargin > 3, tol_fun = varargin{2}; end

    options                          = optimoptions(@fmincon);
    options.Algorithm                = 'sqp'
    options.OptimalityTolerance      = tol_fun;
    options.StepTolerance            = tol_x;
    options.Display                  = 'off';
    options.SpecifyObjectiveGradient = true;
    options.CheckGradients           = true;
    options.FiniteDifferenceType     = 'central';
    options.ConstraintTolerance      = 1e-8

    % weights must sum to 1 constraint formulated as a linear equality constraint
    Aeq = ones(1, init_mixture.nComp); 
    beq = 1;

    weights = fmincon(@(w) negLLHMixtureWeightsMLE(w, init_mixture, data), ...
                      init_mixture.weightArr', ...
                      [], [], ... % linear inequality constraints empty
                      Aeq, beq, ... % linear equality constraints
                      [], [], [], ... % upper bound, lower bound, nonlinear constraints empty
                      options);

end
