function [h] = plotEllipse(param, clr)
% Plots ellipse given by parameters in param. The parameters must be the
% following:

x0    = param(1); %x coordinate of center
y0    = param(2); %y coordinate of center
angle = param(3); %angle between major axis and x-axis in radians
a     = param(4); %length of semimajor axis
b     = param(5); %length of semiminor axis

%c must be an [R G B alpha] vector that sets the size and transparency of the
%patch object

t = linspace(0, 2 * pi, 360);

s = sin(angle);
c = cos(angle);

xvec = x0 + a * cos(t) * c - b * sin(t) * s;
yvec = y0 + a * cos(t) * s + b * sin(t) * c;

%returns the patch object

patch(xvec, yvec, clr(1:3), 'FaceAlpha', clr(4), 'EdgeColor', clr(1:3),...
      'EdgeAlpha', 0.8, 'LineWidth', 2.5);
hold on
plot(x0, y0, 'Marker', '.', 'MarkerSize', 15, 'MarkerFaceColor', clr(1:3), ...
     'MarkerEdgeColor', clr(1:3))
h = gcf
end
