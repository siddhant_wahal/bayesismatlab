function [val, grad, hess] = electrostaticPotentialGenDWeighted(x, W, fixedCharges, exponent)
% Function that compute the repulsie potential in m dimensional space
% due to charges at fixedCharges at x. 
% Inputs:
%   -- x            : query location
%   -- W            : Scaling matrix for the inner product
%                     Cell array of dim x dim matrices with length numFixedCharges
%   -- fixedCharges : Charges already present
%   -- exponent     : Ambient dimension in which the charges are embedded. Need
%                     not be equal to the length of x, must be >= 3.
% Outputs:
%   -- val  : Value
%   -- grad : Gradient of the potential function
%   -- hess : Hessian of the potential function

    dim    = numel(x);
    
    val  = 0.0;
    grad = zeros(dim, 1);
    hess = zeros(dim);

    exponentBy2 = exponent / 2;

    for i = 1 : size(fixedCharges, 2)
        
        diff     = x - fixedCharges(:, i);
        normDiff = transpose(diff) *  (W{i} * diff);

        val  = val + 1.0 / normDiff ^ (exponentBy2 - 1);
        
        grad = grad - 2.0 * (exponentBy2 - 1) / (normDiff ^ exponentBy2) * (W{i} * diff);

        hessUpdate = -2.0 * (exponentBy2 - 1) / (normDiff ^ exponentBy2) * W{i} ...
                     + 4.0 * (exponentBy2 - 1) * (exponentBy2) ...
                       / (normDiff ^ (exponentBy2 + 1)) * (W{i} * diff) * (W{i} * diff)';
        hess       = hess + hessUpdate;
    end

end
