function [updatedMean, updatedCov] = updateMeanAndCov(newData, oldNum, ...
                                                      oldMean, oldCov)
% Returns the updated mean and covariance matrix that result from adding vectors in
% newData to vectors in the original data set. The vectors in the original data
% set aren't required as their mean, covariance and cardinality are enough to
% compute the updated mean and covariance.
%
% Inputs:
% -- newData : Array containing new data vectors, should be dim x nObs
% -- oldNum  : Number of vectors in the original data set
% -- oldMean : Mean of the old data set
% -- oldCov  : Covariance matrix of the original data set
%
% Outputs:
% -- updatedMean : Mean of the combined data set
% -- updatedCov  : Covariance of the combined data set

%Compute the mean and covariance of the new data set

dim = numel(oldMean);

if isempty(newData)
    updatedMean = oldMean;
    updatedCov  = oldCov;
else
    newNum   = size(newData, 2);
    totalNum = newNum + oldNum;
    newFrac  = newNum / totalNum;
    oldFrac  = oldNum / totalNum;
    
    newMean = mean(newData, 2); % 2 : take mean along rows
    if size(newData, 2) == 1
        newCov = zeros(dim);
    else
        newCov  = cov(newData', 1);
    end
    
    updatedMean = newFrac * newMean + oldFrac * oldMean;
    
    diffMean = oldMean - newMean;
    
    updatedCov  = newFrac * newCov + oldFrac * oldCov ...
                  + newFrac * oldFrac * diffMean * diffMean';
    
end
end
