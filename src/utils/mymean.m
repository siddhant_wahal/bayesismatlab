function avg = mymean(x, varargin)
% Returns the mean assuming x is an d x n matrix of n d-dimensional observations
% Inputs:
% -- x           : matrix of samples, should be d x n
% -- varargin(1) : array (1 x n) of weights if weighted mean is needed
% Outputs: 
% -- avg : sample mean

    n   = size(x, 2);

    if nargin > 1
        w = cell2mat(varargin(1));
        w = w / sum(w);
    else
        w = 1 / n * ones(1, n);
    end
    
    avg = sum(w .* x, 2);

end
