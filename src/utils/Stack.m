classdef Stack < handle
    properties
        stack;
        capacity;
        sizeIncrement = 100;
        head;
    end

    methods
        function obj = Stack()
            obj.stack = zeros(obj.sizeIncrement, 1);
            obj.capacity = obj.sizeIncrement;
            obj.head = 0;
        end

        function y = pop(obj)
            if obj.head > 0
                y = obj.stack(obj.head);
                obj.head = obj.head - 1;
            end
        end

        function y = peek(obj)
            if obj.head > 0
                y = obj.stack(obj.head)
            end
        end
            

        function push(obj, x)
            if obj.head >= obj.capacity
                obj.ensureExtraCapacity();
            end
            
            obj.stack(obj.head + 1) = x;
            obj.head = obj.head + 1;

        end

        function ensureExtraCapacity(obj)
            newStack = zeros(obj.capacity + obj.sizeIncrement, 1);
            newStack(1 : obj.head) = obj.stack(1 : obj.head);
            obj.stack = newStack;
        end

        function out = isEmpty(obj)
            out = obj.head == 0;
        end
            
        
    end
end
