function [prob, rel_rmse] = bimc_gmm_wrapper(limits, ...
                                             opt_problem, ...
                                             gmm, ...
                                             num_samples)

% Wrapper function that computes the rare event probability when the nominal
% distribution is a Gaussian Mixture. Proceeds by running the ABIMC algorithm on
% each component of the gmm, and then combining the probability from each
% component.
% Inputs:
% -- limits      : Limits of the target 
% -- problem     : Forward problem
% -- gmm         : Nominal distribution, must be a GaussianMixture object
% -- num_samples : Number of samples

    init_noise_var = 0.001 * (limits(2) - limits(1)) ^2;
    
    component_prob = zeros(gmm.nComp, 1);
    component_var  = zeros(gmm.nComp, 1);

    for i = 1 : gmm.nComp

        opt_problem.prior = gmm.components(i);
        imp_sampler = AdaptiveManifoldBIMC(limits, ...
                                           opt_problem, ...
                                           init_noise_var);
        imp_sampler.initialize();
        imp_sampler.createNormals();
        imp_sampler.nSamples = num_samples;
        [component_prob(i), rel_rmse] = imp_sampler.run()
        component_var(i) = (rel_rmse * component_prob(i)) ^2;

        clear imp_sampler;
    end

    prob = sum(gmm.weightArr' .* component_prob);
    rel_rmse = (sqrt(sum(gmm.weightArr .^2 * component_var))) / prob;

end
