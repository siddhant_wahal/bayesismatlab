function [updatedMean] = updateMean(newData, oldNum, oldMean)
% Returns the updated mean that results from adding vectors in
% newData to vectors in the original data set. The vectors in the original data
% set aren't required as their mean and cardinality are enough to
% compute the updated mean.
%
% Inputs:
% -- newData : Array containing new data vectors, should be dim x nObs
% -- oldNum  : Number of vectors in the original data set
% -- oldMean : Mean of the old data set
%
% Outputs:
% -- updatedMean : Mean of the combined data set

%Compute the mean and covariance of the new data set

if isempty(newData)
    updatedMean = oldMean;
else
    newNum   = size(newData, 2);
    totalNum = newNum + oldNum;
    newFrac  = newNum / totalNum;
    oldFrac  = oldNum / totalNum;
    
    newMean = mean(newData, 2); % 2 : take mean along rows
    
    updatedMean = newFrac * newMean + oldFrac * oldMean;
    
end
end
