function [dkl, rel_err, varargout] = continuationAdaptiveKLDist(mix1, newWeightArr, newComponent, tol, varargin)
% Returns the KL divergence between the mix1 and mix1 + (newWeight, newComponent) 
% Reuses samples from a previous attempt to compute the KL divergence
% The number of samples required is determined adaptively by observing
% the change in the computed value for dkl and terminating when change is below
% tol
% Inputs:
% -- mix1         : Mixture 1
% -- newWeightArr : Mixture weights of the new mixture 
% -- newComponent : The component to be added
% -- tol          : tolerance for establishing convergence
% -- varargin{1}  : pre-computed samples
% -- varargin{2}  : array containing logpdf values of each component of mix1
% -- varargin{3}  : array containing logpdf values of mix1
% Outputs:
% -- dkl: The KL divergence between mix1 and mix2;
% -- varargout{1} : samples used to compute the KL divergence
% -- varargout{2} : array containing logpdf values of each component of mix1
% -- varargout{3} : array containing logpdf values of mix1

    if nargin == 4
    
        newNum  = 1e3;
        samples = mix1.rnd(newNum);

        [logp1, logp1_i] = logpdf_utils(mix1, samples);
        
        allSamples  = [samples];
        allLogPDF   = [logp1];
        allLogPDF_i = [logp1_i];

    elseif nargin == 7 

        samples = varargin{1};
        logp1_i = varargin{2};
        logp1   = varargin{3};
        newNum   = size(samples, 2);

    else 

        ME = MException('continuationAdaptiveKLDist():TypeError', ...
                        'Incorrect number of input arguments');
        throw(ME);
    end
        
        
    logp2 = logpdf_utils_append(newWeightArr, newComponent, samples, logp1_i);

    integrand = logp1 - logp2;
    newMean   = mean(integrand);
    newCov    = cov(integrand, 1);
    
    if newMean ~= 0
        rel_err = sqrt(newCov / newNum) / newMean;
    else
        rel_err = 1;
    end
    
    while rel_err > tol && newNum < 1e6
    
        oldMean = newMean;
        oldCov  = newCov;
        oldNum  = newNum;
    
        %Compute how bad the error is 
        factor = rel_err / tol;
    
        %Estimate *additional* number of samples required to bring rel_err below
        %tol
        increment = round((factor * factor - 1) * oldNum); % -1 : because oldNum
                                                           %      samples have
                                                           %      already been
                                                           %      generated
    
        % depending on tol, increment can be a really high number, 
        %so bound it below 1e4 to avoid memory issues
        if increment > 1e4
            increment = 1e4;
        end
    
        samples = mix1.rnd(increment);
        
        [logp1, logp1_i] = logpdf_utils(mix1, samples);
        logp2 = logpdf_utils_append(newWeightArr, newComponent, samples, logp1_i);

        integrand = logp1 - logp2;
    
        [newMean, newCov] = updateMeanAndCov(integrand', oldNum, oldMean, oldCov);
    
        newNum = increment + oldNum;
    
        rel_err = sqrt(newCov / newNum) / newMean;
        
        if nargout > 2
            allLogPDF   = [allLogPDF; logp1];
            allLogPDF_i = [allLogPDF_i; logp1_i];
            allSamples  = [allSamples, samples];
        end
    
        if newMean ~= 0
            rel_err = sqrt(newCov / newNum) / newMean;
        else
            %if newMean is 0 again, stop iterating
            break;
        end
    
    end
    
    dkl = newMean;

    if nargout > 2

        varargout(1) = {allSamples};

        if nargout > 3

            varargout(2) = {allLogPDF_i};

            if nargout > 4
                varargout(3) = {allLogPDF};
            end

        end

    end
   
end
