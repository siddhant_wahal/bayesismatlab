function s = mycov(x, varargin)
% Returns the covariance assuming x is a d x n matrix of n 
%d-dimensional observations
% Inputs:
% -- x           : matrix of samples, should be d x n
% -- varargin(1) : array(1 x n) of weights if a weighted covariance is required
% Outputs: 
% -- s : sample cov

    n = size(x, 2);

    if nargin > 1
        w = cell2mat(varargin(1));
        w =  w / sum(w);
    else
        w = 1 / n * ones(1, n); 
    end
    
    avg        = mymean(x, w);
    x_centered = x - avg;

    s = (( w .* x_centered) * x_centered' );

end
