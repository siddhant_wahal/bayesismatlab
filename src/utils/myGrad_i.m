function [grad, err] = myGrad_i(fun, x0, hInit, i)

dim = length(x0);

I = eye(dim);
e_i = I(:, i);
[grad, err] = myDerivative(fun, x0, hInit * e_i);


end
