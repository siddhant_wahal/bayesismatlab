function [val, grad, hess] = electrostaticPotentialGenD(x, fixedCharges, exponent)
% Function that compute the electrostatic potential in m dimensional space
% due to unit charges at fixedCharges at x. 
% Inputs:
%   -- x            : query location
%   -- fixedCharges : Charges already present
% Outputs:
%   -- val  : Value
%   -- grad : Gradient of the potential function
%   -- hess : Hessian of the potential function

    dim    = numel(x);
    dimBy2 = 0.5 * dim;
    
    val  = 0.0;
    grad = zeros(dim, 1);
    hess = zeros(dim);


    constant = 1.0;

    for i = 1 : size(fixedCharges, 2)
        
        diff     = x - fixedCharges(:, i);
        normDiff = sqrt(mydot(diff, diff));

        %val  = val + constant / normDiff ^ (dim - 2);
        val  = val + constant / normDiff ^ (exponent - 2);
        
        %grad = grad - constant  * (dim - 2) / (normDiff ^ dim) * diff;
        grad = grad - constant  * (exponent - 2) / (normDiff ^ exponent) * diff;

        %hessUpdate = constant * ((- 1.0 / (normDiff) ^ dim) * eye(dim) ...
        %             + dim * (dim - 2) / (normDiff ^ (dim + 2)) * diff * diff');
        
        hessUpdate = constant * ((- 1.0 / (normDiff) ^ exponent) * speye(dim) ...
                     + exponent * (exponent - 2) / (normDiff ^ (exponent + 2)) * diff * diff');
        hess       = hess + hessUpdate;
    end

end
