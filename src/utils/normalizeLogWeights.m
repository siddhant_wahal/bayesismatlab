function normalizedWts = normalizeLogWeights(logWts)
% Return [w_1 / sum(w_i), w_2 / sum(w_i), ...] given an array containing 
% [log(w_1), log(w_2), log(w_3), ...].

    bigNum = max(logWts);
    temp   = exp(logWts - bigNum);
    
    normalizedWts = temp / sum(temp);

end
