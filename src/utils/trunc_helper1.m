function val = trunc_helper1(x, y)

    diff = exp(x * x - y * y);

    if abs(x) > abs(y)
        val = trunc_helper1(y, x);
    elseif abs(x - y) < 1e-7
        val = trunc_helper1_taylor(x, y - x);
    elseif min(x, y) > 0
        val = (1 - diff) / (erfcx(x) - diff * erfcx(y));
    elseif max(x, y) < 0
        val = (1 - diff) / (diff * erfcx(-y) - erfcx(-x));
    else
        val = exp(-x * x) * (1 - diff) / (erf(y) - erf(x));
    end

end
