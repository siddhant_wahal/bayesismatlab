function [samples, is_weights] = generateSamples(obj)
% Generates samples from the current mixture and returns the corresponding
% self-normalized importance sampling weights
%
% Outputs:
% -- samples    : Samples from the current mixture
% -- is_weights : Corresponding importance sampling weights

    %generate samples
    samples = obj.mixture.rnd(obj.n_samples_per_iter);
    %evaluate targetPDF at samples
    target = obj.targetPDF(samples);
    
    is_weights = exp(log(target) - obj.mixture.logpdf(samples));
    is_weights = is_weights / sum(is_weights);

end
