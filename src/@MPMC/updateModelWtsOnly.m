function updateModel(obj, samples, is_weights, label_post)
% Updates the mixture weights only. This is the M-step in the EM algorithm.
% Inputs:
% -- samples    : Samples from the current mixture, this serves as the data in
%                 the analogy with the EM algorithm
% -- is_weights : Self-normalized importance sampling weight corresponding to
%                 samples
% -- label_post : Probability distribution over which the expected
%                 log-likelihood is computed, the posterior distribution of
%                 the label (hidden variable)
    

    obj.mixture.weightArr = sum(is_weights .* label_post, 1); %1: average each column
    obj.mixture.weightArr = obj.mixture.weightArr / sum(obj.mixture.weightArr);

    if any(isnan(obj.mixture.weightArr))
        keyboard
    end

    obj.removeLowProbabilityComponents();
end
