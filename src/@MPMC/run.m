function run(obj, run_wts_only)
% Runs the MPMC function till convergence
% Inputs:
% -- run_wts_only : Flag that decides if only weights are to be modified

    iter_count = 0;
    obj.norm_perp = -Inf * ones(obj.max_iter, 1);

    while iter_count < obj.max_iter

        [samples, is_weights] = obj.generateSamples();
        
        label_post = obj.getLabelPosterior(samples);
        
        %M-step
        if run_wts_only
            obj.updateModelWtsOnly(samples, is_weights, label_post);
        else
            obj.updateModel(samples, is_weights, label_post);
        end 
       
        iter_count = iter_count + 1;
        
        %E-step
        obj.norm_perp(iter_count) = obj.getNormalizedPerplexity(is_weights);
        
        if obj.display 
            if iter_count == 1
                fprintf('Iter \t Norm. Perp.\n\n');
            end

            fprintf('%d \t %1.6f\n', iter_count, obj.norm_perp(iter_count));
            pause(0.01);
        end

        if iter_count > 1

            rel_err = abs(obj.norm_perp(iter_count) ...
                       - obj.norm_perp(iter_count - 1)) ...
                       / obj.norm_perp(iter_count);
            
            if  obj.norm_perp(iter_count) > 0.90 ...
                || rel_err < obj.tol
                break
            end
        end
        

    end

    obj.removeLowProbabilityComponents();
end
