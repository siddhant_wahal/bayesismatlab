function np = getNormalizedPerplexity(obj, is_weights);
% Returns the normalized perplexity of the input IS weights
% Inputs:
% -- is_weights : Importance sampling weights
% Outputs:
% -- np : Normalized perplexity

    logw = zeros(size(is_weights));
    %non-zero weights
    nzw = is_weights > 0;

    %low weights
    logw(nzw) = log(is_weights(nzw));
    
    np = exp(- is_weights' * logw) / numel(is_weights);
end
