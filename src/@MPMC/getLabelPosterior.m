function label_post = getLabelPosterior(obj, x)
% Returns the posterior distribution for the labels (hidden variable, which
% component did the data come from?)
%
% Outupts:
% -- label_post : n x k matrix of n posterior mass functions that correspond to
%                 the posterior label probability for each data point. 

    n = size(x, 2);
    k = obj.mixture.nComp;

    label_post = zeros(n, k);

    for i = 1 : k
        alpha            = obj.mixture.weightArr(i);
        label_post(:, i) = alpha * obj.mixture.components(i).pdf(x);

    end

    row_sum = sum(label_post, 2);
    row_sum(row_sum == 0) = Inf;
    label_post = label_post ./ row_sum; %2: sum each row

    if any(any(isnan(label_post)))
        keyboard
    end


end
