function removeLowProbabilityComponents(obj)
% Removes components whose mixture probability (weight) is less than the
% specified tolerance. This helps speedup sampling

    idx = obj.mixture.weightArr > obj.probability_tol;

    obj.mixture.nComp      = sum(idx);
    obj.mixture.components = obj.mixture.components(idx);
    obj.mixture.weightArr  = obj.mixture.weightArr(idx);
    obj.mixture.weightArr  = obj.mixture.weightArr / sum(obj.mixture.weightArr);
end
