classdef MPMC < handle

    properties
        dim;
        mixture;
        %display iterations?
        display         = false;
        %normalized perplexity 
        tol             = 1e-3;
        %tolerance for posterior label probabilities. All mixture components below
        %this tolerance are set to 0. If resetting probabilities is not desired,
        %set probability_tol to 0
        probability_tol    = 1e-8;
        n_samples_per_iter = 100000;
        max_iter = 10;
        %normalized perplexity iteration history
        norm_perp = [];
        %function for pointwise evaluation of the targetPDF
        targetPDF; 
    end % properties

    methods 

        function obj = MPMC(targetPDF, init_mixture)
            % Constructor
            % Inputs:
            % -- init_mixture : initial guess for the GMM parameters, passed as
            %                   a GaussianMixture object
            
            obj.dim       = init_mixture.dim;
            obj.targetPDF = targetPDF;
            obj.mixture   = init_mixture;
        end

        [samples, weights] = generateSamples(obj);
        label_post         = getLabelPosterior(obj, x);
        norm_perp          = getNormalizedPerplexity(obj, is_weights);

        updateModelWtsOnly(obj, samples, is_weights, label_post);
        updateModel(obj, samples, is_weights, label_post);
        run(obj, run_wts_only);
        removeLowProbabilityComponents(obj);

    end % methods

end % classdef 
