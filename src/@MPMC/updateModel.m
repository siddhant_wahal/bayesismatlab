function updateModel(obj, samples, is_weights, label_post)
% Updates the mixture only. This is the M-step in the EM algorithm.
% Inputs:
% -- samples    : Samples from the current mixture, this serves as the data in
%                 the analogy with the EM algorithm
% -- is_weights : Self-normalized importance sampling weights corresponding to
%                 samples
% -- label_post : Probability distribution over which the expected
%                 log-likelihood is computed, the posterior distribution of
%                 the label (hidden variable)

    
    %the IS weights and the label posterior define new weights. These weights
    %are used to compute weighted means and covariances that become the new
    %means and covariances
    composite_weights = is_weights .* label_post;

    mixture_weights = sum(composite_weights, 1); %1: sum each column

    k = obj.mixture.nComp;

    means       = zeros(obj.dim, k);
    covariances = zeros(obj.dim, obj.dim, k);

    for i = 1 : k

        %new parameters are just weighted means and covariances of the data with
        %the composite_weights as the weights.
        means(:, i)          = mymean(samples, composite_weights(:, i)');
        covariances(:, :, i) = mycov(samples, composite_weights(:, i)');

    end

    non_zero = (mixture_weights > obj.probability_tol);
    
    mixture_weights = mixture_weights(non_zero);
    means = means(:, non_zero);
    covariances = covariances(:, :, non_zero);
    obj.mixture = GaussianMixture(means, covariances, mixture_weights);
end
