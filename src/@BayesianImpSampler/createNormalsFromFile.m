function createNormalsFromFile(obj)
%Function to create IS distribution from file

    unpackObj = load(obj.optFileName);
    obj.normals = GaussianMixture(unpackObj.means, ...
                                  unpackObj.covariances, ...
                                  1 / obj.nOpt * ones(obj.nOpt, 1));
    obj.normalsExist = true;

end
