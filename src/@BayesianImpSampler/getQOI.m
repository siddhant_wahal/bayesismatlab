function [d] = getQOI(obj, samplesArr)
    
    n = size(samplesArr, 2);
    d = zeros(n, 1);

    d = obj.optProblem.getQOI(samplesArr);
end
