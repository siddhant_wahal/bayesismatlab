function [avg, rmse, ess, dKL, frac] = run(obj)

    fprintf('Generating %d samples ...\n', obj.nSamples)
    if obj.normalsExist == false
        obj.createNormals();
    end

    dim = obj.dim;

    funTimesWeightArr = zeros(obj.nSamples, 1);
    weightArr         = zeros(obj.nSamples, 1);
    obsArr            = zeros(obj.nSamples, 1);
    samplesArr        = zeros(dim, obj.nSamples);
    
    if obj.loadSamplesFromFile
        temp       = dlmread(obj.samplesFileName);
        samplesArr = temp(:, 1 : dim)';
        weightArr  = temp(:, dim + 1);
        obsArr     = temp(:, dim + 2);
        clear temp;
    else
        %Generate samples
        samplesArr = obj.normals.rnd(obj.nSamples);
        %Generate IS weights
        weightArr  = obj.getISWeight(samplesArr);
        %Compute QOI 
        obsArr     = obj.getQOI(samplesArr);
        
        if obj.saveSamples
            dlmwrite(obj.samplesFileName, ...
                     [samplesArr', weightArr, obsArr], ...
                     '-append', 'delimiter', ' ');
        end
    end

    %Compute indicator function
    isInsideIdx = obsArr < obj.limits(2) & obsArr > obj.limits(1);

    %Actual IS weights
    funTimesWeightArr(isInsideIdx) = weightArr(isInsideIdx);

    avg        = mean(funTimesWeightArr);
    rmse       = sqrt(var(funTimesWeightArr) / obj.nSamples) / avg;
    frac       = sum(isInsideIdx) / obj.nSamples;
    weightNorm = funTimesWeightArr / sum(funTimesWeightArr);
    ess        = 1.0 / (obj.nSamples * sum(weightNorm .^ 2));
    dKL        = sum(funTimesWeightArr(isInsideIdx) ...
                 .* log(funTimesWeightArr(isInsideIdx))) ...
                 / (avg * obj.nSamples) - log(avg);

    fprintf('Done.\n');
end
