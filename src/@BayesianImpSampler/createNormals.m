function createNormals(obj)
%Create the Gaussian mixture distribution for importance sampling
    
    %Find point to linearize around, approximately f^{-1}(x_mid)
    mid = mean(obj.limits);
    midMAP = obj.optProblem.getMAP(mid, ...
                                   obj.optProblem.prior.meanVec, ...
                                   obj.initNoiseVar);
    
    %Obtain optimal IS parameters around midMAP
    optParamVec = obj.getOptParams(midMAP);
    
    obj.noiseVar = optParamVec(1);
    obj.data = optParamVec(2);
    
    %Create normals
    if obj.normalsExist == false
        if obj.loadNormalsFromFile
            obj.createNormalsFromFile()
        else
            obj.createNormalsFromScratch()
        end
    end
end
