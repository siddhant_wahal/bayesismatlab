function createNormalsFromScratch(obj)
%Implementation of createNormals function
    
    fprintf('Creating %d mixture components ...\n', obj.nOpt);

    dim = length(obj.solverInitGuess);
    
    %For each target point, obtain the MAP, and the GN hessian around the MAP
    %opint
    for i = 1 : obj.nOpt
        map             = obj.optProblem.getMAP(obj.data, ...
                                                obj.solverInitGuess, ...
                                                obj.noiseVar);
        [hess, invHess] = obj.optProblem.getGNHess(map, obj.noiseVar);

        %Append Gaussian component to the IS mixture
        if i == 1
            obj.normals = GaussianMixture(map, invHess, 1);
        else
            obj.normals.append(Gaussian(map, invHess), 1.0 / i * ones(i, 1));
        end
    
    end
    fprintf('Done\n');

    obj.normalsExist = true;

    if obj.dumpNormalsToFile
        obj.packageNormals()
    end

end
