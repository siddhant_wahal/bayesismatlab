function [params] = getOptParams(obj, x)
%Computes optimal IS parameters around a point x in the parameter space

    %Construct affine approximation
    linApprox = LinearizedEstimator(x, obj.optProblem.problem);

    %Construct ParamsOptimizer object
    if isa(obj.optProblem.prior, 'Gaussian')
        paramOpt = ParamsOptimizer(linApprox, obj.limits, obj.optProblem.prior);
    else
        eqGauss = obj.prior.getEquivalentGaussian;
        paramOpt = ParamsOptimizer(linApprox, obj.limits, eqGauss);
    end

    %Compute optimal parameters
    params = paramOpt.getOptParams([obj.initNoiseVar; mean(obj.limits)],...
                                   [], 1e-6, 1e-6);

end
