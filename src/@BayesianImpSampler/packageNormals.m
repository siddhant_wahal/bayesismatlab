function packageNormals(obj)

    dim = length(obj.normals.components(1).meanVec);
    means = zeros(dim, obj.nOpt);
    covariances = zeros(dim, dim, obj.nOpt);
    
    for i = 1 : obj.nOpt
        means(:, i) = obj.normals.components(i).meanVec;
        covariances(:, :, i) = obj.normals.components(i).covMat;
    end

    field1 = 'means'; value1 = means;
    field2 = 'covariances'; value2 = covariances;

    packStruct = struct(field1, value1, field2, value2);

    save(obj.optFileName, '-struct', 'packStruct');

end
