classdef BayesianImpSampler < handle
%A class for performing goal oriented UQ via the BIMC method.

    properties
        optProblem;
        solverInitGuess;
        initNoiseVar;
        noiseVar;
        limits;
        data;
        dim;
        
        nOpt                = 1;
        nSamples            = 1;
        normals             = []
        optFileName         = [];
        samplesFileName     = [];
        normalsExist        = false
        dumpNormalsToFile   = false;
        loadNormalsFromFile = false;
        loadSamplesFromFile = false;
        saveSamples         = false;
    end

    methods
        function obj = BayesianImpSampler(limits, optProblem, initNoiseVar)
            %Limits of the target interval
            obj.limits = limits;
            %Optimization problem object
            obj.optProblem = optProblem;
            %Decide initial guess for pseudo-MAP computation based on prior type
            if isa(optProblem.prior, 'Gaussian')
                obj.solverInitGuess = optProblem.prior.meanVec;
            else
                obj.solverInitGuess = optProblem.prior.rnd(1);
            end
            obj.dim = numel(obj.solverInitGuess);
            %Initial guess for likelihood function variance
            obj.initNoiseVar = initNoiseVar;
        end

        createNormals(obj);
        createNormalsFromScratch(obj);
        createNormalsFromFile(obj);
        [params]        = getOptParams(obj, x);
        [weightArr]     = getISWeight(obj, sampleArr);
        [d]             = getQOI(obj, samplesArr);
        [avg, rmse, ...
         ess, dKL, ...
         frac]          = run(obj);
    end
end
