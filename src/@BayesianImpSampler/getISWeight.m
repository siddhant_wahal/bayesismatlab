function [weightArr] = getISWeight(obj, sampleArr)
%Returns the IS weight : p(x) / q(x)
%Inputs:
% -- sampleArr: array of size dim x nSamples containing sample at which to
%               compute the ratio
    
    n         = size(sampleArr, 2);
    %Perform computation in the log domain to avoid under/overflow
    weightArr = exp(obj.optProblem.prior.logpdf(sampleArr) ...
                    - obj.normals.logpdf(sampleArr));

end
